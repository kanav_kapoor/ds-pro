<?php
	$designs = [];
	$newProductDesigns = $_POST['newProductDesigns'];
	$newProductImages = $_POST['newProductImages'];
	$dir = "products/";
	$randomStr = time();

	// Move all files
	foreach ($newProductDesigns as $side => $design) {
		$uniqueName = $randomStr."-".$side;
		$fileName = $uniqueName.".json";
		$imgName = $uniqueName.".png";
		// Save Design
		file_put_contents($dir.$fileName, $design);

		// Save Image
		$image = $newProductImages[$side];
		file_put_contents($dir.$imgName, base64_decode(str_replace(' ', '+', str_replace('data:image/png;base64,', '', $image))));

		$images[$side] = $imgName;
	}

	// Append name in json
	$file = "products.json";
	$existingData = json_decode(file_get_contents($file), true);
	$ip = $_SERVER['REMOTE_ADDR'];

	$existingData[$ip][$randomStr] = $images;
	if(file_put_contents($file, json_encode($existingData))) echo "$_SERVER[HTTP_REFERER]?id=".$randomStr;
?>