$(function(){
	var ctx = demo.getContext('2d'),
    font = '64px impact',
    w = demo.width,
    h = demo.height,
    curve,
    offsetY,
    bottom,
    textHeight,
    isTri = false,
    dltY,
    angleSteps = 180 / w,
    i = w,
    y,
    os = document.createElement('canvas'),
    octx = os.getContext('2d');

	os.width = w;
	os.height = h;

	octx.font = font;
	octx.textBaseline = 'top';
	octx.textAlign = 'center';

	function renderBridgeText(iCurve, iOffset, iHeight, iBottom, iTriangle, iText) {

	    curve = parseInt(iCurve, 10);
	    offsetY = parseInt(iOffset, 10);
	    textHeight = parseInt(iHeight, 10);
	    bottom = parseInt(iBottom, 10);
	    isTri = iTriangle;

	    octx.clearRect(0, 0, w, h);
	    ctx.clearRect(0, 0, w, h);

	    octx.fillText(iText.toUpperCase(), w * 0.5, 0, 100);

	    /// slide and dice
	    i = w;
	    dltY = curve / textHeight;
	    y = 0;
	    while (i--) {
	        if (isTri) {
	            y += dltY;
	            if (i === (w * 0.5)|0) dltY = -dltY;
	        } else {
	            y = bottom - curve * Math.sin(i * angleSteps * Math.PI / 180);
	        }
	        ctx.drawImage(os, i, 0, 1, textHeight,
	        i, h * 0.5 - offsetY / textHeight * y, 1, y);
	    }
	    return demo.toDataURL();
	}

	
})

//renderBridgeText(150, 30, 90, 200, false, "kanav kapoor");
/*iCurve.onchange = iOffset.onchange = iHeight.onchange = iBottom.onchange = iText.onkeyup = iTriangle.onchange = renderBridgeText;*/