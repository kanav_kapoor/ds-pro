/*
 * Design Studio Pro - Copyrights Browsewire 2016
 *
 */


(function($) {
	var designStudio = function( container, customOptions){

		// Load default options and replace it with customized options
		var options = $.extend({}, $.fn.designStudio.defaultOptions, customOptions);
		//options.dimensions = $.extend({}, $.fn.designStudio.defaultOptions.dimensions, options.dimensions);

		// Define global variables
		var designStudio = this,
				canvas,
				stage,
				currentSide,
				currentElement,
				template,
				params,
				fabricParams,
				$productContainer;

		var currentState = 0, lastState, nextState;
		var saveThis = true

		/* Private Methods Started */
		var _testCanvas = function(){
			// Test if canvas is supported
			var canvasTest = document.createElement('canvas');

			if(!Boolean(canvasTest.getContext && canvasTest.getContext('2d'))){
				alert("Your browser doesn\'t support custom design. Please use a different browser");
				return;
			}
		}

		var _modifyFabricDefaultPrototype = function(){
			// Changing Default Fabric Prototype
			var _original = fabric.Object.prototype._drawControl;
			fabric.Object.prototype._drawControl = function(control, ctx, methodName, left, top) {
				var size = this.cornerSize;
				if (this.canvas.hasControlCallback && this.canvas.hasControlCallback[control]) {
					this.canvas.controlCallback[control](ctx, left, top, size);
				} else {
					_original.call(this, control, ctx, methodName, left, top);
				}
			};

			fabric.Canvas.prototype.cursorMap = [
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer',
				'pointer'
			];
		}

		var _centerObject = function(element){
			element.center();
			element.setCoords();
		}

		var _doModification = function(element){
			var currentElementParams = element.params;

			if(typeof currentElementParams.colors === 'object' ||
				currentElementParams.removable ||
				currentElementParams.draggable ||
				currentElementParams.resizable ||
				currentElementParams.rotatable ||
				currentElementParams.zChangeable ||
				currentElementParams.editable ||
				currentElementParams.patternable
				|| currentElementParams.uploadZone) {

				element.set('selectable', true);

				if(currentElementParams.draggable) {
					element.lockMovementX = element.lockMovementY = false;
				}

				if(currentElementParams.rotatable) {
					element.lockRotation = false;
				}

				if(currentElementParams.resizable) {
					element.lockScalingX = element.lockScalingY = false;
				}

				if((currentElementParams.resizable || currentElementParams.rotatable || currentElementParams.removable)) {
					element.hasControls = true;
				}
			}
		}

		var _fabricElemCreated = function(element){
			params = element.params;

			// If side is defined, add element to that side
			if(typeof params.side != "undefined") {
				stage = window[params.side];
			}
		
			stage.add(element);



			// If is preview, save in global scope and resize it
			if(params.preview) {
				var originalHeight = element.getHeight();

				window[params.side+"Preview"] = element;
				element.scaleToWidth(stage.getWidth());
			
				if(element.getHeight() > stage.getHeight()){
					element.scaleToHeight(stage.getHeight());
				}
				
				_centerObject(element);
				resizedRatio = element.getHeight()/originalHeight;

				// Add Shadow
				stage.setOverlayImage(params.shadow, stage.renderAll.bind(stage), {width: element.getWidth(), height: element.getHeight(), left: element.left, top: element.top, opacity: 0.5});
				addedShadowCount++;


			}else if(params.path){
				element.scaleToHeight(params.height);

				if(params.color) {
					var sidePath = _getPathByStage(stage, element.name);
					if(sidePath) _setStyle(sidePath, 'fill', params.color);
				}else if(params.texture){
					var sidePath = _getPathByStage(stage, element.name);
					if(sidePath) _setPattern(sidePath, params.texture);
				}else if(params.pattern){
					var sidePath = _getPathByStage(stage, element.name);
					if(sidePath) _setPattern(sidePath, params.pattern);
				}
			}

			_doModification(element);
			// Scale object to canvas width
			if(params.scaleToWidth) {
				element.scaleToWidth(stage.getWidth());
			}

			if(params.resizeToW){
				element.scaleToWidth(params.resizeToW);
			}

			if(params.autoCenter)
				_centerObject(element);

			if(params.autoSelect){
				stage.setActiveObject(element);
				element.setCoords();
			}
			
			if(params.sendToBack)
				stage.sendToBack(element);
			
			stage.renderAll().calcOffset();

			// Reset stage
			if(typeof params.resetStageTo != "undefined") stage = params.resetStageTo;
			if(!params.base && !params.preview && !params.path)
			_pushToHistory(saveThis);
		}

		//Toggle Object Property
		var toggleObjPropCore = function(element, conditionType, property, options){
			if(element.type === conditionType){
				var newObj = {};
				newObj[property] = (element[property] == options[1] ? options[0] : options[1]);
				element.set(newObj);
				element.params[property] = newObj[property];
				stage.renderAll();
			}
		}

		var _toggleObjProp = function(element, conditionType, property, options){ 
			//dd(element, conditionType, property, options);
			if(conditionType.constructor === Array){
				$.each(conditionType, function(index, value){
					toggleObjPropCore(element, value, property, options);
				});
			}else{
				toggleObjPropCore(element, conditionType, property, options);
			}
		}

		var _setStyle = function(object, property, value){
			object.set(property, value);
			object.params[property] = value;
			stage.renderAll().calcOffset();
		}

		var _setPattern = function(object, url) {
			fabric.util.loadImage(url, function(img) {
				object.fill = new fabric.Pattern({
					source: img,
					repeat: 'repeat' // repeat, repeat-x, repeat-y or no-repeat
				});
				stage.renderAll();
			});
		}

		var _getZ = function(object){
			stage.getObjects().indexOf(object);
		}

		var updateColorDisplayDiv = function (element, color){
			element.css("background-color", color);
		}

		var _preloadImages = function(images){
			$.each(images, function(i, img){
				$("body").append("<img src='"+img+"' class='hide'>");
			});
		}

		var _changeImageColor = function(obj, color){
			obj.filters.push(new fabric.Image.filters.Tint({color: color, opacity: 1}));
			try {
				obj.applyFilters(stage.renderAll.bind(stage));
			}
			catch(evt) {
				alert("Color cannot be changed. Please try again");
			}
		}

		var _resizeDisplay = function($productContainer){
			var productStageWidth = options.dimensions.productStageWidth;
			var productStageHeight = options.dimensions.productStageHeight;
			var productDisplayWidth = options.dimensions.productDisplayWidth;

			var widthDiff = productStageWidth-productDisplayWidth;
			var widthDiffProportionately = widthDiff/productStageWidth;
			var productDisplayHeight = options.dimensions.productDisplayHeight = productStageHeight - (productStageHeight*widthDiffProportionately);

			$productContainer.find(".upper-canvas, .lower-canvas, .canvas-container").width(productDisplayWidth).height(productDisplayHeight).css({left: 0, top: 0});
			$productContainer.find(".canvas-container").css({border: '3px dashed #DDD', overflow: 'hidden'});
		}

		var _setDimensions = function(side, $productContainer){
			side.setDimensions({width: options.dimensions.productStageWidth, height: options.dimensions.productStageHeight});
			_resizeDisplay($productContainer);
		}

		var _changeCanvasIcons = function(side){
			side.hasControlCallback = {
				tr: true,
				mtr: true
			};

			side.controlCallback = {
				tr: function (ctx, left, top, size) {
					var image = new Image(), x, y;

					image.src = 'assets/img/control-icons/delete.png';
					x = left - image.width/2 + size/2;
					y = top - image.height/2 + size/2;

					ctx.drawImage(image, x, y);
				},
				mtr: function (ctx, left, top, size) {
					var image = new Image(), x, y;

					image.src = 'assets/img/control-icons/rotate.png';
					x = left - image.width/2 + size/2;
					y = top - image.height/2 + size/2;

					ctx.drawImage(image, x, y);
				}
			};
		}

		var _resetTextTab = function(){
			$("#text-font-family").val($("#text-font-family option:first").val());
			$("#text-properties button").removeClass("active");
			$("#text-content").val('');
		}

		var _updateTextTab = function(element){
			//_resetTextTab();

			var type           = element.type;
			var text           = element.getText();
			var fill           = element.getFill();
			var fontFamily     = element.getFontFamily();
			var fontWeight     = element.getFontWeight();
			var fontStyle      = element.getFontStyle();
			var textDecoration = element.getTextDecoration();
			var textAlign      = element.getTextAlign();
			var lineHeight     = element.getLineHeight();
			var opacity        = element.getOpacity();
			var radius, spacing;


			$("#text-content").val(text);
			$("#text-font-family").val(fontFamily).css('font-family', fontFamily);
			$("#text-line-height").get(0).noUiSlider.set(lineHeight);
			$("#text-opacity").get(0).noUiSlider.set(10*(1-opacity));

			/* Begin - Curved Text */
			//$("#text-curve-switch").bootstrapSwitch('state', type === "curvedText");

			/*if(type === "curvedText"){
				$("#text-curve-radius").get(0).noUiSlider.set(element.getRadius());
				$("#text-curve-spacing").get(0).noUiSlider.set(element.getSpacing());
			}*/
			/* End - Curved Text */

			/*if(fontWeight)
				$("#text-bold").addClass("active");

			if(fontStyle)
				$("#text-italic").addClass("active");

			if(textDecoration)
				$("#text-underline").addClass("active");

			$("#text-align-"+textAlign).addClass("active");*/
		}

		var _showTab = function(elementType){
			if (elementType == "text" || elementType == "i-text") showPopup("text");
			else if (elementType == "image") showPopup("image");
			else if (elementType == "product") showPopup("product");

			function showPopup(name){
				$(".popup-options > div").addClass('hide');
				$(".popup-"+name+"-options").removeClass('hide');
			}
		}

		var _updateTabs = function(currentElement){
			//var elementType = currentElement.type;
			var elementType = currentElement.type == "path" ? "product" : currentElement.type;
			_showTab(elementType);
			return;
		}

		var _pushToHistory = function(saveThis){
			if(saveThis === true){
				var historyObject = $.fn.designStudio.defaultOptions.sidesHistory;
				// Slice if user has undo the changes and now applying new
				stateDifference = $.fn.designStudio.defaultOptions.sidesHistory.length-1-currentState;
				if(stateDifference > 0){
					historyObject.splice(currentState+1, stateDifference);
				}
				historyObject.push(designStudio.getJSONDesign());
				currentState = $.fn.designStudio.defaultOptions.sidesHistory.length-1;
			}
		}

		var _registerEvents = function(side){
			side.on('object:selected', function(opts){

				// Clear stroke of path
				if(typeof currentElement != "undefined" && currentElement.type == "path")
					currentElement.set({stroke: "transparent"});

				currentElement = opts.target;

				if(currentElement.type == "path"){
					currentElement.set({stroke: "#ff0000", strokeWidth: 1, strokeDashArray: [5, 5]});
					currentElement.set({
						borderColor: 'transparent',
						cornerColor: 'transparent',
						cornerSize: 16,
					});
				}else {
					currentElement.set({
						borderColor: 'red',
						cornerColor: 'red',
						cornerSize: 16,
					});
				}
				_updateTabs(currentElement);

				/* Need To Delete */
				/*if(currentElement.type == "path"){
					$("#path-top").val(currentElement.top);
					$("#path-left").val(currentElement.left);
					$("#path-height").val(currentElement.getHeight());
				}*/
				/* Need To Delete */
				
			})

			side.on('object:added', function(opts){
				setTimeout(function(){designStudio.setPreviewImages()}, 200);
				designStudio.setPreviewImages();
				//modifyAddons();
			})

			side.on('object:removed', function(opts){
				setTimeout(function(){designStudio.setPreviewImages()}, 200);
				_pushToHistory(saveThis);
				designStudio.setPreviewImages();
			})

			side.on('object:modified', function(opts){
				//console.log("modified");
				//currentElement = opts.target;
				//_updateTabs(currentElement);
				setTimeout(function(){designStudio.setPreviewImages()}, 200);
				_pushToHistory(saveThis);
				designStudio.setPreviewImages();
			})

			side.on('object:out', function(opts){
				console.log(currentElement.type);
			});

			side.on('mouse:up', function(opts) {
				
				if(opts.target == undefined) {
					designStudio.deselectElement();
				}
				else {
					var targetCorner = opts.target.__corner;
					var currentElement = side.getActiveObject();
					var type = currentElement.type;

					//remove element
					if(targetCorner == 'tr'){
						side.remove(currentElement);
						if(type == "text" || type == "i-text"){
							designStudio.modifyAddons("minus", "text");
						}else if(type == "image"){
							designStudio.modifyAddons("minus", "image");
						}
						designStudio.updatePrice();
					}
				}
				side.renderAll();
				
			});

			side.on('mouse:over', function(opts){
				if(opts.target.type == "path"){
					opts.target.set({stroke: "#ff0000", strokeWidth: 1, strokeDashArray: [5, 5]});
					side.renderAll();
				}
			})

			side.on('mouse:out', function(opts){
				if(opts.target.type == "path" && opts.target != currentElement && opts.target.stroke == "#ff0000"){
					opts.target.set({stroke: "transparent"});
					side.renderAll();
				}
			})

			// Keep Objects Within Limit
			side.on('object:moving', function (e) {
				var obj = e.target;
				 // if object is too big ignore
				if(obj.currentHeight > obj.canvas.height || obj.currentWidth > obj.canvas.width){
						return;
				}        
				obj.setCoords();        
				// top-left  corner
				if(obj.getBoundingRect().top < 0 || obj.getBoundingRect().left < 0){
						obj.top = Math.max(obj.top, obj.top-obj.getBoundingRect().top);
						obj.left = Math.max(obj.left, obj.left-obj.getBoundingRect().left);
				}
				// bot-right corner
				if(obj.getBoundingRect().top+obj.getBoundingRect().height  > obj.canvas.height || obj.getBoundingRect().left+obj.getBoundingRect().width  > obj.canvas.width){
						obj.top = Math.min(obj.top, obj.canvas.height-obj.getBoundingRect().height+obj.top-obj.getBoundingRect().top);
						obj.left = Math.min(obj.left, obj.canvas.width-obj.getBoundingRect().width+obj.left-obj.getBoundingRect().left);
				}
			});
		}

		var _registerHandlers = function(){
			$(window).resize(function(){
				options.dimensions.productDisplayWidth = $(container).children('div:not(".hide")').width();

				$(container).children('div').each(function(){
					_resizeDisplay($(this));
				});
			});
		}

		/*var _addPreviewAndBase = function(){

		}*/

		var _addDefaultParams = function(defaultParams, currentSide){
			var firstSide = $.fn.designStudio.defaultOptions.sides[0];

			// Add Preview And Base
			designStudio.addElement("image", defaultParams.preview, {preview: true, side: currentSide, resetStageTo: window[firstSide], sendToBack: true, shadow: defaultParams.base});
			
			// Add Paths
			if(typeof defaultParams.paths != "undefined"){
				var paths = defaultParams.paths;
				$.each(paths, function(index, pathObj){
					designStudio.addElement("path", pathObj.path, $.extend({}, {path: true, side: currentSide, resetStageTo: window[firstSide]}, pathObj));
				});
			}
			
			// Add Template
			if(typeof defaultParams.template != "undefined"){
				$.each(defaultParams.template, function(i, obj){
					var params = obj.params;
					if(obj.type === "text"){
						designStudio.addText(params.src, $.extend({side: currentSide}, params));
					}else if(obj.type === "image"){
						designStudio.addImage(params.src, $.extend({side: currentSide}, params));
					}
				});
			}



			
			/*designStudio.addShadow(currentSide, defaultParams.base)*/
		}

		var _loadSavedDesign = function(currentSide, jsonDesign){
			currentSide.loadFromJSON(jsonDesign, currentSide.renderAll.bind(currentSide));
			currentSide.renderAll();      

			// Reset stage to first stage (Needed here, as there is a functionality in new canvas to reset stage to front)
			for(var firstSide in design) break;     
			stage = window[firstSide];
			addedShadowCount++;
		}

		var _getPathByName = function(side, name){
			var pathObject = false;
			$.each(window[side].getObjects(), function(i, object){
				if(typeof object.name != "undefined" && object.name == name){
					pathObject = object;
					return;
				}
			});

			return pathObject;
		}

		var _getPathByStage = function(stage, name){
			var pathObject = false;
			$.each(stage.getObjects(), function(i, object){
				if(typeof object.name != "undefined" && object.name == name){
					pathObject = object;
					return;
				}
			});

			return pathObject;
		}

		var addedShadowCount;

		var _init = function(){
			$("#loader").removeClass("hidden");
			_testCanvas();
			_modifyFabricDefaultPrototype();

			$(".preview-images img").parent().hide();

			// empty sides array
			$.fn.designStudio.defaultOptions.sides = [];
			// If saved design, remove default product specs from container
			if(!$.isEmptyObject(design)) $(container).empty();

			// Design Specification object
			var designSpecs = !$.isEmptyObject(design) ? design : $(container).children('div');
			var designSidesCount = !$.isEmptyObject(design) ? getObjectLength(designSpecs) : designSpecs.length;
			addedShadowCount = 0;
			
			$.each(designSpecs, function(side, jsonDesign){
				// New Design
				if($.isEmptyObject(design)){
					$productContainer = $(this);
				}
				// Saved Design
				else {
					$(container).append("<div data-side='"+side+"'></div>");
					$productContainer = $(container).find("div[data-side='"+side+"']");
					if($(container).children().length > 1) $productContainer.addClass('hide');
				}

				// HTML Manipulations
				$productContainer.append('<canvas></canvas>');
				canvas = $productContainer.children('canvas').get(0);

				var currentSide = $productContainer.data('side');
				$.fn.designStudio.defaultOptions.sides.push(currentSide);

				// Use Fabric to set the stage
				window[currentSide] = new fabric.Canvas(canvas, {
					selection: false,
					hoverCursor: 'pointer',
					rotationCursor: 'default',
					controlsAboveOverlay: true,
					centeredScaling: true
				});

				_setDimensions(window[currentSide], $productContainer);
				_changeCanvasIcons(window[currentSide]);
				_registerEvents(window[currentSide], $productContainer);
				$("#"+currentSide+"-preview").parent().show();
				
				// If new, add new params
				if($.isEmptyObject(design)) _addDefaultParams($productContainer.data('params'), currentSide);       
				// If saved, load params from JSON
				else  _loadSavedDesign(window[currentSide], jsonDesign);
				designStudio.updatePrice();
			})
			
			_registerHandlers();
			_preloadImages(['assets/img/control-icons/delete.png', 'assets/img/control-icons/rotate.png']);
			
			var loaderInterval = setInterval(
				function(){
					if(addedShadowCount == designSidesCount){
						$("#loader").addClass("hidden");
						clearInterval(loaderInterval);
					}
				}, 1000);
		}

		setTimeout(_init, 2000);
		
		/* Private Methods Ended */

		/* Helper Methods Started */
		function getObjectLength(object){
			var count = 0;

			for(var i in object){
				if(object.hasOwnProperty(i)) count++;
			}
			return count;
		}
		/* Helper Methods Ended */


		/* Public Methods Stared */
		var _changeStage = function(side){
			stage = window[side];
			currentSide = side;
		}

		var _getTextImage = function(side){
			var objects = window[side].getObjects();
			var elements = [];
			$.each(objects, function(i, object){
				
				if(object.type == "text" || object.type == "i-text" || object.type == "image"){
					var tempObj = {};
					tempObj.type = object.type;
					
					if(object.type == "image") tempObj.src = object.source;
					else tempObj.src = object.text;         
					tempObj.z = i;

					elements.push(tempObj);
				}
			});
			return elements;
		}

		designStudio.updateLayers = function(side){
			$("#layers").empty();
			
			$.each(_getTextImage(side), function(i, object){
				if(object.type == "text" || object.type == "i-text"){
					$("#layers").append("<li data-z='"+object.z+"' class='list-group-item'><span>☰&nbsp;</span>"+object.src+"</li>");
				}else{
					$("#layers").append("<li data-z='"+object.z+"' class='list-group-item'><span>☰&nbsp;</span><img class='layer-img' src='"+object.src+"'/></li>");
				}
			});
		}

		designStudio.setNewZIndex = function(index){
			stage.moveTo(stage.getObjects()[index.old], index.new);
			stage.renderAll();

			designStudio.updateLayers(currentSide);
		}

		designStudio.changeSide = function(side){
			// Change stage
			_changeStage(side);
			
			$(container).children("div").each(function(){
				if($(this).data('side') === side)
					$(this).removeClass('hide');
				else
					$(this).addClass('hide');
			});

			designStudio.updateLayers(side);
		}

		// zoom stage
		designStudio.zoom =  function(zoomValue){
			$(container).children('div').each(function(){
				var $productContainer = $(this);

				var productDisplayWidth = options.dimensions.productDisplayWidth;
				var productDisplayHeight = options.dimensions.productDisplayHeight;

				var newWidth = productDisplayWidth+(productDisplayWidth*zoomValue/100);
				var newHeight = productDisplayHeight+(productDisplayHeight*zoomValue/100);

				var left = parseInt($productContainer.find(".upper-canvas").css('left'));
				var top = parseInt($productContainer.find(".upper-canvas").css('top'));

				var newLeft = 0-(newWidth-productDisplayWidth)/2;
				var newTop = 0-(newHeight-productDisplayHeight)/2;

				$productContainer.find(".upper-canvas, .lower-canvas").width(newWidth).height(newHeight).css({left: newLeft, top: newTop});
			});
		}

		designStudio.move = function(direction){
			$(container).children('div').each(function(){
				var $productContainer = $(this);

				var left = $productContainer.find(".upper-canvas").css('left');
				var top = $productContainer.find(".upper-canvas").css('top');

				var newLeft, newTop;
				
				switch(direction) {
					case "up":
						newTop = parseInt(top)+10;
						newLeft = left;
						break;

					case "down":
						newTop = parseInt(top)-10;
						newLeft = left;
						break;

					case "left":
						newLeft = parseInt(left)+10;
						newTop = top;
						break;

					case "right":
						newLeft = parseInt(left)-10;
						newTop = top;
						break;

				}       

				$productContainer.find(".upper-canvas, .lower-canvas").animate({left: newLeft, top: newTop}, "fast");
			});
		} 

		designStudio.deselectElement = function(){
			if(typeof currentElement != "undefined" && currentElement.type == "path")
				currentElement.set({stroke: "transparent"});
		}


		designStudio.addElement = function(type, source, params){
			var params = $.extend({}, options.elementParameters, params);
			
			if(type == "text"){
				params = $.extend({}, options.textParameters, params);        
			}

			var fabricParams = {
				source: source,
				//title: title,
				top: params.y,
				left: params.x,
				//originX: params.originX,
				//originY: params.originY,
				scaleX: params.scale,
				scaleY: params.scale,
				angle: params.degree,
				opacity: params.opacity,
				id: String(new Date().getTime()),
				//visible: containerIndex == currentViewIndex,
				//viewIndex: containerIndex,
				lockUniScaling: true,
				lineHeight: 1.2
			};

			if(params.editorMode) {
				params.removable = params.resizable = params.rotatable = params.zChangeable = true;
			}
			else {
				$.extend(fabricParams, {
					selectable: false,
					lockRotation: true,
					lockScalingX: true,
					lockScalingY: true,
					lockMovementX: true,
					lockMovementY: true,
					hasControls: false,
					evented: false
				});
			}

			if(type == "image"){
				var imageLoaded = function(fabricImage, params){
					$.extend(fabricParams, {params: params, originParams: $.extend({}, params), crossOrigin: "anonymous"});
					fabricImage.set(fabricParams);
					
					//dd(fabricImage.params);
					if(fabricImage.getWidth() < options.customImagesParameters.minW){
						alert("PLease upload am image with resolution more than 20px");
						return;
					}

					if(fabricImage.getWidth() > options.customImagesParameters.maxW){
						fabricImage.scaleToWidth(options.customImagesParameters.resizeToW);
					}

					if(fabricImage.getHeight() > options.customImagesParameters.maxH){
						fabricImage.scaleToHeight(options.customImagesParameters.resizeToH);                
					}

					_fabricElemCreated(fabricImage);
				}

				fabric.Image.fromURL(source, function(fabricImage) {
					imageLoaded(fabricImage, params);
				});
				
			}else if(type == "text"){
				
				params.text = params.text ? params.text : params.source;
				
				//params.font = params.font ? params.font : options.fonts[0];
				
				if(!params.font) {
					params.font = 'Arial';
				}

				$.extend(fabricParams, {
					fontSize: params.textSize,
					fontFamily: params.font,
					fontStyle: params.fontStyle,
					fontWeight: params.fontWeight,
					textAlign: params.textAlign,
					textBackgroundColor: params.textBackgroundColor,
					lineHeight: params.lineHeight,
					textDecoration: params.textDecoration,
					fill: params.colors[0] ? params.colors[0] : "#000000",
					editable: params.editable,
					spacing: params.curveSpacing,
					radius: params.curveRadius,
					reverse: params.curveReverse,
					params: params,
					originParams: $.extend({}, params)
				});
				
				if(params.curved) {
					//return;
					var fabricText = new fabric.CurvedText(params.text.replace(/\\n/g, '\n'), fabricParams);
				}else{
					var fabricText = new fabric.IText(params.text.replace(/\\n/g, '\n'), fabricParams);
				}
				_fabricElemCreated(fabricText);
			}else if(type=="path"){
				$.extend(fabricParams, {selectable: true, fill: 'transparent', left: params.x, evented: true, name: params.name, params: params, originParams: $.extend({}, params)});

				var fabricPath = new fabric.Path(source, fabricParams);
				_fabricElemCreated(fabricPath);
			}
		}

		designStudio.addShadow = function(side, src){
			var previewImg = window[side+'Preview'];
			window[side].setOverlayImage(src, window[side].renderAll.bind(window[side]), {width: previewImg.getWidth(), height: previewImg.getHeight(), left: previewImg.left, opacity: 0.5});
		}

		function deleteObjectKey(object, key){
			if(typeof object[key] != "undefined") delete object[key]; 
		}

		function objectKeyCount(o){
			var count = 0;
			for(var i in o){
				if(o.hasOwnProperty(i)) count++;
			}
			return count;
		}

		designStudio.modifyAddons = function(type, elementType, params){
			if(elementType == "text" || elementType == "image"){
				if(type == "add") $.fn.designStudio.defaultOptions.addons[elementType] += 1;
				else if(type == "minus") $.fn.designStudio.defaultOptions.addons[elementType] -= 1;
			}else if(elementType == "productFill" || elementType == "productTexture" || elementType == "productPattern") {
				
				if(type == "add"){
					var object = {};
					object.src = params.src;
					var addons = $.fn.designStudio.defaultOptions.addons;
					
					deleteObjectKey(addons.productFill, params.name);
					deleteObjectKey(addons.productTexture, params.name);
					deleteObjectKey(addons.productPattern, params.name);

					addons[elementType][params.name] = object; 
				}
			}else if(type == "minus"){
					deleteObjectKey(addons[elementType], params.name);
			}

			//dd($.fn.designStudio.defaultOptions.addons);
			//dd(designStudio.pricingModel());
		};

		var _getTextImageDetails = function(){
			var textImageDetails = {};
			var textDetails = [];
			var imageDetails = [];
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				textDetails[side] = [];
				imageDetails[side] = [];

				var currentSideObjects = window[side].getObjects();
				$.each(currentSideObjects, function(i, object){
					var type = object.type;
					var commonObjectDetails = $.extend({}, {left: object.get('left'), top: object.get('top'), width: object.get('width'), height: object.get('height'), 'angle': object.get('angle')});

					if(type === "text" || type === "i-text"){
						var textObject = $.extend(commonObjectDetails, {text: object.getText(), fontSize: object.getFontSize(), color: object.getFill(), stroke: object.getStroke(), fontFamily: object.getFontFamily()});
						textDetails[side].push(textObject);
					}

					if(type === "image"){
						var imgObject = $.extend(commonObjectDetails, {src: object.getSrc()});
						imageDetails[side].push(imgObject);
					}
				})
				
			});

			
			textImageDetails.textDetails = textDetails;
			textImageDetails.imageDetails = imageDetails;
			return textImageDetails;
		};

		designStudio.setTextImageDetails = function(){
			setTimeout(function(){
				var textImageDetails = _getTextImageDetails();
				var addedImages = textImageDetails.imageDetails;
				var addedTexts = textImageDetails.textDetails;
				
				var details = "";

				$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
					details += "<br><b>"+side.charAt(0).toUpperCase()+side.slice(1)+" Side </b><br>";
					var addedImagesToThisSide = addedImages[side];
					var addedTextsToThisSide = addedTexts[side];
					
					// If image has been added to this side
					if(addedImagesToThisSide.length){
						details += "<u>Images Detail</u>: <br>";
						$.each(addedImagesToThisSide, function(j, imageDetails){
							$.each(imageDetails, function(property, value){
								details += property+"("+value+"), ";
							});
							details+= "<br>";
						});
					}

					if(addedTextsToThisSide.length){
						details += "<u>Texts Detail</u>: <br>";
						$.each(addedTextsToThisSide, function(j, textDetails){
							$.each(textDetails, function(property, value){
								details += property+"("+value+"), ";
							});							
						});
						details+= "<br>";
					}
				});
				$('.used_objects').html(details);
				
			}, 3000);
		};

		designStudio.pricingModel = function(){

			/*{
				"text": 1,
				"image": 1,
				"productColor": {base: {src: ''}},
				"productTexture": {base: {src: ''}},
				"productPattern": {base: {src: ''}}
			}*/
			var priceObject = {};
			priceObject.base = {count: 1, total: $.fn.designStudio.defaultOptions.price.base};
			var addons = $.fn.designStudio.defaultOptions.addons;

			$.each(addons, function(type, details){
				priceObject[type] = {};
				var price = $.fn.designStudio.defaultOptions.price[type];
				
				// text, image
				if(typeof details === "number"){
					priceObject[type].count = details;          
				}
				// productColor, productTexture, productPattern
				else {
					priceObject[type].count = objectKeyCount(details);
				}
				priceObject[type].perPrice = price;
				priceObject[type].total = priceObject[type].count*price;
			});

			return priceObject;
		}

		designStudio.updatePrice = function(){
			var qty = 0;
			var total = 0;
			var sizes = ['xs', 's', 'm', 'l', 'xl', 'xxl'];
			
			
			$.each(designStudio.pricingModel(), function(name, detailsObject){
				if(detailsObject.count) total += detailsObject.total;
			});

			// Quantities
			$.each(sizes, function(i, size){
				var sizeCount =  parseInt($.trim($('.'+size+'_count').val()));
				sizeCount = isNaN(sizeCount) ? 0 : sizeCount;
				qty += sizeCount;

				$('.'+size+'_total').text(sizeCount*total);
			})      
			//console.log(qty);

			// If quantity is 0, take qty as 1
			if(!qty) qty = 1;

			$(".total_price").text(qty*total.toFixed(2));

			designStudio.setTextImageDetails();
		};

		designStudio.addText = function(src, params){
			designStudio.addElement("text", src, $.extend({text: src}, params));
			designStudio.modifyAddons("add", "text", {src: src});
			designStudio.updatePrice();
		}

		designStudio.addImage = function(src, params){
			designStudio.addElement("image", src, params);
			designStudio.modifyAddons("add", "image", {src: src});
			designStudio.updatePrice();
		}

		designStudio.changeTextProperty = function(property, value){
			if(typeof currentElement === "undefined" || (currentElement.type !== "i-text" && currentElement.type !== "text" && currentElement.type !== "curvedText")) return;

			switch(property) {
				case "font-text":
					_setStyle(currentElement, 'text', value);
					_pushToHistory(saveThis);
					break;

				case "bold":
					_setStyle(currentElement, 'fontWeight', currentElement.getFontWeight() == "bold" ? "" : "bold");
					_pushToHistory(saveThis);
					break 

				case "italic":
					_setStyle(currentElement, 'fontStyle', currentElement.getFontStyle() == "italic" ? "" : "italic");
					_pushToHistory(saveThis);
					break;

				case "underline":
					_setStyle(currentElement, 'textDecoration', currentElement.getTextDecoration() == "underline" ? "" : "underline");
					_pushToHistory(saveThis);
					break;

				case "line-through":
					_setStyle(currentElement, 'textDecoration', currentElement.getTextDecoration() == "line-through" ? "" : "line-through");
					_pushToHistory(saveThis);
					break;

				case "overline":
					_setStyle(currentElement, 'textDecoration', currentElement.getTextDecoration() == "overline" ? "" : "overline");
					_pushToHistory(saveThis);
					break;

				case "align-left":
					_setStyle(currentElement, 'textAlign', 'left');
					_pushToHistory(saveThis);
					break;

				case "align-center":
					_setStyle(currentElement, 'textAlign', 'center');
					_pushToHistory(saveThis);
					break;

				case "align-right":
					_setStyle(currentElement, 'textAlign', 'right');
					_pushToHistory(saveThis);
					break;

				case "font-family":
					_setStyle(currentElement, 'fontFamily', value);
					_pushToHistory(saveThis);
					break;

				case "font-fill":
					_setStyle(currentElement, 'fill', value);
					_pushToHistory(saveThis);
					break;

				case "font-pattern":
					_setPattern(currentElement, value);
					_pushToHistory(saveThis);
					break;

				case "font-stroke":
					_setStyle(currentElement, 'stroke', value);
					_pushToHistory(saveThis);
					break;

				case "line-height":
					_setStyle(currentElement, 'lineHeight', value);
					break;

				case "opacity":
					_setStyle(currentElement, 'opacity', value);
					break;

				case "shadow":
					value = value == 10 ? '' :'rgba(0,0,0,0.7) 5px 5px '+value+'px';
					_setStyle(currentElement, 'shadow', value);
					break;

				case "radius":
					if(currentElement.type == "curvedText") _setStyle(currentElement, 'radius', value);
					break;

				case "spacing":
					if(currentElement.type == "curvedText") _setStyle(currentElement, 'spacing', value);
					break;
			}

			stage.renderAll();
			designStudio.setTextImageDetails();
		}   

		designStudio.changeObjectProperty = function(property){
			if (currentElement.type === "path") return;
			switch(property){
				case "flipX":
				_setStyle(currentElement, "flipX", !currentElement.getFlipX());
				break;

				case "flipY":
				_setStyle(currentElement, "flipY", !currentElement.getFlipY());
				break;
			}

			stage.renderAll();
			designStudio.setTextImageDetails();
			_pushToHistory(saveThis);
		}

		designStudio.changeImageProperty = function(property, value){
			if(currentElement.type !== "image") return;
			switch(property) {
				case "color":
					_changeImageColor(currentElement, value);
					_pushToHistory(saveThis);
					break;

				case "opacity":
					_setStyle(currentElement, 'opacity', value);
					break;
			}
			designStudio.setTextImageDetails();
		}

		designStudio.changePathProperty = function(property, value){
			switch(property) {
				case "fill":
					$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
						var sidePath = _getPathByName(side, currentElement.name);
						if(sidePath) _setStyle(sidePath, 'fill', value);
					});
					_pushToHistory(saveThis);
					designStudio.modifyAddons('add', 'productFill', {name: currentElement.name, src: value});
					break;

				case "texture":
					$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
						var sidePath = _getPathByName(side, currentElement.name);
						if(sidePath) _setPattern(sidePath, value);
					});
					_pushToHistory(saveThis);
					designStudio.modifyAddons('add', 'productTexture', {name: currentElement.name, src: value});
					break;

				case "pattern":
					$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
						var sidePath = _getPathByName(side, currentElement.name);
						if(sidePath) _setPattern(sidePath, value);
					});
					_pushToHistory(saveThis);
					designStudio.modifyAddons('add', 'productPattern', {name: currentElement.name, src: value});
					break;

				
					/*case "left":
					_setStyle(currentElement, 'left', value);
					break;

				case "top":
					_setStyle(currentElement, 'top', value);
					break;

				case "height":
					currentElement.scaleToHeight(value);
					break;*/
				
			}
			designStudio.updatePrice();
			stage.renderAll();
			setTimeout(designStudio.setPreviewImages, 500);
			designStudio.setTextImageDetails();
		}

		designStudio.toggleCurve = function(){
			var defaultText = params.text = currentElement.getText();
			params = currentElement.params;
			params.curved = currentElement.type == 'i-text';
			stage.remove(currentElement);
			designStudio.addElement('text', defaultText, params);
		}

		designStudio.textCurve = function(state){
			var defaultText = params.text = currentElement.getText();
			params = currentElement.params;
			params.curved = state;
			stage.remove(currentElement);
			designStudio.addElement('text', defaultText, params);
		}

		var _getTextWordArImage = function(iCurve, iOffset, iHeight, iBottom, iTriangle, iText, ifont) {
			/*var demo = document.createElement('canvas');
			var att = document.createAttribute('id');
			att.value = "demo";
			demo.setAttributeNode(att);
			demo.width = 200;
			demo.height = 100;*/
			var font = '64px '+ifont
			var ctx = demo.getContext('2d'),
			    font = font,
			    w = demo.width,
			    h = demo.height,
			    curve,
			    offsetY,
			    bottom,
			    textHeight,
			    isTri = false,
			    dltY,
			    angleSteps = 180 / w,
			    i = w,
			    y,
			    os = document.createElement('canvas'),
			    octx = os.getContext('2d');

			os.width = w;
			os.height = h;

			octx.font = font;
			octx.textBaseline = 'top';
			octx.textAlign = 'center';

			function renderBridgeText(iCurve, iOffset, iHeight, iBottom, iTriangle, iText) {

			    curve = parseInt(iCurve, 10);
			    offsetY = parseInt(iOffset, 10);
			    textHeight = parseInt(iHeight, 10);
			    bottom = parseInt(iBottom, 10);
			    isTri = iTriangle;

			    octx.clearRect(0, 0, w, h);
			    ctx.clearRect(0, 0, w, h);

			    octx.fillText(iText.toUpperCase(), w * 0.5, 0, 100);

			    /// slide and dice
			    i = w;
			    dltY = curve / textHeight;
			    y = 0;
			    while (i--) {
			        if (isTri) {
			            y += dltY;
			            if (i === (w * 0.5)|0) dltY = -dltY;
			        } else {
			            y = bottom - curve * Math.sin(i * angleSteps * Math.PI / 180);
			        }
			        ctx.drawImage(os, i, 0, 1, textHeight,
			        i, h * 0.5 - offsetY / textHeight * y, 1, y);
			    }
			    return demo.toDataURL();
			}

	    return renderBridgeText(iCurve, iOffset, iHeight, iBottom, iTriangle, iText);
		};

		designStudio.previewTextWordArt = function(params){
			var defaultText = params.text;
			var defaultFont = params.font;
			if(!defaultText) return false;
			//var fontFamily = currentElement.getFontFamily();
			//stage.remove(currentElement);

			var wordArt;
			var wordArtObj = {};
			switch(params.type) {
				case "concave-both":
					//wordArtObj = (iCurve: 150, iOffset: 30, iHeight: 90, iBottom: 200, iTriangle: false);
					
					wordArt = _getTextWordArImage(150, 30, 90, 200, false, defaultText, defaultFont);
					break;
				case "triangle":
					//wordArtObj = (iCurve: 100, iOffset: 20, iHeight: 90, iBottom: 200, iTriangle: true);
					wordArt = _getTextWordArImage(100, 20, 90, 200, true, defaultText, defaultFont);
					break;
				case "concave-top":
					//wordArtObj = (iCurve: 100, iOffset: 50, iHeight: 90, iBottom: 130, iTriangle: false);
					wordArt = _getTextWordArImage(100, 50, 90, 130, false, defaultText, defaultFont);
					break;
				case "concave-bottom":
					//wordArtObj = (iCurve: 110, iOffset: 0, iHeight: 64, iBottom: 135, iTriangle: false);
					wordArt = _getTextWordArImage(110, 0, 64, 135, false, defaultText, defaultFont);
					break;

				case "curve-bottom":
					//wordArtObj = (iCurve: 110, iOffset: 0, iHeight: 64, iBottom: 135, iTriangle: false);
					wordArt = _getTextWordArImage(57, 100, 100, 85, false, defaultText, defaultFont);
					//wordArt = _getTextWordArImage(114, 94, 166, 200, false, defaultText);
					break;

				case "curve-top":
					//wordArtObj = (iCurve: 110, iOffset: 0, iHeight: 64, iBottom: 135, iTriangle: false);
					//wordArt = _getTextWordArImage(110, 0, 64, 135, false, defaultText);
					wordArt = _getTextWordArImage(60, -43, 100, 91, false, defaultText, defaultFont);
					break;
			}
			return true;
			//designStudio.addImage(wordArt, {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
			//console.log(_getTextWordArImage());
		};

		designStudio.addWordArt = function(){
			designStudio.addImage(demo.toDataURL(), {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
		}

		function getRandomNoArray(min, max, no){
			var list = [];
			for(var i=1; i<=no ; i++){
				list[list.length] = Math.floor(Math.random() * (min - max + 1)) + max;
			}
			return list;
		}

		function getRandomNo(min, max){
			return Math.floor(Math.random() * (min - max + 1)) + max;
		}

		function attachNo(listSplitted){
			var list = [];
			$.each(listSplitted, function(i, word){
				var length = list.length;
				list[length] = [];
				list[length].push(word);
				list[length].push(getRandomNo(15, 40));
			});
			return list;
		}

		function array_count_values(arrayList){
			var total = {};
			for(var i = 0; i< arrayList.length; i++){
				total[arrayList[i]] = total[arrayList[i]] ? total[arrayList[i]]+1 : 1;
			}
			return total;
		}

		function maxValue(object){
			var maxKey, maxValue, maxObject = {};

			for(var i in object){
				maxKey = i;
				maxValue = object[i];
				break;
			}

			for(var i in object){
				if(object[i] > maxValue){
					maxValue = object[i];
					maxKey = i;
				}
			}

			return maxKey;
		}

		designStudio.generateWordCloud = function(params){
			if(params.list.split(',').length == 0) return false;
			var propHeight = 0.6;
			if(params.shape == "square") propHeight = 0.4 
			var newWidth = stage.getWidth()*0.4;
			var newHeight = stage.getHeight()*propHeight;
			
			var listSplitted = params.list.split(',');

			var list = attachNo(listSplitted);
			list.unshift([maxValue(array_count_values(listSplitted)), 60]);

			$wordcloudCanvas = $('#wordcloud_canvas');
			$wordcloudCanvas.attr("width", newWidth).attr("height", newHeight);
			WordCloud($wordcloudCanvas[0], {
				backgroundColor: 'transparent',
				color: '#000000', 
				fontFamily: params.fontFamily,
				rotateRatio: 0.5, 
				list: list,
			});
			return true;
		};

		$("#generate-svg").on("click", function(){
	    var filedata=stage.toSVG(); // the SVG file is now in filedata
	 
	    var locfile = new Blob([filedata], {type: "image/svg+xml;charset=utf-8"});
	    var locfilesrc = URL.createObjectURL(locfile);//mylocfile);
	 
	    var dwn = document.getElementById('dwn');
	    dwn.innerHTML = "<a href=" + locfilesrc + " download='mysvg.svg'>Download SVG</a>";

		});

		$("#generate-image").on("click", function(){
			console.log(stage.toDataURL());
		});

		designStudio.addWordCloud = function(){
			var currActiveObject = stage.getActiveObject();
			var cloudImg = $wordcloudCanvas[0].toDataURL();

			if(currActiveObject && currActiveObject.type == "path"){
				designStudio.changePathProperty('pattern', cloudImg);
			}else{
				designStudio.addImage(cloudImg, {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
			}
		};
		
		designStudio.changeAllSidesColor = function(color){
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				window[side+"Base"].filters.push(new fabric.Image.filters.Tint({color: color, opacity: 1}));
				try {
					var currentSide = window[side];
					//console.log(side+"Base - "+window[side+"Base"]);
					window[side+"Base"].applyFilters(currentSide.renderAll.bind(currentSide));
				}
				catch(evt) {
					alert("Tshirt cannot be color. Please check if image is hosted under same domain");
				}
			});
			setTimeout(function(){designStudio.setPreviewImages()}, 200);
		}

		designStudio.reset = function(){
			$(container).children('div').each(function(){
				var $productContainer = $(this);
				var currentSide = $productContainer.data('side');
				window[currentSide].clear();
				_addDefaultParams($productContainer.data('params'), currentSide);
			})
		}

		designStudio.undo = function(){
			if(currentState == 0) return;
			saveThis = false;
			--currentState;
			design = $.fn.designStudio.defaultOptions.sidesHistory[currentState];
			_init();
		}

		designStudio.redo = function(){
			if($.fn.designStudio.defaultOptions.sidesHistory.length == 0 || currentState == $.fn.designStudio.defaultOptions.sidesHistory.length-1) return;
			saveThis = false;
			++currentState;
			design = $.fn.designStudio.defaultOptions.sidesHistory[currentState];
			_init();      
		}

		designStudio.changeProduct = function(){
			$.fn.designStudio.defaultOptions.sidesHistory = [];
			_init();
		}

		//designStudio.getTextImages

		designStudio.getPreviewImages = function(){
			var previewImages =  {};
			var currActiveObject = stage.getActiveObject();
			//front: front.deactivateAll().renderAll().toDataURL(), back: back.deactivateAll().renderAll().toDataURL(), left: left.deactivateAll().renderAll().toDataURL(), right: right.deactivateAll().renderAll().toDataURL()
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				previewImages[side] = window[side].deactivateAll().renderAll().toDataURL();
			})
			if(currActiveObject) stage.setActiveObject(currActiveObject);
			return previewImages;
		}

		designStudio.setPreviewImages = function(){
			var previewImages = designStudio.getPreviewImages();
			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				$("#"+side+"-preview").attr("src", previewImages[side]);
			})
		}

		designStudio.getJSONDesign = function(){
			var JSONDesign = {};
			var currActiveObject = stage.getActiveObject();

			$.each($.fn.designStudio.defaultOptions.sides, function(i, side){
				var currentStage = window[side];

				var stringCanvas = JSON.stringify(currentStage);
				var parseCanvas = JSON.parse(stringCanvas);

				var currentCanvasObject = currentStage.getObjects();

				//dd(currentCanvasObject);
				$.each(parseCanvas.objects, function(i, obj){
					obj.hasControls      = currentCanvasObject[i].hasControls;
					obj.hasRotatingPoint = currentCanvasObject[i].hasRotatingPoint;
					obj.isRemovable      = currentCanvasObject[i].isRemovable;
					obj.lockMovementX    = currentCanvasObject[i].lockMovementX;
					obj.lockMovementY    = currentCanvasObject[i].lockMovementY;
					obj.lockRotation     = currentCanvasObject[i].isRemovable;
					obj.lockScalingX     = currentCanvasObject[i].lockScalingX;
					obj.lockScalingY     = currentCanvasObject[i].lockScalingY;
					obj.name             = currentCanvasObject[i].name
					obj.originParams     = currentCanvasObject[i].originParams;
					obj.params           = currentCanvasObject[i].params;
					obj.zIndex           = i;
				});
				JSONDesign[side] = JSON.stringify(parseCanvas);
			});
			if(currActiveObject) stage.setActiveObject(currActiveObject);
			return JSONDesign;
		}
		/* Public Methods Ended */

		/* Helpers Started */
		function cl(data){
			console.log(data);
		}

		function dd(data){
			cl(JSON.stringify(data, null, 4));
		}

		function array_combine(keys, values) {
		  //  discuss at: http://phpjs.org/functions/array_combine/
		  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		  // improved by: Brett Zamir (http://brett-zamir.me)
		  //   example 1: array_combine([0,1,2], ['kevin','van','zonneveld']);
		  //   returns 1: {0: 'kevin', 1: 'van', 2: 'zonneveld'}

		  var new_array = {},
		    keycount = keys && keys.length,
		    i = 0;

		  // input sanitation
		  if (typeof keys !== 'object' || typeof values !== 'object' || // Only accept arrays or array-like objects
		    typeof keycount !== 'number' || typeof values.length !== 'number' || !keycount) { // Require arrays to have a count
		    return false;
		  }

		  // number of elements does not match
		  if (keycount != values.length) {
		    return false;
		  }

		  for (i = 0; i < keycount; i++) {
		    new_array[keys[i]] = values[i];
		  }

		  return new_array;
		}

		/* Helpers Ended */
	}

	$.fn.designStudio = function( options ){
		return this.each(function(){
			var element = $(this);
			if(element.data('design-studio')) return;
			var designStudioInstance = new designStudio(this, options);
			element.data('design-studio', designStudioInstance);
		});
	}

	// Default Parameters
	$.fn.designStudio.defaultOptions = {
		sides: [],
		sidesHistory: [],
		price: {
			base: 10,
			text: 3,
			image: 5,
			productFill: 6,
			productTexture: 7,
			productPattern: 8 
		},
		addons: {
			text: 0,
			image: 0,
			productFill: {},
			productTexture: {},
			productPattern: {}
		},
		dimensions: {
			productDisplayWidth: 369,
			productDisplayHeight: 430,
			productStageWidth: 369,
			productStageHeight: 430,
		},
		elementParameters: {
			x: 0,
			y: 0,
			z: -1,
			colors: false,
			removable: false,
			draggable: false, 
			rotatable: false,
			resizable: false, 
			zChangeable: false,
			scale: 1,
			degree: 0,
			price: 0,
			boundingBox: false,
			autoCenter: false,
			opacity: 1,
			originX: 'center',
			originY: 'center',
			replace: '',
			boundingBoxClipping: false,
			autoSelect: false,
			topped: false,
			uploadZone: false
		},
		textParameters: {
			font: false,
			fontWeight: '',
			fontStyle: '',
			textSize: 30,
			patternable: false,
			editable: true,
			lineHeight: 1,
			textAlign: 'left',
			textBackgroundColor: '',
			textDecoration: '',
			maxLength: 0,
			curved: false,
			curvable: false,
			curveSpacing: 10,
			curveRadius: 70,
			curveReverse: false
		},
		defaultTextParameters: {
			lot: {
				x: 550,
				y: 100,
				text: "Lotto 1/11"
			}
		},
		customImagesParameters: {
			minW: 50,
			minH: 100,
			maxW: 300,
			maxH: 400,
			resizeToW: 300,
			resizeToH: 400,
		}
	}

})(jQuery);
