# Design Studio - Pro Version #

Design Studio is a tool to customise any product. It provides the functionality to enhance a product by adding texts, graphics, cliparts and custom uploaded images. It customizes the appearance of product by changing its color and applying different textures and patterns.

Design Studio has been developed using FabricJS. It is a HTML5 canvas library which provides interactive object model on top of canvas element.

# Documentation:
To quickly get started, include `design-studio.js` and `fabric.js` (in same order). In the package, these files are already included in `assets/lib` directory.

    <script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>
    <script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>

Create an HTML element containing the configuration of your product:
    
    <div id="design-studio">
        <!-- Configuration of front side -->
        <div data-side="front" data-params='{"preview": "assets/img/products/tshirt-06/front-preview.png", "base": "assets/img/products/tshirt-06/front-base.png"}'></div>

        <!-- Configuration of back side -->
        <div class="hide" data-side="back" data-params='{"preview": "assets/img/products/tshirt-06/back-preview.png", "base": "assets/img/products/tshirt-06/back-base.png"}'></div>
    </div>



`data-side` represents the side of the product. `data-params` contains a JSON object having `preview` and `base` image of the product for that side. Both images must be of same dimensions. 

Preview image is shown when the product is loaded first time into the design tool. Base image is the shadow layer of the product. 

Initialize the plugin by invoking `designStudio` method on the container element. Pass a parameter object defining your custom dimensions.

    var $stageContainer = $("#design-studio"); // element on which design-studio will be initialized
    var designStudio = $stageContainer.designStudio({dimensions: {productDisplayWidth: $stageContainer.width(), productStageWidth: 500, productStageHeight: 500}}).data('design-studio'); // plugin initialization

`parameter object` contains a `dimensions` property object which has 3 main properties:

 - `productDisplayWidth` defines the width size of your stage that will be shown in the browser. Make sure to wrap it in a responsive element. Height is adjusted automatically in proportion to the available width.

 - `productStageWidth` and `productStageHeight` represents the dimensions of the final output(png).

`data('design-studio')` is a callback function which gives you access to various methods and functionality of the design tool.


### Methods
Design Tool provides lot of methods to customize the product:

 - `addText(text, params)`
    - First argument is the text that will be added. Second argument is an object containing text properties. 
    - Example: designStudio.addText('New Text', {font: font, autoCenter: true, editorMode: true, draggable: true, autoSelect: true});



 - `addImage(base64Image, params)`
    - First argument is the image that will be added. It could be base64 encoded image or source path of the image. Second argument is an object containing image properties. 
    - Example: designStudio.addImage($(this).attr("src"), {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});

 - `changeProductProperty('fill', colorCode)`
    - Changes color of the product. 
    - colorCode is the hexcode of the color that is applied to the product. 
    - Example: designStudio.changeProductProperty('fill', '#DDDDDD);


 - `changeTextProperty(propertyName, propertyValue)`
    - First argument represents the property name and second argument represents the property value. Second argument is required for font-family and font-fill
    - Example: designStudio.changeTextProperty('font-family', 'Arial);
    - Various property available are font-family, font-fill, bold, italic, underline, align-left, align-center, align-right

 - `reset()`
    - To reset the stage
    - Example: designStudio.reset();

 - `changeSide(side)`
    - To change the side of the product into the design tool
    - `side` arguments expects a string of side that needs to be shown into the design tool"
    - Example: designStudio.changeSide("front");


 - `changeProduct()`
    - To load a new product into the design tool
    - Call the `changeProduct` method after replacing old configuration setting under the container element.
    - Example: $("#design-studio").html($("#new-product-specs").html()); designStudio.changeProduct();

 - `getPreviewImages()`
    - A helper method to get base64 images of designed product
    - Example: designStudio.getPreviewImages();

### Implementation
A responsive template is provided with the design-tool to quickly get you started with the design-tool. `index.js` shows the implementation of the design tool. This template has some dependencies:

- Bootstrap
- Bootstrap-Confirmation
- Ionicons
- Google Fonts
- mCustomScrollbar

___

# Pro version includes:

### Text Options

* Add/Delete
* Font Family
* Color
* Modify - Bold, Italic, Underline, Alignments
* Line Height
* Opacity
* Stroke Color
* Overline, Line-Through
* Text Shadow
* Curved Text
* Pattern



### Image Options

* Add from gallery
* Upload
* Show uploaded images in gallery
* Color Images
* Opacity



### Tools

* Flip horizontal
* Flip vertical
* Reset
* Save
* Social Sharing Options



### Features

* Responsive Canvas
* Select different products
* Layering
* Zoom In
* Zoom Out
* Edit each part of product (coordinates) - Can be colored
* Front / Back View / Side View / 3D view
* Undo/Redo
* Save/Edit
* Edit each part of product (coordinates) - Can be patterned
* Pan-Tool




# This repo contains following versions of design tool pro ###


* [Mobile](http://bwcmultimedia.com/dsb/pro/mobile)
* [Tshirt](http://bwcmultimedia.com/dsb/pro/tshirt)
* [Tshirt](http://bwcmultimedia.com/dsb/pro/mug)
* [Tshirt](http://bwcmultimedia.com/dsb/pro/apparel)