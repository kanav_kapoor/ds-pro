<?php require_once('db/load-product.php'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DS Pro - Mobile</title>

		<script type="text/javascript">
			var design = <?php echo json_encode($design); ?>;
		</script>
		
		<!-- CSS -->
		<!-- plugins -->
		<link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
		<link href='http://fonts.googleapis.com/css?family=Josefin+Sans|Shadows+Into+Light|Pacifico|Quicksand|Architects+Daughter|Dancing+Script|Chewy|Gloria+Hallelujah|Shadows+Into+Light|Poiret+One|Indie+Flower|Oswald|' rel='stylesheet' type='text/css'>
		<!-- <link rel='stylesheet' href='assets/css/google-fonts.css'> -->
		<link href="assets/lib/nouislider/nouislider.min.css" media="all" rel="stylesheet" type="text/css" />

		<link rel="stylesheet" href="assets/lib/bootstrap-switch/bootstrap-switch.min.css">
		<!--/ plugins -->

		<link rel="stylesheet" href="assets/css/menu.css">
		<link rel="stylesheet" href="assets/css/media.css">
		<link rel="stylesheet" href="assets/css/index.css">
		<!--/ CSS -->
	</head>
	
	<body>
		<div id="loader">
			<p class="text-center"><br>Loading.... Please wait...</p>
		</div>
		<!-- header -->
		<header>
			<!-- Nav -->
			<nav class="animate focus">
				<ul class="nav nav-pills nav-stacked tool-options" role="tablist">
					<li role="presentation" class="active"><a class="text-center" title="Products" data-toggle="tab" aria-controls="tabpane-products" href="#tabpane-products"><i class="fa fa-th"></i></a></li>
					<li role="presentation"><a class="text-center" title="Text Options" data-toggle="tab" aria-controls="tabpane-text-options" href="#tabpane-text-options"><i class="fa fa-pencil"></i></a></li>
					<li role="presentation"><a class="text-center" title="Image Options" data-toggle="tab" aria-controls="tabpane-image-options" href="#tabpane-image-options"><i class="fa fa-picture-o"></i></a></li>
					<li role="presentation"><a class="text-center" title="Price Calculations" data-toggle="tab" aria-controls="tabpane-price-calculation-options" href="#tabpane-price-calculation-options"><i class="fa fa-cart-plus"></i></a></li>
				</ul>
			</nav>
			<!--/ Nav -->
			<div class="nav-controller hidden">
				<span class="[ ion-2x ion-navicon-round ] controller-open"></span>
				<span class="[ ion-2x ion-ios-close-outline ] controller-close"></span>
			</div>
		</header>
		<!--/ header -->		

		<!--  Main Container -->
		<div class="content-wrapper container-fluid">
			<div class="row">	

				<!-- Tools -->
				<div class="row tools">
					<div class="col-sm-14 col-md-14 col-sm-offset-10 col-md-offset-10">
						<div class="row">
							<div class="col-sm-14 col-md-18">
								<div class="col-sm-24">
									<div class="btn-group btn-group-justified" role="group" aria-label="">
										<div class="btn-group" role="group">
											<button title="Save" type="button" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-default" id="saveProduct"><i class="ion-2x ion-ios-heart-outline"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Flip Horizontally" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Horizontally" class="btn btn-default" id="flipX"><i class="ion-2x ion-ios-arrow-thin-right"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Flip Vertically" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Vertically" class="btn btn-default" id="flipY"><i class="ion-2x ion-ios-arrow-thin-up"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Reset" type="button" data-toggle="confirmation" class="btn btn-default" id="reset"><i class="ion-2x ion-ios-loop"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Undo" type="button" data-toggle="tooltip" data-placement="bottom" title="Undo" class="btn btn-default" id="undo"><i class="ion-2x ion-ios-undo-outline"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Redo" type="button" data-toggle="tooltip" data-placement="bottom" title="Redo" class="btn btn-default" id="redo"><i class="ion-2x ion-ios-redo-outline"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ Tools -->
				
				<!-- Design Tool -->
				<div class="row">

					<!-- Left -->
					<div class="col-sm-10 col-md-10">
						<div class="col-sm-18 col-md-12">
							<!-- Tab Panes -->
							<div class="tab-content tool-panes">
								<!-- Products -->
								<div role="tabpanel" class="products tab-pane fade in active" id="tabpane-products">
									<p class="custom-pane-header text-center h3">Products</p><hr>
									<input type="search" placeholder="Search Products" class="form-control" id="search-products">
									<div class="row display-gallery products-list">
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/galaxy-s6/front-thumbnail.png" alt="Black Color Samsung Galaxy " title="Black Color Samsung Galaxy ">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/galaxy-s6/front-preview.png", "base": "assets/img/products/galaxy-s6/front-base.png", "paths": [{"name": "base-front", "path": "M 101.00,50.11 C 101.00,50.11 171.00,50.11 171.00,50.11 171.00,50.11 370.00,50.11 370.00,50.11 377.09,50.09 381.65,51.73 388.00,54.78 405.48,63.18 414.97,76.52 415.00,96.00 415.00,96.00 415.00,435.00 415.00,435.00 415.00,435.00 415.00,652.00 415.00,652.00 415.00,652.00 415.00,761.00 415.00,761.00 414.80,777.78 405.24,802.29 390.00,810.65 383.76,814.08 375.04,814.99 368.00,815.00 368.00,815.00 176.00,815.00 176.00,815.00 176.00,815.00 69.00,815.00 69.00,815.00 39.14,814.65 17.05,794.30 17.00,764.00 17.00,764.00 17.00,620.00 17.00,620.00 17.00,620.00 16.00,605.00 16.00,605.00 16.00,605.00 16.00,408.00 16.00,408.00 16.00,408.00 16.00,315.00 16.00,315.00 16.00,315.00 15.06,304.00 15.06,304.00 15.06,304.00 15.06,291.00 15.06,291.00 15.06,291.00 15.06,283.00 15.06,283.00 15.06,283.00 15.06,151.00 15.06,151.00 15.06,151.00 15.06,98.00 15.06,98.00 15.03,79.77 27.12,64.91 43.00,56.97 49.62,53.66 56.76,51.78 64.00,50.11 64.00,50.11 101.00,50.11 101.00,50.11 Z M 314.00,78.61 C 322.07,76.79 325.76,67.54 319.61,61.39 316.27,58.05 312.38,57.82 308.00,58.42 292.27,64.06 301.03,81.53 314.00,78.61 Z M 161.31,73.26 C 163.77,75.29 167.98,75.00 171.00,75.00 171.00,75.00 264.00,75.00 264.00,75.00 274.39,74.80 275.58,67.38 271.57,63.51 268.66,60.70 263.75,61.01 260.00,61.11 260.00,61.11 199.00,61.11 199.00,61.11 199.00,61.11 166.00,61.11 166.00,61.11 158.56,63.54 157.17,69.84 161.31,73.26 Z M 122.95,73.67 C 127.84,72.56 129.66,65.17 121.99,62.91 112.13,64.04 115.33,75.39 122.95,73.67 Z M 139.94,73.67 C 145.01,72.51 145.94,63.95 138.00,62.77 128.61,65.53 132.89,75.28 139.94,73.67 Z M 401.00,259.00 C 401.00,259.00 401.00,141.00 401.00,141.00 401.00,141.00 401.00,114.00 401.00,114.00 401.00,112.01 401.23,108.19 399.98,106.60 398.41,104.62 394.30,105.00 392.00,105.00 392.00,105.00 115.00,105.00 115.00,105.00 115.00,105.00 51.00,105.00 51.00,105.00 47.42,105.00 34.03,104.07 32.02,106.60 30.77,108.19 31.00,112.01 31.00,114.00 31.00,114.00 31.00,764.00 31.00,764.00 31.00,764.00 401.00,764.00 401.00,764.00 401.00,764.00 401.00,259.00 401.00,259.00 Z M 178.37,792.00 C 179.82,803.06 190.64,805.99 200.00,806.04 200.00,806.04 215.00,806.04 215.00,806.04 215.00,806.04 227.00,807.00 227.00,807.00 227.00,807.00 250.00,807.00 250.00,807.00 260.44,806.98 270.99,804.57 271.90,792.00 272.89,778.51 260.57,773.06 249.00,773.16 249.00,773.16 194.00,773.16 194.00,773.16 185.63,775.34 177.12,782.51 178.37,792.00 Z", "x": 108, "y": 23, "height": 354,"pattern":"assets/img/patterns/pattern-8.png"}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/galaxy-s6/back-preview.png", "base": "assets/img/products/galaxy-s6/back-base.png", "paths": [{"name": "base-front", "path": "M 90.00,50.12 C 90.00,50.12 160.00,50.12 160.00,50.12 160.00,50.12 362.00,50.12 362.00,50.12 388.92,50.32 417.96,70.29 418.00,99.00 418.00,99.00 418.00,263.00 418.00,263.00 418.00,263.00 417.00,278.00 417.00,278.00 417.00,278.00 417.00,426.00 417.00,426.00 417.00,426.00 417.00,607.00 417.00,607.00 417.00,607.00 416.00,622.00 416.00,622.00 416.00,622.00 416.00,764.00 416.00,764.00 415.67,791.96 395.38,813.96 367.00,814.00 367.00,814.00 54.00,814.00 54.00,814.00 24.91,813.95 12.04,780.97 12.00,756.00 12.00,756.00 12.00,220.00 12.00,220.00 12.00,220.00 12.00,98.00 12.00,98.00 12.04,72.47 28.77,55.84 53.00,50.12 53.00,50.12 90.00,50.12 90.00,50.12 Z M 198.00,131.00 C 198.00,131.00 198.00,167.00 198.00,167.00 198.01,170.51 197.74,174.67 200.01,177.61 202.88,181.33 207.80,180.99 212.00,181.00 212.00,181.00 250.00,181.00 250.00,181.00 254.03,181.00 263.73,181.58 266.94,179.83 271.39,177.39 270.99,173.37 271.00,169.00 271.00,169.00 271.00,135.00 271.00,135.00 271.00,131.47 271.53,123.01 269.83,120.23 267.70,116.74 263.56,117.02 260.00,117.22 260.00,117.22 204.02,117.22 204.02,117.22 197.43,120.21 198.01,124.73 198.00,131.00 Z M 281.00,128.00 C 281.00,128.00 281.00,170.00 281.00,170.00 281.12,178.65 283.45,180.94 292.00,181.00 296.95,181.03 309.24,182.48 311.98,177.85 313.19,175.80 313.00,172.35 313.00,170.00 313.00,170.00 313.00,134.00 313.00,134.00 313.00,130.31 313.74,122.40 311.26,119.65 307.85,115.86 292.98,116.71 288.00,117.39 281.74,119.46 281.09,121.72 281.00,128.00 Z", "x": 106, "y": 23, "height": 354,"pattern":"assets/img/patterns/pattern-8.png"}],"template":[{"type": "text", "params": {"src": "XOXO", "editorMode": true, "draggable": true, "x": 113, "y":128,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 30,"degree":270,"fontPattern":"assets/img/patterns/pattern-2.png"}},{"type": "text", "params": {"src": "LOL", "editorMode": true, "draggable": true, "x": 206, "y":86,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#FEAC1C"], "textSize": 35,"degree":32,"stroke":"#008767"}},{"type": "text", "params": {"src": "ROFL", "editorMode": true, "draggable": true, "x": 213, "y":148,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#C2BEBF"], "textSize": 25,"degree":19,"stroke":"#FEAC1C"}},{"type": "text", "params": {"src": "BRB", "editorMode": true, "draggable": true, "x": 138, "y":171,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#E975A4"], "textSize": 35,"degree":16,"stroke":"#28272C"}},{"type": "text", "params": {"src": "IRL", "editorMode": true, "draggable": true, "x": 110, "y":237,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#85A35D"], "textSize": 35,"degree":330,"stroke":"#B3042B"}},{"type": "text", "params": {"src": "BFF", "editorMode": true, "draggable": true, "x": 191, "y":282,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#85A35D"], "textSize": 35,"degree":332,"stroke":"#B3042B"}},{"type": "text", "params": {"src": "asap", "editorMode": true, "draggable": true, "x": 132, "y":295,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#008767"], "textSize": 35,"degree":12,"stroke":"#C2BEBF"}},{"type": "text", "params": {"src": "gr8", "editorMode": true, "draggable": true, "x": 213, "y":311,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#C2BEBF"], "textSize": 35,"stroke":"#4A5D7B"}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/galaxy-s6/front-thumbnail.png" alt="Black Color Samsung Galaxy" title="Black Color Samsung Galaxy">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/galaxy-s6/front-preview.png", "base": "assets/img/products/galaxy-s6/front-base.png", "paths": [{"name": "base-front", "path": "M 101.00,50.11 C 101.00,50.11 171.00,50.11 171.00,50.11 171.00,50.11 370.00,50.11 370.00,50.11 377.09,50.09 381.65,51.73 388.00,54.78 405.48,63.18 414.97,76.52 415.00,96.00 415.00,96.00 415.00,435.00 415.00,435.00 415.00,435.00 415.00,652.00 415.00,652.00 415.00,652.00 415.00,761.00 415.00,761.00 414.80,777.78 405.24,802.29 390.00,810.65 383.76,814.08 375.04,814.99 368.00,815.00 368.00,815.00 176.00,815.00 176.00,815.00 176.00,815.00 69.00,815.00 69.00,815.00 39.14,814.65 17.05,794.30 17.00,764.00 17.00,764.00 17.00,620.00 17.00,620.00 17.00,620.00 16.00,605.00 16.00,605.00 16.00,605.00 16.00,408.00 16.00,408.00 16.00,408.00 16.00,315.00 16.00,315.00 16.00,315.00 15.06,304.00 15.06,304.00 15.06,304.00 15.06,291.00 15.06,291.00 15.06,291.00 15.06,283.00 15.06,283.00 15.06,283.00 15.06,151.00 15.06,151.00 15.06,151.00 15.06,98.00 15.06,98.00 15.03,79.77 27.12,64.91 43.00,56.97 49.62,53.66 56.76,51.78 64.00,50.11 64.00,50.11 101.00,50.11 101.00,50.11 Z M 314.00,78.61 C 322.07,76.79 325.76,67.54 319.61,61.39 316.27,58.05 312.38,57.82 308.00,58.42 292.27,64.06 301.03,81.53 314.00,78.61 Z M 161.31,73.26 C 163.77,75.29 167.98,75.00 171.00,75.00 171.00,75.00 264.00,75.00 264.00,75.00 274.39,74.80 275.58,67.38 271.57,63.51 268.66,60.70 263.75,61.01 260.00,61.11 260.00,61.11 199.00,61.11 199.00,61.11 199.00,61.11 166.00,61.11 166.00,61.11 158.56,63.54 157.17,69.84 161.31,73.26 Z M 122.95,73.67 C 127.84,72.56 129.66,65.17 121.99,62.91 112.13,64.04 115.33,75.39 122.95,73.67 Z M 139.94,73.67 C 145.01,72.51 145.94,63.95 138.00,62.77 128.61,65.53 132.89,75.28 139.94,73.67 Z M 401.00,259.00 C 401.00,259.00 401.00,141.00 401.00,141.00 401.00,141.00 401.00,114.00 401.00,114.00 401.00,112.01 401.23,108.19 399.98,106.60 398.41,104.62 394.30,105.00 392.00,105.00 392.00,105.00 115.00,105.00 115.00,105.00 115.00,105.00 51.00,105.00 51.00,105.00 47.42,105.00 34.03,104.07 32.02,106.60 30.77,108.19 31.00,112.01 31.00,114.00 31.00,114.00 31.00,764.00 31.00,764.00 31.00,764.00 401.00,764.00 401.00,764.00 401.00,764.00 401.00,259.00 401.00,259.00 Z M 178.37,792.00 C 179.82,803.06 190.64,805.99 200.00,806.04 200.00,806.04 215.00,806.04 215.00,806.04 215.00,806.04 227.00,807.00 227.00,807.00 227.00,807.00 250.00,807.00 250.00,807.00 260.44,806.98 270.99,804.57 271.90,792.00 272.89,778.51 260.57,773.06 249.00,773.16 249.00,773.16 194.00,773.16 194.00,773.16 185.63,775.34 177.12,782.51 178.37,792.00 Z", "x": 108, "y": 23, "height": 354,"texture":"assets/img/textures/white-cotton.jpg"}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/galaxy-s6/back-preview.png", "base": "assets/img/products/galaxy-s6/back-base.png", "paths": [{"name": "base-front", "path": "M 90.00,50.12 C 90.00,50.12 160.00,50.12 160.00,50.12 160.00,50.12 362.00,50.12 362.00,50.12 388.92,50.32 417.96,70.29 418.00,99.00 418.00,99.00 418.00,263.00 418.00,263.00 418.00,263.00 417.00,278.00 417.00,278.00 417.00,278.00 417.00,426.00 417.00,426.00 417.00,426.00 417.00,607.00 417.00,607.00 417.00,607.00 416.00,622.00 416.00,622.00 416.00,622.00 416.00,764.00 416.00,764.00 415.67,791.96 395.38,813.96 367.00,814.00 367.00,814.00 54.00,814.00 54.00,814.00 24.91,813.95 12.04,780.97 12.00,756.00 12.00,756.00 12.00,220.00 12.00,220.00 12.00,220.00 12.00,98.00 12.00,98.00 12.04,72.47 28.77,55.84 53.00,50.12 53.00,50.12 90.00,50.12 90.00,50.12 Z M 198.00,131.00 C 198.00,131.00 198.00,167.00 198.00,167.00 198.01,170.51 197.74,174.67 200.01,177.61 202.88,181.33 207.80,180.99 212.00,181.00 212.00,181.00 250.00,181.00 250.00,181.00 254.03,181.00 263.73,181.58 266.94,179.83 271.39,177.39 270.99,173.37 271.00,169.00 271.00,169.00 271.00,135.00 271.00,135.00 271.00,131.47 271.53,123.01 269.83,120.23 267.70,116.74 263.56,117.02 260.00,117.22 260.00,117.22 204.02,117.22 204.02,117.22 197.43,120.21 198.01,124.73 198.00,131.00 Z M 281.00,128.00 C 281.00,128.00 281.00,170.00 281.00,170.00 281.12,178.65 283.45,180.94 292.00,181.00 296.95,181.03 309.24,182.48 311.98,177.85 313.19,175.80 313.00,172.35 313.00,170.00 313.00,170.00 313.00,134.00 313.00,134.00 313.00,130.31 313.74,122.40 311.26,119.65 307.85,115.86 292.98,116.71 288.00,117.39 281.74,119.46 281.09,121.72 281.00,128.00 Z", "x": 106, "y": 23, "height": 354,"texture":"assets/img/textures/white-cotton.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/calavera-01.png","editorMode": true, "draggable": true, "resizeToW": 117, "x": 133, "y": 325,"degree":270}},{"type": "image", "params": {"src": "assets/img/sample/calavera-02.png","editorMode": true, "draggable": true, "resizeToW": 110, "x": 137, "y": 225,"degree":270}},{"type": "text", "params": {"src": "TILL DEATH DO US PART", "editorMode": true, "draggable": true, "x": 261, "y":304,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#D1316F"], "textSize": 16,"stroke":"#4A5D7B","degree":270}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/iphone-6/front-thumbnail.png" alt="White Color Iphone" title="White Color Iphone">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/iphone-6/front-preview.png", "base": "assets/img/products/iphone-6/front-base.png", "paths": [{"name": "base-front", "path": "M 110.00,49.11 C 110.00,49.11 169.00,49.11 169.00,49.11 169.00,49.11 346.00,49.11 346.00,49.11 371.80,49.04 396.96,71.67 397.00,98.00 397.00,98.00 397.00,648.00 397.00,648.00 397.00,648.00 397.00,765.00 397.00,765.00 396.95,795.17 374.16,813.95 345.00,814.00 345.00,814.00 271.00,814.00 271.00,814.00 271.00,814.00 256.00,815.00 256.00,815.00 256.00,815.00 178.00,815.00 178.00,815.00 178.00,815.00 163.00,816.00 163.00,816.00 163.00,816.00 84.00,816.00 84.00,816.00 54.18,815.95 30.05,790.56 30.00,761.00 30.00,761.00 30.00,273.00 30.00,273.00 30.00,273.00 30.00,103.00 30.00,103.00 30.04,75.41 51.80,54.34 78.00,49.11 78.00,49.11 110.00,49.11 110.00,49.11 Z M 213.99,79.72 C 221.56,78.97 221.89,67.80 212.04,67.54 203.82,70.76 207.02,80.41 213.99,79.72 Z M 159.00,104.76 C 169.10,106.29 171.69,94.21 165.77,89.74 163.27,87.85 160.89,88.07 158.02,88.43 149.02,92.16 150.64,103.49 159.00,104.76 Z M 188.15,99.98 C 190.18,101.19 193.67,101.00 196.00,101.00 196.00,101.00 232.00,101.00 232.00,101.00 235.42,100.95 241.92,100.44 240.16,95.13 239.11,91.99 235.71,92.05 233.00,92.22 233.00,92.22 189.11,92.22 189.11,92.22 185.44,94.20 184.02,97.50 188.15,99.98 Z M 43.00,134.00 C 43.00,134.00 43.00,593.00 43.00,593.00 43.00,593.00 43.00,700.00 43.00,700.00 43.00,700.00 43.00,724.00 43.00,724.00 43.01,725.82 42.85,728.92 44.02,730.40 45.59,732.38 49.70,732.00 52.00,732.00 52.00,732.00 305.00,732.00 305.00,732.00 305.00,732.00 363.00,732.00 363.00,732.00 366.48,732.00 378.99,732.92 380.98,730.40 382.23,728.81 382.00,724.99 382.00,723.00 382.00,723.00 382.00,134.00 382.00,134.00 382.00,134.00 43.00,134.00 43.00,134.00 Z M 192.22,759.00 C 182.05,776.11 193.92,798.50 214.00,798.99 219.02,799.11 222.41,798.92 227.00,796.62 248.04,786.10 246.56,755.58 224.00,747.55 219.42,745.92 215.76,745.83 211.00,746.30 203.14,747.84 196.40,751.98 192.22,759.00 Z", "x": 114, "y": 23, "height": 355,"pattern":"assets/img/patterns/stars-4.jpg"}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/iphone-6/back-preview.png", "base": "assets/img/products/iphone-6/back-base.png", "paths": [{"name": "base-front", "path": "M 109.00,51.00 C 109.00,51.00 168.00,51.00 168.00,51.00 168.00,51.00 349.00,51.00 349.00,51.00 358.50,51.01 361.60,52.49 370.00,56.76 386.67,65.22 397.97,79.93 398.00,99.00 398.00,99.00 398.00,647.00 398.00,647.00 398.00,647.00 398.00,764.00 398.00,764.00 397.96,792.86 376.19,811.96 348.00,812.00 348.00,812.00 318.00,812.00 318.00,812.00 318.00,812.00 301.00,813.00 301.00,813.00 301.00,813.00 216.00,813.00 216.00,813.00 216.00,813.00 201.00,814.00 201.00,814.00 201.00,814.00 117.00,814.00 117.00,814.00 117.00,814.00 106.00,814.98 106.00,814.98 106.00,814.98 89.00,814.98 89.00,814.98 59.83,814.95 33.05,792.06 33.00,762.00 33.00,762.00 33.00,308.00 33.00,308.00 33.00,308.00 33.00,102.00 33.00,102.00 33.04,76.09 53.30,57.83 77.00,51.52 77.00,51.52 109.00,51.00 109.00,51.00 Z M 78.79,81.09 C 68.29,94.62 76.44,114.56 94.00,114.99 97.56,115.07 100.69,115.10 104.00,113.59 123.99,104.45 118.25,72.83 94.00,73.33 87.78,74.28 82.83,75.88 78.79,81.09 Z M 174.00,106.45 C 188.96,103.21 186.46,80.45 169.00,82.40 151.71,87.96 160.10,109.47 174.00,106.45 Z M 137.00,101.67 C 146.42,103.66 149.47,89.33 140.00,87.47 138.19,86.95 137.65,87.31 136.02,87.47 128.10,92.07 130.85,100.37 137.00,101.67 Z", "x": 116, "y": 23, "height": 354,"pattern":"assets/img/patterns/stars-4.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/kitty.png","editorMode": true, "draggable": true, "resizeToW": 145, "x": 125, "y": 65}},{"type": "text", "params": {"src": "Hiiiii", "editorMode": true, "draggable": true, "x": 165, "y":237,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FED348"], "textSize": 35}},{"type": "text", "params": {"src": "Everyone", "editorMode": true, "draggable": true, "x": 139, "y":265,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FED348"], "textSize": 35}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/iphone-6/front-thumbnail.png" alt="White Color Iphone" title="White Color Iphone">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/iphone-6/front-preview.png", "base": "assets/img/products/iphone-6/front-base.png", "paths": [{"name": "base-front", "path": "M 110.00,49.11 C 110.00,49.11 169.00,49.11 169.00,49.11 169.00,49.11 346.00,49.11 346.00,49.11 371.80,49.04 396.96,71.67 397.00,98.00 397.00,98.00 397.00,648.00 397.00,648.00 397.00,648.00 397.00,765.00 397.00,765.00 396.95,795.17 374.16,813.95 345.00,814.00 345.00,814.00 271.00,814.00 271.00,814.00 271.00,814.00 256.00,815.00 256.00,815.00 256.00,815.00 178.00,815.00 178.00,815.00 178.00,815.00 163.00,816.00 163.00,816.00 163.00,816.00 84.00,816.00 84.00,816.00 54.18,815.95 30.05,790.56 30.00,761.00 30.00,761.00 30.00,273.00 30.00,273.00 30.00,273.00 30.00,103.00 30.00,103.00 30.04,75.41 51.80,54.34 78.00,49.11 78.00,49.11 110.00,49.11 110.00,49.11 Z M 213.99,79.72 C 221.56,78.97 221.89,67.80 212.04,67.54 203.82,70.76 207.02,80.41 213.99,79.72 Z M 159.00,104.76 C 169.10,106.29 171.69,94.21 165.77,89.74 163.27,87.85 160.89,88.07 158.02,88.43 149.02,92.16 150.64,103.49 159.00,104.76 Z M 188.15,99.98 C 190.18,101.19 193.67,101.00 196.00,101.00 196.00,101.00 232.00,101.00 232.00,101.00 235.42,100.95 241.92,100.44 240.16,95.13 239.11,91.99 235.71,92.05 233.00,92.22 233.00,92.22 189.11,92.22 189.11,92.22 185.44,94.20 184.02,97.50 188.15,99.98 Z M 43.00,134.00 C 43.00,134.00 43.00,593.00 43.00,593.00 43.00,593.00 43.00,700.00 43.00,700.00 43.00,700.00 43.00,724.00 43.00,724.00 43.01,725.82 42.85,728.92 44.02,730.40 45.59,732.38 49.70,732.00 52.00,732.00 52.00,732.00 305.00,732.00 305.00,732.00 305.00,732.00 363.00,732.00 363.00,732.00 366.48,732.00 378.99,732.92 380.98,730.40 382.23,728.81 382.00,724.99 382.00,723.00 382.00,723.00 382.00,134.00 382.00,134.00 382.00,134.00 43.00,134.00 43.00,134.00 Z M 192.22,759.00 C 182.05,776.11 193.92,798.50 214.00,798.99 219.02,799.11 222.41,798.92 227.00,796.62 248.04,786.10 246.56,755.58 224.00,747.55 219.42,745.92 215.76,745.83 211.00,746.30 203.14,747.84 196.40,751.98 192.22,759.00 Z", "x": 114, "y": 23, "height": 355,"texture":"assets/img/textures/white-black.jpg"}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/iphone-6/back-preview.png", "base": "assets/img/products/iphone-6/back-base.png", "paths": [{"name": "base-front", "path": "M 109.00,51.00 C 109.00,51.00 168.00,51.00 168.00,51.00 168.00,51.00 349.00,51.00 349.00,51.00 358.50,51.01 361.60,52.49 370.00,56.76 386.67,65.22 397.97,79.93 398.00,99.00 398.00,99.00 398.00,647.00 398.00,647.00 398.00,647.00 398.00,764.00 398.00,764.00 397.96,792.86 376.19,811.96 348.00,812.00 348.00,812.00 318.00,812.00 318.00,812.00 318.00,812.00 301.00,813.00 301.00,813.00 301.00,813.00 216.00,813.00 216.00,813.00 216.00,813.00 201.00,814.00 201.00,814.00 201.00,814.00 117.00,814.00 117.00,814.00 117.00,814.00 106.00,814.98 106.00,814.98 106.00,814.98 89.00,814.98 89.00,814.98 59.83,814.95 33.05,792.06 33.00,762.00 33.00,762.00 33.00,308.00 33.00,308.00 33.00,308.00 33.00,102.00 33.00,102.00 33.04,76.09 53.30,57.83 77.00,51.52 77.00,51.52 109.00,51.00 109.00,51.00 Z M 78.79,81.09 C 68.29,94.62 76.44,114.56 94.00,114.99 97.56,115.07 100.69,115.10 104.00,113.59 123.99,104.45 118.25,72.83 94.00,73.33 87.78,74.28 82.83,75.88 78.79,81.09 Z M 174.00,106.45 C 188.96,103.21 186.46,80.45 169.00,82.40 151.71,87.96 160.10,109.47 174.00,106.45 Z M 137.00,101.67 C 146.42,103.66 149.47,89.33 140.00,87.47 138.19,86.95 137.65,87.31 136.02,87.47 128.10,92.07 130.85,100.37 137.00,101.67 Z", "x": 116, "y": 23, "height": 354,"texture":"assets/img/textures/white-black.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/beatles.png","editorMode": true, "draggable": true, "resizeToW": 145, "x": 124, "y": 78}},{"type": "text", "params": {"src": "THE", "editorMode": true, "draggable": true, "x": 180, "y":264,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#3C3230"], "textSize": 20,"stroke":"#D1316F"}},{"type": "text", "params": {"src": "BEATLES", "editorMode": true, "draggable": true, "x": 136, "y":287,"fontWeight":"bold", "textAlign": "center", "font": "Shadows into Light", "colors": ["#3C3230"], "textSize": 35,"stroke":"#D1316F"}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/nexus-6/front-thumbnail.png" alt="Black Color Nexus" title="Black Color Nexus">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/nexus-6/front-preview.png", "base": "assets/img/products/nexus-6/front-base.png", "paths": [{"name": "base-front", "path": "M 215.00,44.93 C 215.00,44.93 368.00,44.93 368.00,44.93 371.30,44.96 374.76,44.43 378.00,44.93 399.60,48.68 416.97,68.19 417.00,90.00 417.00,90.00 417.00,651.00 417.00,651.00 417.00,651.00 417.00,764.00 417.00,764.00 416.97,781.25 408.34,806.26 393.00,815.52 386.82,819.25 382.02,819.99 375.00,820.00 375.00,820.00 173.00,820.00 173.00,820.00 173.00,820.00 61.00,820.00 61.00,820.00 33.23,819.67 13.04,797.21 13.00,770.00 13.00,770.00 13.00,748.00 13.00,748.00 13.00,748.00 12.00,731.00 12.00,731.00 12.00,731.00 12.00,515.00 12.00,515.00 12.00,515.00 12.00,417.00 12.00,417.00 12.00,417.00 11.00,402.00 11.00,402.00 11.00,402.00 11.00,206.00 11.00,206.00 11.00,206.00 11.00,92.00 11.00,92.00 11.04,65.08 37.17,46.98 62.00,44.93 62.00,44.93 215.00,44.93 215.00,44.93 Z M 160.15,68.26 C 162.90,70.28 166.76,69.99 170.00,70.00 170.00,70.00 262.00,70.00 262.00,70.00 276.09,69.83 274.85,60.83 270.77,57.74 268.11,55.71 264.18,56.01 261.00,56.00 261.00,56.00 196.00,56.00 196.00,56.00 196.00,56.00 162.04,56.51 162.04,56.51 156.89,58.94 155.02,64.51 160.15,68.26 Z M 328.98,70.05 C 335.28,68.55 335.75,58.26 326.06,59.62 318.50,64.19 323.97,71.24 328.98,70.05 Z M 28.00,101.00 C 28.00,101.00 28.00,769.00 28.00,769.00 28.00,769.00 402.00,769.00 402.00,769.00 402.00,769.00 402.00,101.00 402.00,101.00 402.00,101.00 28.00,101.00 28.00,101.00 Z M 159.11,800.26 C 162.02,801.51 177.79,801.00 182.00,801.00 182.00,801.00 263.00,801.00 263.00,801.00 265.35,801.00 268.80,801.19 270.85,799.98 274.67,797.72 274.67,792.28 270.85,790.02 268.80,788.81 265.35,789.00 263.00,789.14 263.00,789.14 196.00,789.14 196.00,789.14 196.00,789.14 160.04,789.14 160.04,789.14 153.86,792.15 154.11,798.11 159.11,800.26 Z", "x": 106, "y": 21, "height": 358,"pattern":"assets/img/patterns/pattern-5.png"}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/nexus-6/back-preview.png", "base": "assets/img/products/nexus-6/back-base.png", "paths": [{"name": "base-front", "path": "M 100.00,48.11 C 100.00,48.11 171.00,48.11 171.00,48.11 171.00,48.11 374.00,48.11 374.00,48.11 380.92,48.09 384.88,49.70 391.00,52.76 408.35,61.43 417.97,75.43 418.00,95.00 418.00,95.00 418.00,433.00 418.00,433.00 418.00,433.00 418.00,651.00 418.00,651.00 418.00,651.00 418.00,761.00 418.00,761.00 417.80,777.95 408.19,803.83 393.00,812.64 386.84,816.21 380.95,816.99 374.00,817.00 374.00,817.00 176.00,817.00 176.00,817.00 176.00,817.00 66.00,817.00 66.00,817.00 37.94,816.67 17.04,794.79 17.00,767.00 17.00,767.00 17.00,682.00 17.00,682.00 17.00,682.00 16.00,667.00 16.00,667.00 16.00,667.00 16.00,577.00 16.00,577.00 16.00,577.00 16.00,356.00 16.00,356.00 16.00,356.00 15.00,341.00 15.00,341.00 15.00,341.00 15.00,221.00 15.00,221.00 15.00,221.00 15.00,95.00 15.00,95.00 15.04,69.07 38.65,52.81 62.00,48.11 62.00,48.11 100.00,48.11 100.00,48.11 Z M 209.00,138.99 C 241.40,139.77 249.87,98.09 223.00,84.66 217.06,81.69 211.45,81.66 205.00,82.30 171.34,88.89 175.74,138.18 209.00,138.99 Z", "x": 107, "y": 22, "height": 356,"pattern":"assets/img/patterns/pattern-5.png"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/arrow.png","editorMode": true, "draggable": true, "resizeToW": 169, "x": 115, "y": 54}},{"type": "text", "params": {"src": "Keep", "editorMode": true, "draggable": true, "x":179, "y":214,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#612B39"], "textSize": 30,"stroke":"#3C3230"}},{"type": "text", "params": {"src": "Moving", "editorMode": true, "draggable": true, "x":162, "y":249,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#612B39"], "textSize": 30,"stroke":"#3C3230"}},{"type": "text", "params": {"src": "Forward", "editorMode": true, "draggable": true, "x":153, "y":284,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#612B39"], "textSize": 30,"stroke":"#3C3230"}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->

										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/nexus-6/front-thumbnail.png" alt="Black Color Nexus" title="Black Color Nexus">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/nexus-6/front-preview.png", "base": "assets/img/products/nexus-6/front-base.png", "paths": [{"name": "base-front", "path": "M 215.00,44.93 C 215.00,44.93 368.00,44.93 368.00,44.93 371.30,44.96 374.76,44.43 378.00,44.93 399.60,48.68 416.97,68.19 417.00,90.00 417.00,90.00 417.00,651.00 417.00,651.00 417.00,651.00 417.00,764.00 417.00,764.00 416.97,781.25 408.34,806.26 393.00,815.52 386.82,819.25 382.02,819.99 375.00,820.00 375.00,820.00 173.00,820.00 173.00,820.00 173.00,820.00 61.00,820.00 61.00,820.00 33.23,819.67 13.04,797.21 13.00,770.00 13.00,770.00 13.00,748.00 13.00,748.00 13.00,748.00 12.00,731.00 12.00,731.00 12.00,731.00 12.00,515.00 12.00,515.00 12.00,515.00 12.00,417.00 12.00,417.00 12.00,417.00 11.00,402.00 11.00,402.00 11.00,402.00 11.00,206.00 11.00,206.00 11.00,206.00 11.00,92.00 11.00,92.00 11.04,65.08 37.17,46.98 62.00,44.93 62.00,44.93 215.00,44.93 215.00,44.93 Z M 160.15,68.26 C 162.90,70.28 166.76,69.99 170.00,70.00 170.00,70.00 262.00,70.00 262.00,70.00 276.09,69.83 274.85,60.83 270.77,57.74 268.11,55.71 264.18,56.01 261.00,56.00 261.00,56.00 196.00,56.00 196.00,56.00 196.00,56.00 162.04,56.51 162.04,56.51 156.89,58.94 155.02,64.51 160.15,68.26 Z M 328.98,70.05 C 335.28,68.55 335.75,58.26 326.06,59.62 318.50,64.19 323.97,71.24 328.98,70.05 Z M 28.00,101.00 C 28.00,101.00 28.00,769.00 28.00,769.00 28.00,769.00 402.00,769.00 402.00,769.00 402.00,769.00 402.00,101.00 402.00,101.00 402.00,101.00 28.00,101.00 28.00,101.00 Z M 159.11,800.26 C 162.02,801.51 177.79,801.00 182.00,801.00 182.00,801.00 263.00,801.00 263.00,801.00 265.35,801.00 268.80,801.19 270.85,799.98 274.67,797.72 274.67,792.28 270.85,790.02 268.80,788.81 265.35,789.00 263.00,789.14 263.00,789.14 196.00,789.14 196.00,789.14 196.00,789.14 160.04,789.14 160.04,789.14 153.86,792.15 154.11,798.11 159.11,800.26 Z", "x": 106, "y": 21, "height": 358,"pattern":"assets/img/patterns/stars-1.jpg"}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/nexus-6/back-preview.png", "base": "assets/img/products/nexus-6/back-base.png", "paths": [{"name": "base-front", "path": "M 100.00,48.11 C 100.00,48.11 171.00,48.11 171.00,48.11 171.00,48.11 374.00,48.11 374.00,48.11 380.92,48.09 384.88,49.70 391.00,52.76 408.35,61.43 417.97,75.43 418.00,95.00 418.00,95.00 418.00,433.00 418.00,433.00 418.00,433.00 418.00,651.00 418.00,651.00 418.00,651.00 418.00,761.00 418.00,761.00 417.80,777.95 408.19,803.83 393.00,812.64 386.84,816.21 380.95,816.99 374.00,817.00 374.00,817.00 176.00,817.00 176.00,817.00 176.00,817.00 66.00,817.00 66.00,817.00 37.94,816.67 17.04,794.79 17.00,767.00 17.00,767.00 17.00,682.00 17.00,682.00 17.00,682.00 16.00,667.00 16.00,667.00 16.00,667.00 16.00,577.00 16.00,577.00 16.00,577.00 16.00,356.00 16.00,356.00 16.00,356.00 15.00,341.00 15.00,341.00 15.00,341.00 15.00,221.00 15.00,221.00 15.00,221.00 15.00,95.00 15.00,95.00 15.04,69.07 38.65,52.81 62.00,48.11 62.00,48.11 100.00,48.11 100.00,48.11 Z M 209.00,138.99 C 241.40,139.77 249.87,98.09 223.00,84.66 217.06,81.69 211.45,81.66 205.00,82.30 171.34,88.89 175.74,138.18 209.00,138.99 Z", "x": 107, "y": 22, "height": 356,"pattern":"assets/img/patterns/stars-1.jpg"}],"template":[{"type": "text", "params": {"src": "LIVE", "editorMode": true, "draggable": true, "x":168, "y":93,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FFFFFF"], "textSize": 30}},{"type": "text", "params": {"src": "the", "editorMode": true, "draggable": true, "x":182, "y":126,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FFFFFF"], "textSize": 25}},{"type": "text", "params": {"src": "Life", "editorMode": true, "draggable": true, "x":173, "y":147,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#FFFFFF"], "textSize": 60,"degree":20}},{"type": "text", "params": {"src": "You", "editorMode": true, "draggable": true, "x":172, "y":224,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FFFFFF"], "textSize": 30}},{"type": "text", "params": {"src": "LOVE", "editorMode": true, "draggable": true, "x":166, "y":261,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FFFFFF"], "textSize": 30}}]}'></div>
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
									</div>
								</div>
								<!--/ Products -->

								<!-- Text Options Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-text-options">
									<p class="custom-pane-header text-center h3">Text Options</p><hr>
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#text" aria-controls="text" role="tab" data-toggle="tab">Text</a></li>
										<li><a role="presentation" href="#wordart" aria-controls="wordart" role="tab" data-toggle="tab">Art</a></li>
										<li><a role="presentation" href="#wordcloud" aria-controls="wordcloud" role="tab" data-toggle="tab">Cloud</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active text-center" id="text">
											<br/>
											<textarea class="form-control" id="text-content"></textarea>
											<br/>
											<select class="form-control input-lg" id="text-font-family">
												<option style="font-family:Arial" value="Arial">Arial</option>
												<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
												<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
												<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
												<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
												<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
												<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
												<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
												<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
												<option style="font-family:Chewy" value="Chewy">Chewy</option>
												<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>
											</select>
											<br/>
											<button id="btn-add-text" type="button" class="btn btn-default btn-lg">Add Text</button>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="wordart">
											<br>
											<div class="form-group">
												<textarea class="form-control" id="wordart-content"></textarea>
											</div>
											
											<select class="form-control input-lg" id="wordart-font-family">
												<option style="font-family:Arial" value="Arial">Arial</option>
												<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
												<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
												<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
												<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
												<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
												<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
												<option style="font-family:Chewy" value="Chewy">Chewy</option>
												<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>
											</select>
											<br/>
											<div class="row">
												<div class="col-xs-24">
													<!-- <div class="well"> -->
														<div class="carousel slide" id="myCarousel" data-interval="false">
															<!-- Carousel items -->
															<div class="carousel-inner wordarts">
																<div class="item active">
																	<div class="row-fluid">
																		<div class="col-xs-6 padding-sm"><img data-wordart="concave-top" class="img-thumbnail active" src="assets/img/wordarts/wordart-01.png" alt=""></div>
																		<div class="col-xs-6 padding-sm"><img data-wordart="concave-bottom" class="img-thumbnail" src="assets/img/wordarts/wordart-02.png" alt=""></div>
																		<div class="col-xs-6 padding-sm"><img data-wordart="concave-both" class="img-thumbnail" src="assets/img/wordarts/wordart-03.png" alt=""></div>
																		<div class="col-xs-6 padding-sm"><img data-wordart="triangle" class="img-thumbnail" src="assets/img/wordarts/wordart-04.png" alt=""></div>
																	</div>
																	<!--/row-fluid-->
																</div>
																<!--/item-->

																<div class="item">
																	<div class="row-fluid">
																		<div class="col-xs-6 padding-sm"><img data-wordart="curve-top" class="img-thumbnail" src="assets/img/wordarts/wordart-06.png" alt=""></div>
																	</div>
																	<!--/row-fluid-->
																</div>
																<!--/item-->
															</div>
															<!--/carousel-inner-->
															<a data-slide="prev" href="#myCarousel" class="left carousel-control">‹</a>
															<a data-slide="next" href="#myCarousel" class="right carousel-control">›</a>
														</div>
														<!--/myCarousel-->
													<!-- </div> -->
													<!--/well-->   
												</div>
											</div>											
											<div class="wordart-preview text-center hidden">
												<hr>
												<canvas id="demo" width=200 height=100></canvas>
												<br>
												<button class="btn btn-default btn-sm" id="add-wordart">Add Wordart</button>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade text-center" id="wordcloud">
											<br/>
											<textarea name="words-wordcloud" id="wordcloud-list" class="form-control"></textarea>
											<p class="help-block">Seperate words using comma <br>(eg. I,am,a,wordcloud)</p>
											<br/>
											<select class="form-control input-lg" id="wordcloud-font-family">
												<option style="font-family:Arial" value="Arial">Arial</option>
												<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
												<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
												<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
												<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
												<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
												<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
												<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
												<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
												<option style="font-family:Chewy" value="Chewy">Chewy</option>
												<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>
											</select>
											<br/>
											<select id="wordcloud-size" class="form-control input-lg">
												<option value="rectangle">Rectangle</option>
												<option value="square">Square</option>
											</select>
											<hr>
											<div class="form-group">
												<button class="btn btn-default btn-sm form-control" id="generate-wordcloud">Generate Wordcloud</button>
											</div>											
											
											<div class="wordcloud-preview text-center hidden">
												<br>
												<canvas id="wordcloud_canvas"></canvas>
												<br>
												<button class="btn btn-default btn-sm" id="add-wordcloud">Add Wordcloud</button>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<!--/ Text Options Pane -->
								
								<!-- Image Options Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-image-options">
									<p class="custom-pane-header text-center h3">Image Options</p><hr>									
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
										<li><a role="presentation" href="#uploads" aria-controls="uploads" role="tab" data-toggle="tab">Uploads</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="gallery">
											<br>
											<input type="text" id="search-gallery" placeholder="Search Gallery..." class="form-control">
											<div class="row display-gallery">
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Nike" alt="Nike" src="assets/img/sample/nike.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Adidas" alt="Adidas" src="assets/img/sample/adidas.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Being Human" alt="Being Human" src="assets/img/sample/being-human.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Puma" alt="Puma" src="assets/img/sample/puma.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Tattoo" alt="Tattoo" src="assets/img/sample/tattoos-01.png" alt=""></div>
												
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Camera" alt="Camera" src="assets/img/sample/camera-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Dragon" alt="Dragon" src="assets/img/sample/dragon-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Hearts" alt="Hearts" src="assets/img/sample/hearts-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Happy" alt="Happy" src="assets/img/sample/happy-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Sad" alt="Sad" src="assets/img/sample/sad-01.png" alt=""></div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="uploads">
											<div class="row display-gallery">
												<div class="col-sm-24 text-center">
													<br>
													<button id="btn-add-img" type="button" class="btn btn-default btn-lg"><i class="mdi mdi-upload"></i>Upload Image</button>
													<input type="file" id="add-img" style="display: none;" />
												</div>
											</div>
										</div>
									</div>
									<hr>
									<div class="form-group">
										<button class="btn btn-default btn-sm form-control" id="generate-svg">Generate SVG</button>
										<div id="dwn"></div>
									</div>
								</div>
								<!-- Image Options Pane -->

								<!-- Layering Options -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-layering-options">
									<p class="custom-pane-header text-center h3">Layers</p><hr>
									<!-- <table class="table table-hover">
										<tbody id="layers">
											<tr>
												<td><span>☰&nbsp;</span>Base Product Price</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Product - Colors</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Product - Textures</td>
											</tr>												
											<tr>
												<td><span>☰&nbsp;</span>Product - Patterns</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Texts</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Images</td>
											</tr>
										</tbody>
									</table> -->
									<ul id="layers" class="list-group">
										<!-- <li class="list-group-item"><span>☰&nbsp;</span>This is Sortable</li>
										<li class="list-group-item"><span>☰&nbsp;</span>It works with Bootstrap...</li>
										<li class="list-group-item"><span>☰&nbsp;</span>...out of the box.</li>
										<li class="list-group-item"><span>☰&nbsp;</span>It has support for touch devices.</li>
										<li class="list-group-item"><span>☰&nbsp;</span>Just drag some elements around.</li> -->
									</ul>
								</div>
								<!--/ Layering Options -->

								<!-- Cart Calculations Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-price-calculation-options">
									<p class="custom-pane-header text-center h3">Price Calculations</p><hr>
									<!-- table -->
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Size</th>
												<th class="text-center">#</th>
												<th class="text-center">Total</th>
											</tr>
										</thead>
											<tbody>
												<tr data-toggle="popover" data-popover-size="xs" title="Name & Number" data-html="true" data-content="<div data-nn-size='xs'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>XS</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="xs" class="sizes xs_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="xs_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="s" title="Name & Number" data-html="true" data-content="<div data-nn-size='s'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>S</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="s" class="sizes s_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="s_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="m" title="Name & Number" data-html="true" data-content="<div data-nn-size='m'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>M</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="m" class="sizes m_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="m_total">0</span></td>
												</tr>												
												<tr data-toggle="popover" data-popover-size="l" title="Name & Number" data-html="true" data-content="<div data-nn-size='l'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>L</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="l" class="sizes l_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="l_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="xl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>XL</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="xl" class="sizes xl_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="xl_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="xxl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xxl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>XXL</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="xxl" class="sizes xxl_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="xxl_total">0</span></td>
												</tr>
												<tr>
													<td></td>													
													<td class="text-center">
														<h4><strong>Total</strong></h4>
													</td>
													<td class="text-center text-danger">
														<h4><strong>$<span class="total_price">0.00</span></strong></h4>	
													</td>
												</tr>
											</tbody>
									</table>
									<!--/ table -->
									
									<!-- <div class="form-group">
										<button class="btn btn-default btn-sm form-control" id="generate-image">Generate Image</button>
									</div> -->
									<h4>Details: </h4>
									<em class="used_objects">None</em>
									<br><br>
									<button class="btn btn-lg btn-block btn-default">Add To Cart</button>
								</div>
								<!--/ Cart Calculations Pane -->
							</div>
							<!--/ Tab Panes -->
							<br>
						</div>
						<div class="col-sm-18 col-md-12 popup-options">
							<span class="visible-md visible-lg"><br><br><br><br></span>
							<span class="visible-sm"><hr></span>
							<div class="popup-text-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-fill">
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Stroke</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-stroke">
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Pattern</div>
									<div class="pull-right">
										<div class="pattern-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-pattern">
									<div class="col-sm-8 col-md-6"><div class="well" data-src="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/all-pink.jpg" style="background: url('assets/img/textures/all-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-blend.jpg" style="background: url('assets/img/textures/black-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton.jpg" style="background: url('assets/img/textures/black-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-span.jpg" style="background: url('assets/img/textures/black-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-splasm.jpg" style="background: url('assets/img/textures/black-cotton-splasm.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-blend.jpg" style="background: url('assets/img/textures/blue-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-cotton-blend.jpg" style="background: url('assets/img/textures/blue-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/brown-blend.jpg" style="background: url('assets/img/textures/brown-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/collon-blend.jpg" style="background: url('assets/img/textures/collon-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink.jpg" style="background: url('assets/img/textures/coral-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink-cotton.jpg" style="background: url('assets/img/textures/coral-pink-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/dark-grey.jpg" style="background: url('assets/img/textures/dark-grey.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/emerald-cotton-blend.jpg" style="background: url('assets/img/textures/emerald-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/light-brown.jpg" style="background: url('assets/img/textures/light-brown.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/red-blend.jpg" style="background: url('assets/img/textures/red-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise.jpg" style="background: url('assets/img/textures/turquoise.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise-cotton-span.jpg" style="background: url('assets/img/textures/turquoise-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-black.jpg" style="background: url('assets/img/textures/white-black.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-blue.jpg" style="background: url('assets/img/textures/white-blue.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-cotton.jpg" style="background: url('assets/img/textures/white-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>

									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/dragon2.jpg" style="background: url('assets/img/patterns/dragon2.jpg') repeat scroll 0 0; background-size: cover"></div></div>						
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-1.jpg" style="background: url('assets/img/patterns/stars-1.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-4.jpg" style="background: url('assets/img/patterns/stars-4.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/balls.jpg" style="background: url('assets/img/patterns/balls.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-1.png" style="background: url('assets/img/patterns/pattern-1.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-2.png" style="background: url('assets/img/patterns/pattern-2.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-3.png" style="background: url('assets/img/patterns/pattern-3.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-4.png" style="background: url('assets/img/patterns/pattern-4.png') repeat scroll 0 0; background-size: cover"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-group btn-group-justified" role="group" id="text-properties">
									<div class="btn-group" role="group">
										<button type="button" id="text-bold" class="btn btn-default"><i class="fa fa-bold"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-italic" class="btn btn-default"><i class="fa fa-italic"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-underline" class="btn btn-default"><i class="fa fa-underline"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-line-through" class="btn btn-default"><i class="fa fa-strikethrough"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-overline" class="btn btn-default"><img style="width:14px;height:14px" src="assets/img/control-icons/text-overline.svg" alt="O"></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-left" class="btn btn-default"><i class="fa fa-align-left"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-center" class="btn btn-default"><i class="fa fa-align-center"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-right"  class="btn btn-default"><i class="fa fa-align-right"></i></button>
									</div>
								</div>
								<br>
								<!-- <div class="row">
									<div class="col-xs-24">
										<div class="pull-left"><b>Text Curve:</b></div>
										<div class="pull-right"><input type="checkbox" id="text-curve-switch"></div>
									</div>
								</div>
								<br>
								<div class="row text-curve-prop">
									<div class="col-xs-12">
										<b>Curve Radius:</b><br><br><div id="text-curve-radius"></div>
									</div>
									<div class="col-xs-12">
										<b>Curve Space:</b><br><br><div id="text-curve-spacing"></div>
										<br>
									</div>
								</div> -->
								<div class="row">
									<div class="col-xs-12">
										<b>Height:</b><br><br><div id="text-line-height"></div>
									</div>
									<div class="col-xs-12">
										<b>Opacity:</b><br><br><div id="text-opacity"></div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-24">
										<b>Text Shadow:</b><br><br><div id="text-shadow"></div>
									</div>
								</div>
							</div>
							<div class="popup-image-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="image-fill">
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#000000" style="background-color: #000000"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="row">
									<div class="col-xs-24">
										<b>Opacity:</b><br><br><div id="image-opacity"></div>
									</div>
								</div>
							</div>
							<div class="popup-product-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="base-path-fill">
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Texture</div>
									<div class="pull-right">
										<div class="texture-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="base-path-texture">
									<div class="col-sm-8 col-md-6"><div class="well" data-src="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/all-pink.jpg" style="background: url('assets/img/textures/all-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-blend.jpg" style="background: url('assets/img/textures/black-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton.jpg" style="background: url('assets/img/textures/black-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-span.jpg" style="background: url('assets/img/textures/black-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-splasm.jpg" style="background: url('assets/img/textures/black-cotton-splasm.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-blend.jpg" style="background: url('assets/img/textures/blue-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-cotton-blend.jpg" style="background: url('assets/img/textures/blue-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/brown-blend.jpg" style="background: url('assets/img/textures/brown-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/collon-blend.jpg" style="background: url('assets/img/textures/collon-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink.jpg" style="background: url('assets/img/textures/coral-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink-cotton.jpg" style="background: url('assets/img/textures/coral-pink-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/dark-grey.jpg" style="background: url('assets/img/textures/dark-grey.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/emerald-cotton-blend.jpg" style="background: url('assets/img/textures/emerald-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/light-brown.jpg" style="background: url('assets/img/textures/light-brown.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/red-blend.jpg" style="background: url('assets/img/textures/red-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise.jpg" style="background: url('assets/img/textures/turquoise.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise-cotton-span.jpg" style="background: url('assets/img/textures/turquoise-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-black.jpg" style="background: url('assets/img/textures/white-black.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-blue.jpg" style="background: url('assets/img/textures/white-blue.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-cotton.jpg" style="background: url('assets/img/textures/white-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Pattern</div>
									<div class="pull-right">
										<div class="pattern-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="base-path-pattern">
									<div class="col-sm-8 col-md-6"><div class="well" data-src="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/dragon2.jpg" style="background: url('assets/img/patterns/dragon2.jpg') repeat scroll 0 0; background-size: cover"></div></div>										
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-1.jpg" style="background: url('assets/img/patterns/stars-1.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-4.jpg" style="background: url('assets/img/patterns/stars-4.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/balls.jpg" style="background: url('assets/img/patterns/balls.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-1.png" style="background: url('assets/img/patterns/pattern-1.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-2.png" style="background: url('assets/img/patterns/pattern-2.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-3.png" style="background: url('assets/img/patterns/pattern-3.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-4.png" style="background: url('assets/img/patterns/pattern-4.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-5.png" style="background: url('assets/img/patterns/pattern-5.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-6.png" style="background: url('assets/img/patterns/pattern-6.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-7.png" style="background: url('assets/img/patterns/pattern-7.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-8.png" style="background: url('assets/img/patterns/pattern-8.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-9.png" style="background: url('assets/img/patterns/pattern-9.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-10.png" style="background: url('assets/img/patterns/pattern-10.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-11.png" style="background: url('assets/img/patterns/pattern-11.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-12.png" style="background: url('assets/img/patterns/pattern-12.png') repeat scroll 0 0; background-size: cover"></div></div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<!--/ Left -->
					
					<div class="col-sm-14 col-md-14">
						<div class="row">
							<!-- Center -->
							<div class="col-sm-14 col-md-18">
								<!-- Design Tool Stage-->
								<div class="col-sm-24">
									<div class="text-center stage" id="design-studio">
										<div data-side="front" data-params='{"preview": "assets/img/products/galaxy-s6/front-preview.png", "base": "assets/img/products/galaxy-s6/front-base.png", "paths": [{"name": "base-front", "path": "M 101.00,50.11 C 101.00,50.11 171.00,50.11 171.00,50.11 171.00,50.11 370.00,50.11 370.00,50.11 377.09,50.09 381.65,51.73 388.00,54.78 405.48,63.18 414.97,76.52 415.00,96.00 415.00,96.00 415.00,435.00 415.00,435.00 415.00,435.00 415.00,652.00 415.00,652.00 415.00,652.00 415.00,761.00 415.00,761.00 414.80,777.78 405.24,802.29 390.00,810.65 383.76,814.08 375.04,814.99 368.00,815.00 368.00,815.00 176.00,815.00 176.00,815.00 176.00,815.00 69.00,815.00 69.00,815.00 39.14,814.65 17.05,794.30 17.00,764.00 17.00,764.00 17.00,620.00 17.00,620.00 17.00,620.00 16.00,605.00 16.00,605.00 16.00,605.00 16.00,408.00 16.00,408.00 16.00,408.00 16.00,315.00 16.00,315.00 16.00,315.00 15.06,304.00 15.06,304.00 15.06,304.00 15.06,291.00 15.06,291.00 15.06,291.00 15.06,283.00 15.06,283.00 15.06,283.00 15.06,151.00 15.06,151.00 15.06,151.00 15.06,98.00 15.06,98.00 15.03,79.77 27.12,64.91 43.00,56.97 49.62,53.66 56.76,51.78 64.00,50.11 64.00,50.11 101.00,50.11 101.00,50.11 Z M 314.00,78.61 C 322.07,76.79 325.76,67.54 319.61,61.39 316.27,58.05 312.38,57.82 308.00,58.42 292.27,64.06 301.03,81.53 314.00,78.61 Z M 161.31,73.26 C 163.77,75.29 167.98,75.00 171.00,75.00 171.00,75.00 264.00,75.00 264.00,75.00 274.39,74.80 275.58,67.38 271.57,63.51 268.66,60.70 263.75,61.01 260.00,61.11 260.00,61.11 199.00,61.11 199.00,61.11 199.00,61.11 166.00,61.11 166.00,61.11 158.56,63.54 157.17,69.84 161.31,73.26 Z M 122.95,73.67 C 127.84,72.56 129.66,65.17 121.99,62.91 112.13,64.04 115.33,75.39 122.95,73.67 Z M 139.94,73.67 C 145.01,72.51 145.94,63.95 138.00,62.77 128.61,65.53 132.89,75.28 139.94,73.67 Z M 401.00,259.00 C 401.00,259.00 401.00,141.00 401.00,141.00 401.00,141.00 401.00,114.00 401.00,114.00 401.00,112.01 401.23,108.19 399.98,106.60 398.41,104.62 394.30,105.00 392.00,105.00 392.00,105.00 115.00,105.00 115.00,105.00 115.00,105.00 51.00,105.00 51.00,105.00 47.42,105.00 34.03,104.07 32.02,106.60 30.77,108.19 31.00,112.01 31.00,114.00 31.00,114.00 31.00,764.00 31.00,764.00 31.00,764.00 401.00,764.00 401.00,764.00 401.00,764.00 401.00,259.00 401.00,259.00 Z M 178.37,792.00 C 179.82,803.06 190.64,805.99 200.00,806.04 200.00,806.04 215.00,806.04 215.00,806.04 215.00,806.04 227.00,807.00 227.00,807.00 227.00,807.00 250.00,807.00 250.00,807.00 260.44,806.98 270.99,804.57 271.90,792.00 272.89,778.51 260.57,773.06 249.00,773.16 249.00,773.16 194.00,773.16 194.00,773.16 185.63,775.34 177.12,782.51 178.37,792.00 Z", "x": 108, "y": 23, "height": 354,"pattern":"assets/img/patterns/pattern-8.png"}]}'></div>
												
										<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/galaxy-s6/back-preview.png", "base": "assets/img/products/galaxy-s6/back-base.png", "paths": [{"name": "base-front", "path": "M 90.00,50.12 C 90.00,50.12 160.00,50.12 160.00,50.12 160.00,50.12 362.00,50.12 362.00,50.12 388.92,50.32 417.96,70.29 418.00,99.00 418.00,99.00 418.00,263.00 418.00,263.00 418.00,263.00 417.00,278.00 417.00,278.00 417.00,278.00 417.00,426.00 417.00,426.00 417.00,426.00 417.00,607.00 417.00,607.00 417.00,607.00 416.00,622.00 416.00,622.00 416.00,622.00 416.00,764.00 416.00,764.00 415.67,791.96 395.38,813.96 367.00,814.00 367.00,814.00 54.00,814.00 54.00,814.00 24.91,813.95 12.04,780.97 12.00,756.00 12.00,756.00 12.00,220.00 12.00,220.00 12.00,220.00 12.00,98.00 12.00,98.00 12.04,72.47 28.77,55.84 53.00,50.12 53.00,50.12 90.00,50.12 90.00,50.12 Z M 198.00,131.00 C 198.00,131.00 198.00,167.00 198.00,167.00 198.01,170.51 197.74,174.67 200.01,177.61 202.88,181.33 207.80,180.99 212.00,181.00 212.00,181.00 250.00,181.00 250.00,181.00 254.03,181.00 263.73,181.58 266.94,179.83 271.39,177.39 270.99,173.37 271.00,169.00 271.00,169.00 271.00,135.00 271.00,135.00 271.00,131.47 271.53,123.01 269.83,120.23 267.70,116.74 263.56,117.02 260.00,117.22 260.00,117.22 204.02,117.22 204.02,117.22 197.43,120.21 198.01,124.73 198.00,131.00 Z M 281.00,128.00 C 281.00,128.00 281.00,170.00 281.00,170.00 281.12,178.65 283.45,180.94 292.00,181.00 296.95,181.03 309.24,182.48 311.98,177.85 313.19,175.80 313.00,172.35 313.00,170.00 313.00,170.00 313.00,134.00 313.00,134.00 313.00,130.31 313.74,122.40 311.26,119.65 307.85,115.86 292.98,116.71 288.00,117.39 281.74,119.46 281.09,121.72 281.00,128.00 Z", "x": 106, "y": 23, "height": 354,"pattern":"assets/img/patterns/pattern-8.png"}],"template":[{"type": "text", "params": {"src": "XOXO", "editorMode": true, "draggable": true, "x": 113, "y":128,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#304972"], "textSize": 30,"degree":270,"fontPattern":"assets/img/patterns/pattern-2.png"}},{"type": "text", "params": {"src": "LOL", "editorMode": true, "draggable": true, "x": 206, "y":86,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#FEAC1C"], "textSize": 35,"degree":32,"stroke":"#008767"}},{"type": "text", "params": {"src": "ROFL", "editorMode": true, "draggable": true, "x": 213, "y":148,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#C2BEBF"], "textSize": 25,"degree":19,"stroke":"#FEAC1C"}},{"type": "text", "params": {"src": "BRB", "editorMode": true, "draggable": true, "x": 138, "y":171,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#E975A4"], "textSize": 35,"degree":16,"stroke":"#28272C"}},{"type": "text", "params": {"src": "IRL", "editorMode": true, "draggable": true, "x": 110, "y":237,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#85A35D"], "textSize": 35,"degree":330,"stroke":"#B3042B"}},{"type": "text", "params": {"src": "BFF", "editorMode": true, "draggable": true, "x": 191, "y":282,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#85A35D"], "textSize": 35,"degree":332,"stroke":"#B3042B"}},{"type": "text", "params": {"src": "asap", "editorMode": true, "draggable": true, "x": 132, "y":295,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#008767"], "textSize": 35,"degree":12,"stroke":"#C2BEBF"}},{"type": "text", "params": {"src": "gr8", "editorMode": true, "draggable": true, "x": 213, "y":311,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#C2BEBF"], "textSize": 35,"stroke":"#4A5D7B"}}]}'></div>
									</div>								
								</div>
								<!--/ Design Tool Stage-->
							</div>
							<!--/ Center -->

							<!-- Right -->
							<div class="col-sm-10 col-md-6">
								<div class="row">
									<span class="visible-xs"><br></span>
									<div class="col-sm-12 text-center preview-images">
										<div class="row">
											<!-- Preview Images -->
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="front-preview" alt="Front">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="left-preview" alt="Left">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="back-preview" alt="Back">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="right-preview" alt="Right">
											</div>
											<!--/ Preview Images -->
											
											<div class="col-xs-24 visible-lg text-center operation-icons">
												<br><br><br>
												<i class="ion-chevron-left ion-2x previous-operation" title="Pervious Operation" data-toggle="tooltip"></i>
												&nbsp;&nbsp;&nbsp;&nbsp;
												<i class="ion-chevron-right ion-2x next-operation" title="Next Operation" data-toggle="tooltip"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--/ Right -->
						</div>

						<div class="row">
							<div class="col-sm-14 col-md-18">
								<div class="col-sm-20 col-sm-offset-2">
									<br>
									<div class="col-xs-18 col-md-21">
										<br>
										<div id="zoom-stage"></div>
									</div>
									<div class="col-xs-3">
										<div class="circle">
											<div class="circle_outer">
												<div class="left_arrow"><a href="javascript:void(0)" class="move" data-side="left"><i class="ion-1-half ion-ios-arrow-left"></i></a></div>
												<div class="top_arrow"><a href="javascript:void(0)" class="move" data-side="up"><i class="ion-1-half ion-ios-arrow-up"></i></a></div>
												<div class="right_arrow"><a href="javascript:void(0)" class="move" data-side="right"><i class="ion-1-half ion-ios-arrow-right"></i></a></div>
												<div class="down_arrow"><a href="javascript:void(0)" class="move" data-side="down"><i class="ion-1-half ion-ios-arrow-down"></i></a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Price -->
							<div class="col-sm-10 col-md-6">
								<span class="visible-xs"><br><br><br><br><br><br></span>
								<div class="col-sm-12 text-center total_price_col">
									<br>
									<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
									<i class="face-share ion-2x ion-social-facebook-outline"></i>
									&nbsp;&nbsp;
									<i class="tweet-share ion-2x ion-social-twitter-outline"></i>
								</div>
							</div>
							<!--/ Price -->
						</div>
					</div>
				</div>
				<!--/ Design Tool -->
			</div>
		</div>
		<!--/  Main Container -->
		
		<!-- Pre-load Fonts -->
		<section class="preload-fonts"></section>
		<!--/ Pre-load Fonts -->
		<!-- JS -->
		<!-- plugins -->
		<script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/lib/confirmation/bootstrap-confirmation.min.js"></script>
		<script type="text/javascript" src="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/lib/wordcloud2/wordcloud2.js"></script>
		<script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>
		<script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>
		<script type="text/javascript" src="assets/lib/fabric-curved-text/fabric.curvedText.js"></script>
		<script type="text/javascript" src="assets/lib/nouislider/nouislider.min.js"></script>

		<script src="assets/lib/bootstrap-switch/bootstrap-switch.min.js"></script>
		<!--/ plugins -->
		<script type="text/javascript" src="assets/js/index.js"></script>
		<!--script type="text/javascript" src="assets/js/wordart.js"></script-->
		<!--/ JS -->
	</body>
</html>