<?php
	$design = [];
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$dir = __DIR__.'/';
		$products = json_decode(file_get_contents($dir.'products.json'), true);

		foreach($products as $ip => $subProducts){
			if(array_key_exists($id, $subProducts)){
				$product = $subProducts[$id];
				break;
			}
		}

		// If product exists
		if(isset($product)){
			foreach($product as $side => $name){
				$jsonFileName = str_replace('.png', '.json', $name);
				$design[$side] = file_get_contents($dir.'/products/'.$jsonFileName);
			}
		}else {
			// product id doesnot exists
		}
	}
?>