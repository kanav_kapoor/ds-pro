(function($){
	$.noConflict();
	$(function($) {
		// Plugins
		$('[data-toggle="tooltip"]').tooltip();
		$('[data-toggle=confirmation]').confirmation({placement: "bottom", popout: true});
		$('#myCarousel').carousel();

		$("#search-products").on('keyup change paste', showMatchedProducts);
		$("#search-gallery").on('keyup change paste', showMatchedGallery);

		_preloadFonts();

		function showMatchedProducts(e){
			var $target     = $(e.target),
			searchedKeyword = $.trim($target.val().toLowerCase());
			$products       = $(".product");

			if(searchedKeyword.length > 0){
				$products.each(function(i, products){
					$this = $(this);
					if($this.children('img').attr("title").toLowerCase().indexOf(searchedKeyword) > -1) $this.show();
					else $this.hide();
				});
			}else $products.show();
		}

		function showMatchedGallery(e){
			var $target     = $(e.target),
			searchedKeyword = $.trim($target.val().toLowerCase());
			$galleryImgs       = $("#gallery img");

			if(searchedKeyword.length > 0){
				$galleryImgs.each(function(i, block){
					$this = $(this);
					if($this.attr("title").toLowerCase().indexOf(searchedKeyword) > -1) $this.parent().show();
					else $this.parent().hide();
				});
			}else $galleryImgs.parent().show();
		}

		/*var sortable = new Sortable($("#layers").get(0), {
			onEnd: function (evt) {
				var nextIndex = $(evt.item).next().data('z');
				console.log(nextIndex-1);
				designStudio.setNewZIndex({old: evt.oldIndex, new: nextIndex-1});
       }
		});*/

		$("body").mCustomScrollbar({
				theme:"minimal-dark",
				autoHideScrollbar: true,
		});
		
		$(".display-gallery").mCustomScrollbar({
				theme:"minimal-dark",
				autoHideScrollbar: true
		});
		

		$('html').on("mouseup", function(event) {
			// Hide Display Gallery
			if(!$(event.target).closest('.color-selector + .display-gallery').length && !$(event.target).closest('.color-selector').length)
					$('.color-selector + .display-gallery').addClass('hidden');
			hideAllPopovers(event);
		});

		// Color/Pattern Selector
		$(".color-selector").on("click", function(){
			$(this).next().toggleClass("hidden");
			$('.color-selector + .display-gallery').not($(this).next()).addClass('hidden');
		});


		$("#close").on("click", function(){
			$("a[data-toggle='tab']:first").tab("show");
		});

		// Tab Sliding Effect
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			if(!$(this).closest('ul').hasClass('tool-options')) return;

			// Hide Popups
			$(".popup-options > div").addClass("hide");
			var target = $(this).attr('href');

			$(target).css('top','-'+$(window).width()+'px');   
			var top = $(target).offset().top;
			$(target).css({top:top}).animate({"top":"0px"}, 800);
		});

		// Toggle nav
		function toggleNav(){
			// Large Screen
			if($(window).width() >= 768) {
				$(".nav-controller").addClass('hidden');
				$("nav").addClass('focus');
			}
			// Small Screen
			else {
				$(".nav-controller").removeClass('hidden');
				$("nav").removeClass('focus');
			}
		}

		// Hide Navbar on click -  small screens
		$('nav, .nav-controller').on('click', function(event) {
			if($(window).width() < 768) $('nav').toggleClass('focus');
		});

		$(window).on("resize load", toggleNav);

		// Sliders
		var textLineHeight = $("#text-line-height").get(0);
		noUiSlider.create(textLineHeight,  {start: 0, range:{'min': 1, 'max': 5}, connect: 'lower'});

		var textOpacity = $("#text-opacity").get(0);
		noUiSlider.create(textOpacity,  {start: 0, range:{'min': 1, 'max': 9}, connect: 'lower'});

		var textShadow = $("#text-shadow").get(0);
		noUiSlider.create(textShadow,  {start: 0, range:{'min': 0, 'max': 10}, connect: 'lower', step: 1});

		/*var textCurveRadius = $("#text-curve-radius").get(0);
		noUiSlider.create(textCurveRadius,  {start: 0, range:{'min': 50, 'max': 100}, connect: 'lower', step: 1});*/

		/*var textCurveSpacing = $("#text-curve-spacing").get(0);
		noUiSlider.create(textCurveSpacing,  {start: 0, range:{'min': 10, 'max': 30}, connect: 'lower', step: 1});*/

		var imageOpacity = $("#image-opacity").get(0);
		noUiSlider.create(imageOpacity,  {start: 0, range:{'min': 1, 'max': 9}, connect: 'lower'});

		var zoomStage = $("#zoom-stage").get(0);
		noUiSlider.create(zoomStage,  {start: 0, range:{'min': 1, 'max': 100, '20%': 20, '40%': 40, '60%': 60, '80%': 80}, connect: 'lower', pips: {mode: 'range', density: 4}});

		// Bootstrap Switch
		//$("#text-curve-switch").bootstrapSwitch({state: false, size: "mini"});
		

		var $stageContainer = $("#design-studio");
		var designStudio = $stageContainer.designStudio({dimensions: {productDisplayWidth: $stageContainer.width(), productStageWidth: 400, productStageHeight: 400}}).data('design-studio');

		$("#wordcloud-font-family").on("change", generateWordCloud)
		$("#generate-wordcloud").on("click", generateWordCloud);
		$("#wordcloud-size").on("change", generateWordCloud);
		$("#add-wordcloud").on("click", addWordCloud);

		function generateWordCloud(){
			if(designStudio.generateWordCloud({list: $("#wordcloud-list").val(), fontFamily: $("#wordcloud-font-family").val(), shape: $("#wordcloud-size").val()})){ 
				$('.wordcloud-preview').removeClass('hidden');
				$("body").mCustomScrollbar("scrollTo", "bottom");
			}
			else $('.wordcloud-preview').addClass('hidden');
		}

		function addWordCloud(){
			designStudio.addWordCloud();
			$("body").mCustomScrollbar("scrollTo", "top");
			//$('.wordcloud-preview').addClass('hidden');
		}

		$("#flipX").on("click", function(){
			designStudio.changeObjectProperty("flipX");
		});

		$("#flipY").on("click", function(){
			designStudio.changeObjectProperty("flipY");
		});

		$("#reset").on("click", function(){
			//$("#loader").removeClass("hidden");
			//$(".canvas-container").remove();
			designStudio.reset();
		});

		$("#undo").on("click", function(){
			designStudio.undo();
		});

		$("#redo").on("click", function(){
			designStudio.redo();
		});

		$(".navigate-left").on("click", function(){
			var $prevSide = $("#design-studio > div:visible").prev();
			var sideName = $prevSide.length ? $prevSide.data('side') : $("#design-studio > div:last").data('side');
			designStudio.changeSide(sideName);
		});

		$(".navigate-right").on("click", function(){
			var $prevSide = $("#design-studio > div:visible").next();
			var sideName = $prevSide.length ? $prevSide.data('side') : $("#design-studio > div:first").data('side');
			designStudio.changeSide(sideName);
		});

		// Change Sides
		$(".preview-images img").on("click", function(){
			designStudio.changeSide($(this).attr('id').replace("-preview", ""));
		});

		// Operation Icons		
		$(".previous-operation").on("click", function(){
			$(".tool-options li[class='active']").prev().find("a").tab('show');
			/*var $prevNav = $(".tool-options li[class='active']").prev();
			$prevNav = $prevNav.length ? $prevNav : $(".tool-options li:last");
			$prevNav.find("a").tab('show');*/
		});

		$(".next-operation").on("click", function(){
			$(".tool-options li[class='active']").next().find("a").tab('show');
			/*var $nextNav = $(".tool-options li[class='active']").next();		
			$nextNav = $nextNav.length ? $nextNav : $(".tool-options li:first");
			$nextNav.find("a").tab('show');*/
		});

		// zoom stage
		zoomStage.noUiSlider.on("slide", function(value, handle, unencoded){
			designStudio.zoom(unencoded);
		});


		// move stage
		var timeoutId = 0;
		$('.move').click(function() {
			designStudio.move($(this).data('side'));
		}).mousedown(function() {
			var side = $(this).data('side');
			timeoutId = setInterval(function(){designStudio.move(side)}, 200);
		}).bind('mouseup mouseleave', function() {
				clearTimeout(timeoutId);
		});

		var _preloadImages = function(images){
			$.each(images, function(i, img){
				(new Image()).src = img;
			});
		}

		function _preloadFonts(){
			$("#text-font-family").children().each(function(){
				$('.preload-fonts').append("<p style='"+$(this).attr('style')+"'>Loading...</p>");
			});
			$('.preload-fonts').hide();
		}

		function onlyUnique(value, index, self){
			return self.indexOf(value) === index;
		}
		
		var productImages = [];
		$('.product-specs [data-params]').each(function(){
			var dataParams = JSON.parse($(this).attr('data-params'));

			productImages.push(dataParams.preview);
			productImages.push(dataParams.base);
		});

		$("[data-src]").each(function(){
			if($(this).data('src')) productImages.push($(this).data('src'));
		});

		productImages = productImages.filter(onlyUnique);
		_preloadImages(productImages);

		// Add Text
		$("#btn-add-text").on("click", function(){
			var defaultText = "New Text";
			designStudio.addText('New Text', {autoCenter: true, editorMode: true, draggable: true, autoSelect: true})
			$("#text-content").val(defaultText);
		});

		// Modify Text
		$("#text-content").on("keyup", function(){
			var defaultText = $.trim($("#text-content").val());
			designStudio.changeTextProperty('font-text', defaultText);
		});
		$("#text-font-family").on("change",  function(){
			var fontFamily = $(this).val();
			designStudio.changeTextProperty('font-family', fontFamily);
			$(this).css("font-family", fontFamily);
		});

		$("#text-font-fill div[data-fill]").on("click",  function(){
				var fontFill = $(this).data("fill");
				designStudio.changeTextProperty('font-fill', fontFill);
		});

		$("#text-font-stroke div[data-stroke]").on("click",  function(){
				var fontStroke = $(this).data("stroke");
				designStudio.changeTextProperty('font-stroke', fontStroke);
		});

		$("#text-bold").on("click", function(){
			designStudio.changeTextProperty('bold');
		});

		$("#text-italic").on("click", function(){
			designStudio.changeTextProperty('italic');
		});

		$("#text-underline").on("click", function(){
			designStudio.changeTextProperty('underline');
		});

		$("#text-line-through").on("click", function(){
			designStudio.changeTextProperty('line-through');
		});

		$("#text-overline").on("click", function(){
			designStudio.changeTextProperty('overline');
		});

		$("#text-align-left").on("click", function(){
			designStudio.changeTextProperty('align-left');
		});

		$("#text-align-center").on("click", function(){
			designStudio.changeTextProperty('align-center');
		});

		$("#text-align-right").on("click", function(){
			designStudio.changeTextProperty('align-right');
		});

		$(".wordarts [data-wordart]").on("click", function(){
			$(".wordarts [data-wordart]").removeClass('active');
			$(this).addClass('active');
			previewWordArtHelper();
		});

		$("#wordart-font-family").on("change", previewWordArtHelper);
		function previewWordArtHelper(){
			var wordartType = $(".wordarts [data-wordart].active").data('wordart');
			var params = {type: wordartType, text: $.trim($("#wordart-content").val()), font: $("#wordart-font-family").val()};
			if(designStudio.previewTextWordArt(params)) $(".wordart-preview").removeClass('hidden');
		}

		$("#add-wordart").on("click", function(){
			designStudio.addWordArt();
		});

		/*$("#text-curve-switch").on("switchChange.bootstrapSwitch", function(event, state){			
			designStudio.previewTextWordArt();
			
		});*/


		/*textCurveRadius.noUiSlider.on("slide", function(value, handle, unencoded){
			designStudio.changeTextProperty('radius', unencoded);
		});*/

		/*textCurveSpacing.noUiSlider.on("slide", function(value, handle, unencoded){
			designStudio.changeTextProperty('spacing', unencoded);
		});*/

		textLineHeight.noUiSlider.on("slide", function(value, handle, unencoded){
			designStudio.changeTextProperty('line-height', unencoded);
		});

		textOpacity.noUiSlider.on("slide", function(value, handle, unencoded){
			var sent = 1-(unencoded/10);
			designStudio.changeTextProperty('opacity', 1-(unencoded/10));
		});

		textShadow.noUiSlider.on("slide", function(value, handle, unencoded){
			designStudio.changeTextProperty('shadow', 10-unencoded);
		});


		// Register Add Image Handler
			$("#btn-add-img").on("click", function(e){
				$('#add-img').trigger('click');
				e.preventDefault();
			});

			// Add Image
			$("#add-img").on("change", function(e){
				var reader = new FileReader();
				reader.readAsDataURL(e.target.files[0]);
				reader.onload = function(e){
					designStudio.addImage(e.target.result, {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
					
					// Add image to gallery
					if($("img[src='"+e.target.result+"']").length) return;
					$("#uploads > .row").append("<div class='col-xs-6 top5'><img height=120 width=120 class='img-thumbnail' src='"+e.target.result+"' alt=''></div>");
				}
			});

			// Load from gallery
			$("div").on("click", "#gallery img, #uploads img", function(e){
					designStudio.addImage($(this).attr("src"), {autoCenter: true, editorMode: true, draggable: true, autoSelect: true});
					e.stopPropagation();
			});

			// Image Opacity
			imageOpacity.noUiSlider.on("slide", function(value, handle, unencoded){
				designStudio.changeImageProperty('opacity', 1-(unencoded/10));
			});

			// Patching
			$("#image-patching").on("switchChange.bootstrapSwitch", function(event, state){			
				//designStudio.patching(state);
			});


			// Chnage Image Color
			$("#image-fill div[data-fill]").on("click",  function(){
				var imageFill = $(this).data("fill");
				designStudio.changeImageProperty('color', imageFill);
			});

			// Load from gallery
			$("#text-font-pattern div[data-src]").on("click", function(e){
					designStudio.changeTextProperty('font-pattern', $(this).data("src"));
					e.stopPropagation();
			});


			// Change Base Color
			$("#base-path-fill div[data-fill]").on("click", function(){
				designStudio.changePathProperty('fill', $(this).data('fill'));
				//designStudio.changeAllSidesColor($(this).data('color'));
			});

			// Change Texture
			$("#base-path-texture div[data-src]").on("click", function(){
				designStudio.changePathProperty('texture', $(this).data('src'));
				//designStudio.changeAllSidesColor($(this).data('color'));
			});

			// Change Pattern
			$("#base-path-pattern div[data-src]").on("click", function(){
				designStudio.changePathProperty('pattern', $(this).data('src'));
				//designStudio.changeAllSidesColor($(this).data('color'));
			});

			// Change Path Left
			/*$("#path-left").on("change keyup", function(){
				designStudio.changePathProperty('left', parseFloat($(this).val()));
			});

			// Change Path Top
			$("#path-top").on("change keyup", function(){
				designStudio.changePathProperty('top', parseFloat($(this).val()));
			});

			// Change Path Height
			$("#path-height").on("change keyup", function(){
				designStudio.changePathProperty('height', parseFloat($(this).val()));
			});*/

			// Change Product
			$(".products-list .product").on("click", function(){
				design = null;
				$("#design-studio").html($(this).children(".product-specs").html());
				designStudio.changeProduct();
			});

			var $saveToJson = function(){
				return $.post('db/save-to-json.php', {newProductDesigns: designStudio.getJSONDesign(), newProductImages: designStudio.getPreviewImages()});	
			}

			/* Calculations  - Start */
			$(".sizes").on({"change keyup paste": sizeChanged, "click focus": showPopover});

			function sizeChanged(e){
				designStudio.updatePrice();
				changeNameNumberInputs(e);
				showPopover(e);
			}

			function showPopover(e){
				var $this = $(e.target).closest("tr");
				$('.sizes').closest("tr").not($this).popover('hide');
				$this.popover({"trigger": "manual", container: "body"});
				$this.popover('show');
			}


			function changeNameNumberInputs(e){
				var $elem = $(e.target);
				var value = $.trim($elem.val());
				// If value is numeric or null
				if(!isNaN(value) && value !=""){
					
					var size = $elem.data('input-size');
					// popover containing settings
					var $popover = $elem.closest("tr");
					// popover that is displayed dynamically
					var $popoverDom = $("[data-nn-size='"+size+"']");

					
					var $childrenDiv = $popoverDom.children('div');
					var existingValue = $childrenDiv.length;
					var rowHtml = "<div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div>";
					
					//console.log(existingValue+" - "+value);
					if(existingValue < value) {
						for(var i=existingValue; i<value;i++){
							$popoverDom.append(rowHtml);
						}
					}else if(existingValue > value){
						 $popoverDom.children('div:gt('+(value-1)+')').remove();
					}else {

					}
					
					var newHtml = $popoverDom.get(0).outerHTML;
					$popover.attr('data-content', newHtml);
				}
				e.stopPropagation();
			}

	

			$("[data-toggle='popover']").on('click keyup', function(){
				$("[data-toggle='popover']").not(this).popover('hide');
			});

			function hideAllPopovers(e){
				$('[data-toggle="popover"]').each(function () {
				//the 'is' for buttons that trigger popups
				//the 'has' for icons within a button that triggers a popup
					if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
						$(this).popover('hide');
					}
				});

			}


			$("[data-toggle='popover']").on('hide.bs.popover', function(){
				var $this = $(this);
				var size = $this.data('popover-size');
				var $popoverDom = $("[data-nn-size='"+size+"']");

				if(!$popoverDom.length) return;

				$popoverDom.find('input').each(function(){
					$(this).attr('value', $(this).val())
				});

				var newHtml = $popoverDom.get(0).outerHTML;
				$this.attr('data-content', newHtml);
			});
			/* Calculations - Ends */

			// Save/Load
			
			$("#saveProduct").on("click", function(){
				$("#loader").removeClass('hidden');
				$saveToJson().then(function (response) {
					loadSavedProducts();
					$("#loader").addClass('hidden');
				});
			});

			
			/* Facebook Sharing Start */
			$(".face-share").on("click", publishOnFacebook);

			/*window.fbAsyncInit = function() {
				FB.init({
					appId      : '1623171791255228',
					xfbml      : true,
					version    : 'v2.4'
				});
			};

			(function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/en_US/sdk.js";
				 fjs.parentNode.insertBefore(js, fjs);
			 }(document, 'script', 'facebook-jssdk'));*/

			function publishOnFacebook() {
				$("#loader").removeClass("hidden");

				var frontImage =  designStudio.getPreviewImages().front;
				$.post("db/save-front-image.php", {frontImage: frontImage}, function(savedImage){
						$("#loader").addClass("hidden");
						FB.ui(
						{
							method: 'feed',
							name: 'Custom Designed Product',
							link: window.location.href,
							picture: savedImage,
							caption: 'Created Using Design Studio',
							description: 'I designed an awesome product using design tool.'
						},
						function(response) {
						 /* if (response && response.post_id) {
								alert('Post was published.');
							} else {
								alert('Post was not published.');
							}*/
						}
					);
				});
			}
			/* Facebook Sharing Ends */

		
			$(".tweet-share").on("click", publishOnTwitter);

			function publishOnTwitter(){
				$("#loader").removeClass("hidden");
				$saveToJson().then(function (urlToEdit) {
					var tweetText = "https://twitter.com/intent/tweet?text=I%20designed%20an%20awesome%20product%20using%20design%20tool&url="+urlToEdit;
					window.open(tweetText);
					$("#loader").addClass('hidden');
				});
			}	
	});
})(jQuery)