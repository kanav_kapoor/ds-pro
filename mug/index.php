<?php require_once('db/load-product.php'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>DS Pro - Mug</title>

		<script type="text/javascript">
			var design = <?php echo json_encode($design); ?>;
		</script>
		
		<!-- CSS -->
		<!-- plugins -->
		<link rel="stylesheet" href="assets/lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<link rel="stylesheet" type="text/css" href="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
		<link href='http://fonts.googleapis.com/css?family=Josefin+Sans|Shadows+Into+Light|Pacifico|Quicksand|Architects+Daughter|Dancing+Script|Chewy|Gloria+Hallelujah|Shadows+Into+Light|Poiret+One|Indie+Flower|Oswald|' rel='stylesheet' type='text/css'>
		<!-- <link rel='stylesheet' href='assets/css/google-fonts.css'> -->
		<link href="assets/lib/nouislider/nouislider.min.css" media="all" rel="stylesheet" type="text/css" />

		<link rel="stylesheet" href="assets/lib/bootstrap-switch/bootstrap-switch.min.css">
		<!--/ plugins -->

		<link rel="stylesheet" href="assets/css/menu.css">
		<link rel="stylesheet" href="assets/css/media.css">
		<link rel="stylesheet" href="assets/css/index.css">
		<!--/ CSS -->
	</head>
	
	<body>
		<div id="loader">
			<p class="text-center"><br>Loading.... Please wait...</p>
		</div>
		<!-- header -->
		<header>
			<!-- Nav -->
			<nav class="animate focus">
				<ul class="nav nav-pills nav-stacked tool-options" role="tablist">
					<li role="presentation" class="active"><a class="text-center" title="Products" data-toggle="tab" aria-controls="tabpane-products" href="#tabpane-products"><i class="fa fa-th"></i></a></li>
					<li role="presentation"><a class="text-center" title="Text Options" data-toggle="tab" aria-controls="tabpane-text-options" href="#tabpane-text-options"><i class="fa fa-pencil"></i></a></li>
					<li role="presentation"><a class="text-center" title="Image Options" data-toggle="tab" aria-controls="tabpane-image-options" href="#tabpane-image-options"><i class="fa fa-picture-o"></i></a></li>
					<li role="presentation"><a class="text-center" title="Price Calculations" data-toggle="tab" aria-controls="tabpane-price-calculation-options" href="#tabpane-price-calculation-options"><i class="fa fa-cart-plus"></i></a></li>
				</ul>
			</nav>
			<!--/ Nav -->
			<div class="nav-controller hidden">
				<span class="[ ion-2x ion-navicon-round ] controller-open"></span>
				<span class="[ ion-2x ion-ios-close-outline ] controller-close"></span>
			</div>
		</header>
		<!--/ header -->		

		<!--  Main Container -->
		<div class="content-wrapper container-fluid">
			<div class="row">	

				<!-- Tools -->
				<div class="row tools">
					<div class="col-sm-14 col-md-14 col-sm-offset-10 col-md-offset-10">
						<div class="row">
							<div class="col-sm-14 col-md-18">
								<div class="col-sm-24">
									<div class="btn-group btn-group-justified" role="group" aria-label="">
										<div class="btn-group" role="group">
											<button title="Save" type="button" data-toggle="tooltip" data-placement="bottom" title="Save" class="btn btn-default" id="saveProduct"><i class="ion-2x ion-ios-heart-outline"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Flip Horizontally" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Horizontally" class="btn btn-default" id="flipX"><i class="ion-2x ion-ios-arrow-thin-right"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Flip Vertically" type="button" data-toggle="tooltip" data-placement="bottom" title="Flip Vertically" class="btn btn-default" id="flipY"><i class="ion-2x ion-ios-arrow-thin-up"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Reset" type="button" data-toggle="confirmation" class="btn btn-default" id="reset"><i class="ion-2x ion-ios-loop"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Undo" type="button" data-toggle="tooltip" data-placement="bottom" title="Undo" class="btn btn-default" id="undo"><i class="ion-2x ion-ios-undo-outline"></i></button>
										</div>
										<div class="btn-group" role="group">
											<button title="Redo" type="button" data-toggle="tooltip" data-placement="bottom" title="Redo" class="btn btn-default" id="redo"><i class="ion-2x ion-ios-redo-outline"></i></button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--/ Tools -->
				
				<!-- Design Tool -->
				<div class="row">

					<!-- Left -->
					<div class="col-sm-10 col-md-10">
						<div class="col-sm-18 col-md-12">
							<!-- Tab Panes -->
							<div class="tab-content tool-panes">
								<!-- Products -->
								<div role="tabpanel" class="products tab-pane fade in active" id="tabpane-products">
									<p class="custom-pane-header text-center h3">Products</p><hr>
									<input type="search" placeholder="Search Products" class="form-control" id="search-products">
									<div class="row display-gallery products-list">
										<!-- product -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-01/mug-thumbnail.png" alt="White Cotton Rounded Mugs" title="White Cotton Rounded Mugs">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-01/left-preview.png", "base": "assets/img/products/mug-01/left-base.png", "paths": [{"name": "base-front", "path": "M 227.37,22.00 C 231.09,36.83 234.98,60.87 235.00,76.00 235.00,76.00 235.00,105.00 235.00,105.00 235.00,105.00 233.17,127.00 233.17,127.00 229.87,159.99 225.51,186.55 211.29,217.00 204.61,231.32 192.02,253.25 179.83,263.07 172.12,269.30 159.66,272.62 150.00,274.25 138.01,276.26 126.16,277.02 114.00,277.00 114.00,277.00 93.00,276.00 93.00,276.00 80.32,275.94 62.84,271.67 53.09,263.33 46.97,258.10 37.13,242.46 33.22,235.00 23.03,215.55 18.47,206.38 12.73,185.00 3.51,150.60 -0.42,136.53 0.00,100.00 0.00,100.00 0.96,89.00 0.96,89.00 1.82,69.25 3.11,55.39 7.50,36.00 10.55,22.52 11.33,15.87 18.00,3.00 37.56,14.93 59.68,16.56 82.00,18.09 82.00,18.09 94.00,19.04 94.00,19.04 94.00,19.04 110.00,19.04 110.00,19.04 110.00,19.04 119.00,19.97 119.00,19.97 119.00,19.97 133.00,19.00 133.00,19.00 145.27,18.98 157.84,18.00 170.00,16.27 186.53,13.91 206.19,11.62 219.00,0.00 223.20,5.43 225.69,15.28 227.37,22.00 Z", "x": 127, "y": 84, "height": 260,"texture":"assets/img/textures/white-cotton.jpg"},{"name": "handle", "path": "M 13.44,124.00 C -5.00,95.04 -5.54,57.51 16.01,30.00 20.08,24.81 24.59,19.86 30.00,16.04 49.62,2.18 72.76,0.00 96.00,0.00 96.00,0.00 90.00,28.00 90.00,28.00 90.00,28.00 73.00,26.00 73.00,26.00 65.37,25.91 58.31,26.46 51.00,28.89 46.35,30.43 41.91,32.79 38.01,35.76 31.40,40.80 26.79,49.05 24.64,57.00 17.72,82.63 29.02,104.95 47.04,122.96 55.00,130.92 65.72,138.28 76.00,142.91 76.00,142.91 92.10,148.74 92.10,148.74 93.74,150.51 94.84,156.51 95.47,159.00 95.47,159.00 101.00,180.00 101.00,180.00 66.90,166.36 34.52,157.12 13.44,124.00 Z", "x": 46, "y": 102, "height": 169,"pattern":"assets/img/patterns/stars-1.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/browsewire.png","editorMode": true, "draggable": true, "resizeToW": 92, "x": 196, "y": 121}},{"type": "text", "params": {"src": "BROWSEWIRE", "editorMode": true, "draggable": true, "x": 167, "y":186,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#6E95CE"], "textSize": 20,"stroke":"#28272C"}},{"type": "text", "params": {"src": "Consulting Pvt. Ltd", "editorMode": true, "draggable": true, "x": 167, "y":209,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#B3042B"], "textSize": 15}},{"type": "text", "params": {"src": "Lets Grow Together", "editorMode": true, "draggable": true, "x": 184, "y":227,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#663D50"], "textSize": 12}}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-01/right-preview.png", "base": "assets/img/products/mug-01/right-base.png", "paths": [{"name": "base-front", "path": "M 28.00,7.38 C 47.08,15.42 69.61,16.69 90.00,18.09 90.00,18.09 102.00,19.00 102.00,19.00 132.49,19.05 169.67,19.49 199.00,11.09 210.03,7.93 210.96,5.69 218.00,3.00 218.00,3.00 224.09,19.00 224.09,19.00 228.07,32.90 233.93,64.86 234.00,79.00 234.00,79.00 235.00,97.00 235.00,97.00 235.00,97.00 233.90,125.00 233.90,125.00 233.90,125.00 233.90,136.00 233.90,136.00 231.30,155.61 225.78,172.16 220.73,191.00 215.87,209.13 207.30,225.85 197.86,242.00 192.58,251.03 186.09,260.98 177.00,266.54 162.42,275.45 145.52,275.81 129.00,276.00 129.00,276.00 117.00,276.97 117.00,276.97 117.00,276.97 108.00,276.09 108.00,276.09 90.51,274.89 72.14,274.02 57.00,263.90 51.04,259.91 45.04,251.86 40.86,246.00 31.08,232.30 22.21,214.91 16.66,199.00 7.76,173.51 4.38,149.68 1.83,123.00 1.83,123.00 0.00,104.00 0.00,104.00 0.00,104.00 0.00,77.00 0.00,77.00 0.01,69.43 0.87,67.05 1.72,60.00 1.72,60.00 2.29,51.00 2.29,51.00 3.46,41.48 5.92,28.18 8.56,19.00 8.56,19.00 15.00,0.00 15.00,0.00 15.00,0.00 28.00,7.38 28.00,7.38 Z", "x": 61, "y": 79, "height": 260,"texture":"assets/img/textures/white-cotton.jpg"},{"name": "handle", "path": "M 54.00,6.69 C 81.57,16.70 98.12,41.49 101.16,70.00 101.16,70.00 101.92,77.00 101.92,77.00 101.92,91.20 96.79,108.85 89.55,121.00 79.07,138.57 61.25,153.40 43.00,162.40 43.00,162.40 25.00,169.80 25.00,169.80 18.41,172.44 6.40,177.84 0.00,179.00 0.00,179.00 9.00,147.00 9.00,147.00 18.95,146.19 27.60,142.15 36.00,136.94 55.35,124.93 76.01,101.47 78.82,78.00 79.50,72.32 78.84,67.51 77.66,62.00 76.26,55.42 75.09,49.72 71.16,44.00 58.35,25.34 32.33,22.99 12.00,28.00 12.00,28.00 6.00,0.00 6.00,0.00 24.04,0.00 36.63,0.39 54.00,6.69 Z", "x": 268, "y": 97, "height": 168,"pattern":"assets/img/patterns/stars-1.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/browsewire.png","editorMode": true, "draggable": true, "resizeToW": 92, "x": 126, "y": 113}},{"type": "text", "params": {"src": "BROWSEWIRE", "editorMode": true, "draggable": true, "x": 98, "y":181,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#6E95CE"], "textSize": 20,"stroke":"#28272C"}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product  -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-02/mug-thumbnail.png" alt="White  Rounded Mugs" title="White Mugs">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-02/left-preview.png", "base": "assets/img/products/mug-02/left-base.png", "paths": [{"name": "base-front", "path": "M 73.00,4.39 C 101.71,9.80 130.90,10.00 160.00,10.00 189.08,10.00 218.31,9.80 247.00,4.39 247.00,4.39 264.00,0.00 264.00,0.00 268.82,19.11 268.03,50.74 268.00,71.00 268.00,71.00 267.09,83.00 267.09,83.00 267.09,83.00 265.83,104.00 265.83,104.00 264.29,120.09 259.55,145.55 254.96,161.00 247.60,185.80 237.38,210.42 223.00,232.00 220.02,236.47 211.50,249.65 206.96,251.40 205.11,252.12 202.02,252.00 200.00,252.00 200.00,252.00 178.00,252.00 178.00,252.00 178.00,252.00 161.00,251.00 161.00,251.00 161.00,251.00 120.00,251.00 120.00,251.00 117.91,251.00 114.94,251.12 113.00,250.40 108.13,248.61 100.57,238.37 97.24,234.00 88.24,222.22 78.69,204.00 73.69,190.00 73.69,190.00 68.14,173.00 68.14,173.00 67.56,170.96 66.03,164.72 65.02,163.31 63.23,160.83 57.09,159.80 54.00,158.55 47.40,155.88 41.87,151.94 36.00,148.00 31.20,144.78 26.12,141.05 22.04,136.96 10.28,125.19 0.08,104.87 0.00,88.00 0.00,88.00 0.00,81.00 0.00,81.00 0.21,63.36 9.43,46.19 27.00,40.77 37.51,37.53 41.97,38.78 52.00,39.00 52.00,29.36 53.30,8.82 56.00,0.00 56.00,0.00 73.00,4.39 73.00,4.39 Z M 37.00,55.33 C 23.07,57.77 15.07,69.46 15.00,83.00 15.00,83.00 15.00,89.00 15.00,89.00 15.21,104.77 28.93,125.85 42.00,134.05 49.92,139.02 52.67,138.90 61.00,141.00 61.00,141.00 56.51,125.00 56.51,125.00 56.51,125.00 52.00,83.00 52.00,83.00 52.00,83.00 53.00,56.00 53.00,56.00 48.05,55.11 41.98,54.45 37.00,55.33 Z", "x": 74, "y": 90, "height": 237,"pattern":"assets/img/patterns/pattern-17.png"}],"template":[{"type": "text", "params": {"src": "Coffee", "editorMode": true, "draggable": true, "x": 193, "y":115,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "is", "editorMode": true, "draggable": true, "x": 217, "y":141,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "always", "editorMode": true, "draggable": true, "x": 193, "y":170,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "a", "editorMode": true, "draggable": true, "x": 215, "y":193,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "GOOD", "editorMode": true, "draggable": true, "x": 199, "y":225,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "Idea", "editorMode": true, "draggable": true, "x": 201, "y":265,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-02/right-preview.png", "base": "assets/img/products/mug-02/right-base.png", "paths": [{"name": "base-front", "path": "M 25.00,5.39 C 41.27,8.32 65.39,9.97 82.00,10.00 82.00,10.00 97.00,11.00 97.00,11.00 97.00,11.00 118.00,11.00 118.00,11.00 118.00,11.00 131.00,10.02 131.00,10.02 131.00,10.02 146.00,10.02 146.00,10.02 146.00,10.02 156.00,9.09 156.00,9.09 174.59,7.82 194.49,6.95 212.00,0.00 214.70,8.82 216.00,29.36 216.00,39.00 226.03,38.78 230.49,37.53 241.00,40.77 258.57,46.19 267.79,63.36 268.00,81.00 268.00,81.00 268.00,87.00 268.00,87.00 267.96,111.86 251.99,134.58 232.00,148.00 226.13,151.94 220.60,155.88 214.00,158.55 210.91,159.80 204.77,160.83 202.98,163.31 201.97,164.72 200.44,170.96 199.86,173.00 199.86,173.00 194.31,190.00 194.31,190.00 189.31,204.00 179.76,222.22 170.76,234.00 167.43,238.37 159.87,248.61 155.00,250.40 153.06,251.12 150.09,251.00 148.00,251.00 148.00,251.00 107.00,251.00 107.00,251.00 107.00,251.00 91.00,252.00 91.00,252.00 91.00,252.00 68.00,252.00 68.00,252.00 65.98,252.00 62.89,252.12 61.04,251.40 56.50,249.65 47.98,236.47 45.00,232.00 30.62,210.42 20.40,185.80 13.04,161.00 8.45,145.55 3.71,120.09 2.17,104.00 2.17,104.00 0.91,83.00 0.91,83.00 0.91,83.00 0.00,71.00 0.00,71.00 -0.03,50.74 -0.82,19.11 4.00,0.00 4.00,0.00 25.00,5.39 25.00,5.39 Z M 226.00,134.05 C 238.51,126.20 249.84,108.54 252.81,94.00 253.14,91.30 253.00,86.87 252.81,84.00 252.96,60.66 236.05,52.23 215.00,56.00 215.00,56.00 216.00,83.00 216.00,83.00 216.00,83.00 211.49,125.00 211.49,125.00 211.49,125.00 207.00,141.00 207.00,141.00 215.33,138.90 218.08,139.02 226.00,134.05 Z", "x": 74, "y": 91, "height":236,"pattern":"assets/img/patterns/pattern-17.png"}],"template":[{"type": "text", "params": {"src": "You", "editorMode": true, "draggable": true, "x": 160, "y":111,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#B3042B"], "textSize": 40}},{"type": "text", "params": {"src": "are", "editorMode": true, "draggable": true, "x": 228, "y":113,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#B3042B"], "textSize": 15}},{"type": "text", "params": {"src": "the", "editorMode": true, "draggable": true, "x": 230, "y":130,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#B3042B"], "textSize": 15}},{"type": "text", "params": {"src": "Cream", "editorMode": true, "draggable": true, "x": 181, "y":156,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#D1316F"], "textSize":30}},{"type": "text", "params": {"src": "in my", "editorMode": true, "draggable": true, "x": 186, "y":193,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#891C2F"], "textSize": 30}},{"type": "text", "params": {"src": "Coffee", "editorMode": true, "draggable": true, "x":178, "y":224,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#D1316F"], "textSize": 30}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product  -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-03/mug-thumbnail.png" alt="Blue Blend  Coffee Mugs" title="Blue Blend  Coffee Mugs">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-03/left-preview.png", "base": "assets/img/products/mug-03/left-base.png", "paths": [{"name": "base-front", "path": "M 68.00,21.15 C 68.00,21.15 98.00,24.00 98.00,24.00 98.00,24.00 135.00,24.00 135.00,24.00 135.00,24.00 165.00,21.15 165.00,21.15 190.98,18.04 209.27,14.87 232.00,1.00 232.00,1.00 232.00,276.00 232.00,276.00 231.98,287.04 228.09,289.42 219.00,295.30 207.78,302.56 195.04,306.78 182.00,309.40 162.63,313.30 142.70,314.03 123.00,314.00 123.00,314.00 103.00,313.00 103.00,313.00 89.60,312.84 54.94,308.39 42.00,304.85 28.75,301.22 5.94,289.92 1.65,276.00 0.89,273.55 1.00,270.56 1.00,268.00 1.00,268.00 1.00,93.00 1.00,93.00 1.00,93.00 0.00,78.00 0.00,78.00 0.00,78.00 0.00,0.00 0.00,0.00 22.23,14.68 42.05,18.04 68.00,21.15 Z", "x": 136, "y": 86, "height": 262,"texture":"assets/img/textures/blue-cotton-blend.jpg"},{"name": "handle", "path": "M 48.00,233.63 C 22.56,220.90 12.40,191.80 6.13,166.00 3.88,156.75 2.85,147.44 1.84,138.00 -1.46,107.17 -0.94,88.81 9.00,59.00 16.16,37.53 27.32,18.33 48.00,7.32 59.00,1.47 63.83,2.00 75.00,0.00 75.00,0.00 75.00,23.00 75.00,23.00 71.50,23.01 68.35,22.87 65.00,24.14 50.47,29.66 40.91,54.21 36.33,68.00 33.92,75.25 28.88,92.84 28.17,100.00 27.63,105.43 29.19,119.00 29.84,125.00 31.98,145.05 36.87,165.91 45.97,184.00 50.33,192.66 56.91,201.33 64.00,207.91 73.69,216.90 75.85,213.62 76.00,222.00 76.00,222.00 76.00,240.00 76.00,240.00 66.88,239.26 56.24,237.75 48.00,233.63 Z", "x": 72, "y": 90, "height": 200,"texture":"assets/img/textures/light-brown.jpg"}],"template":[{"type": "text", "params": {"src": "FAILURE", "editorMode": true, "draggable": true, "x":174, "y":146,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#FED348"], "textSize": 30}},{"type": "text", "params": {"src": "IS THE", "editorMode": true, "draggable": true, "x":176, "y":181,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#FFFFFF"], "textSize": 25}},{"type": "text", "params": {"src": "Opportunity", "editorMode": true, "draggable": true, "x":173, "y":206,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#F0C6DC"], "textSize": 25}},{"type": "text", "params": {"src": "TO", "editorMode": true, "draggable": true, "x":218, "y":237,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#FED348"], "textSize": 20}},{"type": "text", "params": {"src": "BEGIN AGAIN", "editorMode": true, "draggable": true, "x":173, "y":263,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#FFFFFF"], "textSize": 20}}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-03/right-preview.png", "base": "assets/img/products/mug-03/right-base.png", "paths": [{"name": "base-front", "path": "M 232.00,29.00 C 232.00,29.00 232.00,78.00 232.00,78.00 232.00,78.00 231.00,95.00 231.00,95.00 231.00,95.00 231.00,268.00 231.00,268.00 231.00,270.56 231.11,273.55 230.35,276.00 226.06,289.92 203.25,301.22 190.00,304.85 177.06,308.39 142.40,312.84 129.00,313.00 129.00,313.00 109.00,314.00 109.00,314.00 89.30,314.03 69.37,313.30 50.00,309.40 36.96,306.78 24.22,302.56 13.00,295.30 3.91,289.42 0.02,287.04 0.00,276.00 0.00,276.00 0.00,1.00 0.00,1.00 22.73,14.87 41.02,18.04 67.00,21.15 67.00,21.15 97.00,24.00 97.00,24.00 127.43,24.05 140.17,24.68 171.00,20.27 189.92,17.57 216.47,12.99 231.00,0.00 232.91,4.83 232.00,22.74 232.00,29.00 Z M 132.70,122.26 C 134.84,121.61 135.81,117.21 130.43,117.63 127.06,120.77 130.73,122.85 132.70,122.26 Z", "x": 71, "y":86, "height":262,"texture":"assets/img/textures/blue-cotton-blend.jpg"},{"name": "handle", "path": "M 28.00,7.32 C 48.68,18.33 59.84,37.53 67.00,59.00 76.94,88.81 77.46,107.17 74.16,138.00 73.15,147.44 72.12,156.75 69.87,166.00 63.60,191.80 53.44,220.90 28.00,233.63 19.76,237.75 9.12,239.26 0.00,240.00 0.00,240.00 0.00,223.00 0.00,223.00 0.06,213.25 1.68,217.49 12.00,207.91 19.09,201.33 25.67,192.66 30.03,184.00 39.13,165.91 44.02,145.05 46.16,125.00 46.81,119.00 48.37,105.43 47.83,100.00 47.12,92.84 42.08,75.25 39.67,68.00 35.09,54.21 25.53,29.66 11.00,24.14 7.65,22.87 4.50,23.01 1.00,23.00 1.00,23.00 1.00,0.00 1.00,0.00 12.17,2.00 17.00,1.47 28.00,7.32 Z", "x": 266, "y": 91, "height": 200,"texture":"assets/img/textures/light-brown.jpg"}],"template":[{"type": "text", "params": {"src": "If you can", "editorMode": true, "draggable": true, "x":107, "y":133,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FED348"], "textSize": 30}},{"type": "text", "params": {"src": "dream it ...", "editorMode": true, "draggable": true, "x":108, "y":174,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FFFFFF"], "textSize": 30}},{"type": "text", "params": {"src": "You can do it", "editorMode": true, "draggable": true, "x":92, "y":209,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#F0C6DC"], "textSize": 25}},{"type": "text", "params": {"src": "Walt Disney", "editorMode": true, "draggable": true, "x":141, "y":250,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FED348"], "textSize": 18}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
										
										<!-- product  -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-04/mug-thumbnail.png" alt="Blue Blend  Oval Mugs" title="Blue Blend  Oval Mugs">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-04/left-preview.png", "base": "assets/img/products/mug-04/left-base.png", "paths": [{"name": "base-front", "path": "M 96.00,4.15 C 133.35,-0.31 182.85,-2.44 220.00,2.58 220.00,2.58 256.00,8.79 256.00,8.79 261.26,9.96 270.71,12.09 275.00,15.00 264.20,17.72 230.02,21.98 219.00,22.00 219.00,22.00 200.00,23.04 200.00,23.04 200.00,23.04 186.00,23.04 186.00,23.04 186.00,23.04 174.00,24.00 174.00,24.00 174.00,24.00 150.00,24.00 150.00,24.00 150.00,24.00 133.00,23.00 133.00,23.00 133.00,23.00 94.00,22.00 94.00,22.00 94.00,22.00 60.00,18.13 60.00,18.13 54.07,17.28 44.00,15.97 39.00,13.00 39.00,13.00 96.00,4.15 96.00,4.15 Z M 318.27,31.00 C 318.27,31.00 308.87,80.00 308.87,80.00 304.75,96.53 301.96,110.98 295.55,127.00 279.67,166.71 250.92,203.42 210.00,218.94 192.86,225.44 180.87,226.35 163.00,227.49 155.06,227.99 138.05,226.02 130.00,224.40 87.18,215.78 57.32,192.27 36.31,154.00 26.68,136.45 20.90,117.04 15.12,98.00 7.43,72.69 2.92,56.79 1.00,30.00 1.00,30.00 1.00,22.00 1.00,22.00 1.00,22.00 0.00,11.00 0.00,11.00 10.88,15.54 11.64,18.00 26.00,22.28 47.70,28.75 70.58,31.00 93.00,33.17 93.00,33.17 138.00,36.00 138.00,36.00 138.00,36.00 150.00,36.95 150.00,36.95 150.00,36.95 166.00,36.95 166.00,36.95 166.00,36.95 181.00,35.96 181.00,35.96 181.00,35.96 197.00,35.96 197.00,35.96 197.00,35.96 207.00,35.00 207.00,35.00 227.99,34.76 261.63,30.54 282.00,25.47 295.56,22.10 308.42,18.62 319.00,9.00 320.69,13.26 318.98,25.99 318.27,31.00 Z M 155.00,231.00 C 168.45,231.06 181.90,229.23 195.00,226.12 195.00,226.12 211.00,222.00 211.00,222.00 207.05,233.76 203.57,236.19 213.00,247.00 213.00,247.00 111.00,247.00 111.00,247.00 111.00,247.00 113.66,236.00 113.66,236.00 113.66,236.00 110.00,221.00 110.00,221.00 124.12,226.99 139.56,230.93 155.00,231.00 Z", "x": 71, "y": 100, "height": 231,"texture":"assets/img/textures/blue-cotton-blend.jpg"},{"name": "handle", "path": "M 46.00,87.00 C 31.16,87.02 18.29,84.42 8.52,72.00 -0.77,60.20 -1.31,44.29 0.73,30.00 2.11,20.34 3.16,14.05 11.01,7.21 18.35,0.83 27.71,0.01 37.00,0.00 37.00,0.00 40.00,19.00 40.00,19.00 20.24,14.38 12.22,38.05 19.33,53.00 26.12,67.29 41.09,69.00 55.00,69.00 55.00,69.00 61.00,86.00 61.00,86.00 61.00,86.00 46.00,87.00 46.00,87.00 Z", "x": 36, "y": 144, "height": 84,"pattern":"assets/img/patterns/dragon2.jpg"},{"name": "upper", "path": "M 177.00,0.21 C 177.00,0.21 194.00,1.00 194.00,1.00 220.46,1.04 247.00,2.80 273.00,8.00 284.48,10.30 298.94,14.02 309.00,20.02 312.46,22.08 318.20,25.96 319.34,30.00 320.36,33.60 317.60,36.26 314.98,38.21 309.66,42.19 301.33,45.09 295.00,47.16 270.87,55.07 230.56,59.70 205.00,60.00 205.00,60.00 192.00,61.00 192.00,61.00 192.00,61.00 168.00,61.00 168.00,61.00 168.00,61.00 154.00,61.82 154.00,61.82 154.00,61.82 143.00,61.00 143.00,61.00 143.00,61.00 115.00,60.00 115.00,60.00 115.00,60.00 95.00,58.17 95.00,58.17 95.00,58.17 69.00,55.71 69.00,55.71 69.00,55.71 49.00,52.28 49.00,52.28 36.90,50.25 21.87,46.90 11.00,41.22 7.74,39.51 1.21,36.08 0.90,31.96 0.66,28.66 3.70,25.59 6.04,23.68 11.33,19.36 20.46,15.44 27.00,13.34 45.99,7.26 72.04,3.46 92.00,2.09 92.00,2.09 108.00,1.00 108.00,1.00 113.14,0.94 115.73,1.17 121.00,0.21 121.00,0.21 177.00,0.21 177.00,0.21 Z M 90.00,6.17 C 72.79,7.81 52.71,10.53 36.00,14.74 29.55,16.37 12.14,21.78 12.37,29.96 12.55,36.30 26.88,40.96 32.00,42.67 51.67,49.23 79.25,52.76 100.00,53.00 100.00,53.00 113.00,54.00 113.00,54.00 113.00,54.00 128.00,54.00 128.00,54.00 128.00,54.00 144.00,55.00 144.00,55.00 144.00,55.00 183.00,55.00 183.00,55.00 183.00,55.00 196.00,54.00 196.00,54.00 196.00,54.00 223.00,53.00 223.00,53.00 223.00,53.00 257.00,49.42 257.00,49.42 270.11,47.57 283.65,45.56 296.00,40.55 303.15,37.65 314.86,31.85 304.89,23.61 303.59,22.53 301.53,21.37 300.00,20.63 290.99,16.28 273.20,12.96 263.00,11.00 263.00,11.00 220.00,6.00 220.00,6.00 220.00,6.00 203.00,5.00 203.00,5.00 203.00,5.00 182.00,4.14 182.00,4.14 182.00,4.14 118.00,4.14 118.00,4.14 118.00,4.14 90.00,6.17 90.00,6.17 Z M 176.00,7.21 C 176.00,7.21 188.00,7.96 188.00,7.96 188.00,7.96 201.00,7.96 201.00,7.96 201.00,7.96 217.00,9.00 217.00,9.00 227.07,9.05 249.82,11.78 260.00,13.61 271.72,15.72 285.97,18.41 297.00,22.71 305.54,26.04 308.68,30.73 299.00,35.73 295.60,37.48 285.62,41.25 282.00,41.02 279.80,40.88 274.56,38.67 272.00,37.88 272.00,37.88 251.00,32.42 251.00,32.42 251.00,32.42 211.00,26.04 211.00,26.04 211.00,26.04 202.00,26.04 202.00,26.04 202.00,26.04 192.00,25.00 192.00,25.00 192.00,25.00 172.00,24.00 172.00,24.00 172.00,24.00 144.00,25.00 144.00,25.00 144.00,25.00 133.00,25.96 133.00,25.96 104.14,27.22 75.37,31.75 47.00,37.20 34.38,39.62 34.17,41.43 22.00,35.24 19.40,33.92 14.69,31.82 16.17,28.09 17.13,25.66 20.77,23.87 23.00,22.78 29.42,19.66 36.03,17.81 43.00,16.40 59.39,13.10 76.32,10.23 93.00,9.09 93.00,9.09 110.00,8.00 110.00,8.00 115.42,7.94 118.46,8.21 124.00,7.21 124.00,7.21 176.00,7.21 176.00,7.21 Z", "x": 72, "y": 75, "height": 58,"pattern":"assets/img/patterns/dragon2.jpg"}],"template":[{"type": "text", "params": {"src": "ADVENTURE", "editorMode": true, "draggable": true, "x":163, "y":142,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FED348"], "textSize": 20}},{"type": "image", "params": {"src": "assets/img/sample/fish.png","editorMode": true, "draggable": true, "resizeToW": 105, "x": 171, "y": 157}},{"type": "text", "params": {"src": "RIVER BOAT TOURS", "editorMode": true, "draggable": true, "x":146, "y":256,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#FED348"], "textSize": 18}}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-04/right-preview.png", "base": "assets/img/products/mug-04/right-base.png", "paths": [{"name": "base-front", "path": "M 96.00,4.15 C 133.35,-0.31 182.85,-2.44 220.00,2.58 220.00,2.58 256.00,8.79 256.00,8.79 261.26,9.96 270.71,12.09 275.00,15.00 264.20,17.72 230.02,21.98 219.00,22.00 219.00,22.00 200.00,23.04 200.00,23.04 200.00,23.04 186.00,23.04 186.00,23.04 186.00,23.04 174.00,24.00 174.00,24.00 174.00,24.00 150.00,24.00 150.00,24.00 150.00,24.00 133.00,23.00 133.00,23.00 133.00,23.00 94.00,22.00 94.00,22.00 94.00,22.00 60.00,18.13 60.00,18.13 54.07,17.28 44.00,15.97 39.00,13.00 39.00,13.00 96.00,4.15 96.00,4.15 Z M 318.27,31.00 C 318.27,31.00 308.87,80.00 308.87,80.00 304.75,96.53 301.96,110.98 295.55,127.00 279.67,166.71 250.92,203.42 210.00,218.94 192.86,225.44 180.87,226.35 163.00,227.49 155.06,227.99 138.05,226.02 130.00,224.40 87.18,215.78 57.32,192.27 36.31,154.00 26.68,136.45 20.90,117.04 15.12,98.00 7.43,72.69 2.92,56.79 1.00,30.00 1.00,30.00 1.00,22.00 1.00,22.00 1.00,22.00 0.00,11.00 0.00,11.00 10.88,15.54 11.64,18.00 26.00,22.28 47.70,28.75 70.58,31.00 93.00,33.17 93.00,33.17 138.00,36.00 138.00,36.00 138.00,36.00 150.00,36.95 150.00,36.95 150.00,36.95 166.00,36.95 166.00,36.95 166.00,36.95 181.00,35.96 181.00,35.96 181.00,35.96 197.00,35.96 197.00,35.96 197.00,35.96 207.00,35.00 207.00,35.00 227.99,34.76 261.63,30.54 282.00,25.47 295.56,22.10 308.42,18.62 319.00,9.00 320.69,13.26 318.98,25.99 318.27,31.00 Z M 155.00,231.00 C 168.45,231.06 181.90,229.23 195.00,226.12 195.00,226.12 211.00,222.00 211.00,222.00 207.05,233.76 203.57,236.19 213.00,247.00 213.00,247.00 111.00,247.00 111.00,247.00 111.00,247.00 113.66,236.00 113.66,236.00 113.66,236.00 110.00,221.00 110.00,221.00 124.12,226.99 139.56,230.93 155.00,231.00 Z", "x": 34, "y":97, "height":232,"texture":"assets/img/textures/blue-cotton-blend.jpg"},{"name": "handle", "path": "M 51.82,8.32 C 57.51,13.73 59.25,20.58 60.41,28.00 62.76,42.97 62.84,59.40 52.96,71.96 43.21,84.35 29.79,87.02 15.00,87.00 15.00,87.00 0.00,86.00 0.00,86.00 1.29,82.76 5.10,71.67 7.50,70.02 9.56,68.60 15.88,69.10 19.00,68.54 26.53,67.18 33.72,63.83 38.79,57.99 44.16,51.79 44.09,46.68 44.00,39.00 43.83,25.06 35.02,16.62 21.00,19.00 21.00,19.00 24.00,0.00 24.00,0.00 34.10,0.00 44.07,0.96 51.82,8.32 Z", "x": 307, "y": 142, "height": 83,"pattern":"assets/img/patterns/dragon2.jpg"},{"name": "upper", "path": "M 175.00,0.21 C 209.40,0.00 244.25,2.03 278.00,9.21 288.74,11.50 301.89,15.20 311.00,21.36 314.19,23.51 319.77,27.63 319.46,31.99 319.18,36.05 313.22,39.18 310.00,40.88 300.61,45.83 286.48,49.45 276.00,51.55 276.00,51.55 233.00,58.00 233.00,58.00 233.00,58.00 225.00,58.00 225.00,58.00 225.00,58.00 206.00,60.04 206.00,60.04 206.00,60.04 196.00,60.04 196.00,60.04 196.00,60.04 186.00,61.00 186.00,61.00 186.00,61.00 158.00,62.00 158.00,62.00 158.00,62.00 143.00,61.00 143.00,61.00 143.00,61.00 128.00,61.00 128.00,61.00 128.00,61.00 115.00,60.00 115.00,60.00 115.00,60.00 66.00,55.93 66.00,55.93 53.29,54.73 26.40,48.73 15.00,43.45 11.04,41.61 0.88,36.74 0.54,31.96 0.30,28.62 3.66,25.43 6.02,23.52 12.54,18.22 21.97,14.66 30.00,12.29 52.27,5.73 74.97,3.49 98.00,1.91 98.00,1.91 110.00,1.00 110.00,1.00 110.00,1.00 134.00,0.21 134.00,0.21 134.00,0.21 175.00,0.21 175.00,0.21 Z M 118.00,5.00 C 118.00,5.00 99.00,6.00 99.00,6.00 83.85,6.07 48.93,11.92 34.00,16.14 27.70,17.92 20.25,19.44 15.19,23.76 12.32,26.21 10.28,29.30 12.62,32.91 15.22,36.93 21.66,39.45 26.00,41.05 37.13,45.13 50.23,47.73 62.00,49.15 83.03,51.70 103.78,53.97 125.00,54.00 125.00,54.00 142.00,55.00 142.00,55.00 142.00,55.00 174.00,55.00 174.00,55.00 174.00,55.00 189.00,54.00 189.00,54.00 189.00,54.00 204.00,54.00 204.00,54.00 204.00,54.00 221.00,53.00 221.00,53.00 239.61,52.97 268.16,48.75 286.00,43.41 291.43,41.78 299.26,38.76 303.96,35.67 306.61,33.93 309.16,31.55 308.16,28.04 307.08,24.29 301.34,21.19 298.00,19.67 287.77,14.99 277.90,13.28 267.00,11.20 236.94,5.44 202.61,4.00 172.00,4.14 172.00,4.14 132.00,4.14 132.00,4.14 126.03,5.14 123.88,4.99 118.00,5.00 Z M 287.00,39.45 C 284.33,39.70 273.40,37.09 270.00,36.46 270.00,36.46 233.00,30.29 233.00,30.29 233.00,30.29 195.00,26.00 195.00,26.00 195.00,26.00 186.00,26.00 186.00,26.00 186.00,26.00 175.00,25.00 175.00,25.00 175.00,25.00 147.00,24.00 147.00,24.00 147.00,24.00 130.00,25.00 130.00,25.00 115.39,25.02 89.44,28.28 75.00,31.34 75.00,31.34 53.00,36.42 53.00,36.42 39.54,40.36 41.19,42.89 29.00,38.83 25.58,37.69 15.20,34.27 15.20,29.92 15.20,24.59 26.87,21.28 31.00,20.15 48.77,15.28 64.86,13.00 83.00,10.58 97.45,8.65 112.43,8.02 127.00,8.00 127.00,8.00 146.00,7.00 146.00,7.00 180.75,6.96 215.55,6.82 250.00,11.73 262.10,13.46 274.22,15.24 286.00,18.58 293.95,20.83 300.86,23.18 306.00,30.00 301.70,33.73 292.77,38.92 287.00,39.45 Z", "x": 33, "y": 72, "height": 59,"pattern":"assets/img/patterns/dragon2.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/fish-01.png","editorMode": true, "draggable": true, "resizeToW": 178, "x": 79, "y": 149}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
																											
										<!-- product  -->
										<div class="col-xs-8 top5 product">
											<img class="img-thumbnail" src="assets/img/products/mug-05/mug-thumbnail.png" alt="Black Color Oval Mugs" title="Black  Oval Mugs">
											<!-- product specifications -->
											<div class="product-specs">
												<div data-side="front" data-params='{"preview": "assets/img/products/mug-05/left-preview.png", "base": "assets/img/products/mug-05/left-base.png", "paths": [{"name": "base-front", "path": "M 318.27,22.00 C 318.27,22.00 308.87,71.00 308.87,71.00 308.87,71.00 297.95,112.00 297.95,112.00 282.86,153.14 253.14,193.67 211.00,209.58 192.93,216.40 181.82,217.29 163.00,218.49 155.06,218.99 138.05,217.02 130.00,215.40 87.18,206.78 57.32,183.27 36.31,145.00 27.17,128.36 21.58,110.03 15.98,92.00 7.33,64.19 3.96,52.32 1.04,23.00 1.04,23.00 1.04,13.00 1.04,13.00 1.04,13.00 0.00,2.00 0.00,2.00 10.88,6.54 11.64,9.00 26.00,13.28 47.70,19.75 70.58,22.00 93.00,24.17 93.00,24.17 138.00,27.00 138.00,27.00 138.00,27.00 150.00,27.95 150.00,27.95 150.00,27.95 166.00,27.95 166.00,27.95 166.00,27.95 181.00,26.96 181.00,26.96 181.00,26.96 197.00,26.96 197.00,26.96 197.00,26.96 207.00,26.00 207.00,26.00 227.59,25.76 262.01,21.45 282.00,16.47 295.56,13.10 308.42,9.62 319.00,0.00 320.69,4.26 318.98,16.99 318.27,22.00 Z M 207.20,225.00 C 207.20,225.00 210.00,238.00 210.00,238.00 210.00,238.00 140.00,238.00 140.00,238.00 133.55,238.00 112.73,238.88 108.00,237.00 117.44,228.98 113.60,222.70 110.00,213.00 110.00,213.00 137.00,219.54 137.00,219.54 159.15,223.54 189.92,223.11 210.00,212.00 210.00,212.00 207.20,225.00 207.20,225.00 Z", "x": 68, "y": 105, "height": 223,"texture":"assets/img/textures/black-blend.jpg"},{"name": "handle", "path": "M 46.00,87.00 C 31.16,87.02 18.29,84.42 8.52,72.00 -0.77,60.20 -1.31,44.29 0.73,30.00 2.11,20.34 3.16,14.05 11.01,7.21 18.35,0.83 27.71,0.01 37.00,0.00 37.00,0.00 40.00,19.00 40.00,19.00 20.24,14.38 12.22,38.05 19.33,53.00 26.12,67.29 41.09,69.00 55.00,69.00 55.00,69.00 61.00,86.00 61.00,86.00 61.00,86.00 46.00,87.00 46.00,87.00 Z", "x": 33, "y": 142, "height": 83,"pattern":"assets/img/patterns/pattern-3.png"},{"name": "upper", "path": "M 179.00,0.21 C 179.00,0.21 196.00,1.00 196.00,1.00 221.80,1.04 247.65,2.93 273.00,8.00 284.98,10.40 298.37,13.68 309.00,19.88 312.40,21.85 318.24,26.10 319.34,30.00 320.36,33.60 317.60,36.26 314.98,38.21 310.30,41.71 300.69,45.55 295.00,47.28 270.41,54.76 231.75,59.70 206.00,60.00 206.00,60.00 192.00,61.00 192.00,61.00 192.00,61.00 176.00,61.00 176.00,61.00 176.00,61.00 158.00,62.00 158.00,62.00 158.00,62.00 141.00,61.00 141.00,61.00 141.00,61.00 115.00,60.00 115.00,60.00 115.00,60.00 95.00,58.17 95.00,58.17 95.00,58.17 47.00,52.08 47.00,52.08 36.04,50.18 19.66,45.97 10.00,40.69 6.35,38.69 -0.43,35.07 0.99,30.00 3.33,21.59 21.27,15.05 29.00,12.72 48.73,6.78 69.54,4.12 90.00,2.17 90.00,2.17 107.00,1.00 107.00,1.00 111.93,0.95 113.94,1.14 119.00,0.21 119.00,0.21 179.00,0.21 179.00,0.21 Z", "x": 67, "y": 72, "height": 59,"pattern":"assets/img/patterns/pattern-9.png"}],"template":[{"type": "text", "params": {"src": "eat MORE", "editorMode": true, "draggable": true, "x":92, "y":137,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#FFFFFF"], "textSize": 25}},{"type": "text", "params": {"src": "Of WHAT", "editorMode": true, "draggable": true, "x":106, "y":169,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#FFFFFF"], "textSize": 25}},{"type": "text", "params": {"src": "YOU", "editorMode": true, "draggable": true, "x":128, "y":197,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#FFFFFF"], "textSize": 25}},{"type": "text", "params": {"src": "LOVE", "editorMode": true, "draggable": true, "x":129, "y":230,"fontWeight":"bold", "textAlign": "center", "font": "Shadows Into Light", "colors": ["#FFFFFF"], "textSize": 25}},{"type": "image", "params": {"src": "assets/img/sample/decoration.png","editorMode": true, "draggable": true, "resizeToW": 107, "x": 207, "y": 137}}]}'></div>
												
												<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-05/right-preview.png", "base": "assets/img/products/mug-05/right-base.png", "paths": [{"name": "base-front", "path": "M 16.00,8.71 C 37.42,17.95 63.93,20.93 87.00,23.17 87.00,23.17 138.00,26.00 138.00,26.00 138.00,26.00 150.00,26.96 150.00,26.96 150.00,26.96 165.00,26.96 165.00,26.96 165.00,26.96 177.00,26.04 177.00,26.04 177.00,26.04 189.00,26.04 189.00,26.04 189.00,26.04 202.00,25.00 202.00,25.00 202.00,25.00 244.00,21.28 244.00,21.28 263.03,18.91 287.47,15.87 305.00,8.14 305.00,8.14 320.00,0.00 320.00,0.00 320.00,0.00 315.73,43.00 315.73,43.00 313.10,59.99 307.87,76.58 302.86,93.00 292.02,128.55 278.85,160.62 250.00,185.71 211.00,219.64 146.50,226.95 100.00,205.11 83.38,197.31 68.27,184.78 56.29,171.00 40.88,153.28 25.99,126.48 19.02,104.00 19.02,104.00 9.35,65.00 9.35,65.00 6.13,50.77 0.02,21.81 0.00,8.00 0.00,8.00 0.00,0.00 0.00,0.00 6.72,3.76 8.32,5.40 16.00,8.71 Z M 123.00,216.44 C 129.66,218.34 145.18,220.92 152.00,221.00 165.42,221.15 178.91,219.47 192.00,216.50 192.00,216.50 210.00,212.00 210.00,212.00 203.66,224.07 203.35,225.78 211.00,237.00 211.00,237.00 109.00,237.00 109.00,237.00 114.14,224.05 111.82,224.34 109.00,212.00 109.00,212.00 123.00,216.44 123.00,216.44 Z", "x": 34, "y":107, "height":222,"texture":"assets/img/textures/black-blend.jpg"},{"name": "handle", "path": "M 51.82,8.32 C 57.51,13.73 59.25,20.58 60.41,28.00 62.76,42.97 62.84,59.40 52.96,71.96 43.21,84.35 29.79,87.02 15.00,87.00 15.00,87.00 0.00,86.00 0.00,86.00 1.29,82.76 5.10,71.67 7.50,70.02 9.56,68.60 15.88,69.10 19.00,68.54 26.53,67.18 33.72,63.83 38.79,57.99 44.16,51.79 44.09,46.68 44.00,39.00 43.83,25.06 35.02,16.62 21.00,19.00 21.00,19.00 24.00,0.00 24.00,0.00 34.10,0.00 44.07,0.96 51.82,8.32 Z", "x": 307, "y": 142, "height": 83,"pattern":"assets/img/patterns/pattern-3.png"},{"name": "upper", "path": "M 175.00,0.21 C 209.40,0.00 244.25,2.03 278.00,9.21 288.74,11.50 301.89,15.20 311.00,21.36 314.19,23.51 319.77,27.63 319.46,31.99 319.18,36.05 313.22,39.18 310.00,40.88 300.61,45.83 286.48,49.45 276.00,51.55 246.03,57.55 207.61,60.65 177.00,61.00 177.00,61.00 158.00,62.00 158.00,62.00 158.00,62.00 143.00,61.00 143.00,61.00 116.64,60.96 90.14,59.24 64.00,55.71 49.60,53.77 25.77,48.86 13.00,42.48 8.81,40.39 -0.77,35.85 0.84,30.00 1.73,26.79 5.40,23.89 8.00,22.04 14.91,17.14 23.89,14.05 32.00,11.71 53.95,5.37 76.36,3.46 99.00,1.91 99.00,1.91 111.00,1.00 111.00,1.00 111.00,1.00 134.00,0.21 134.00,0.21 134.00,0.21 175.00,0.21 175.00,0.21 Z", "x": 33, "y": 72, "height": 59,"pattern":"assets/img/patterns/pattern-9.png"}],"template":[{"type": "text", "params": {"src": "No Sugar", "editorMode": true, "draggable": true, "x": 61, "y":145,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#FFFFFF"], "textSize":25}},{"type": "text", "params": {"src": "I am", "editorMode": true, "draggable": true, "x": 88, "y":173,"fontWeight":"bold", "textAlign": "center", "font": "Dancing Script", "colors": ["#FFFFFF"], "textSize":25}},{"type": "text", "params": {"src": "Sweet enough", "editorMode": true, "draggable": true, "x": 64, "y":207,"fontWeight":"bold", "textAlign": "center", "font": "Architects Daughter", "colors": ["#FFFFFF"], "textSize":17}},{"type": "image", "params": {"src": "assets/img/sample/decoration.png","editorMode": true, "draggable": true, "resizeToW": 103, "x": 190, "y": 138}}]}'></div>
												
											</div>
											<!--/ product specifications -->
										</div>
										<!--/ product -->
									</div>
								</div>
								<!--/ Products -->

								<!-- Text Options Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-text-options">
									<p class="custom-pane-header text-center h3">Text Options</p><hr>
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#text" aria-controls="text" role="tab" data-toggle="tab">Text</a></li>
										<li><a role="presentation" href="#wordart" aria-controls="wordart" role="tab" data-toggle="tab">Art</a></li>
										<li><a role="presentation" href="#wordcloud" aria-controls="wordcloud" role="tab" data-toggle="tab">Cloud</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active text-center" id="text">
											<br/>
											<textarea class="form-control" id="text-content"></textarea>
											<br/>
											<select class="form-control input-lg" id="text-font-family">
												<option style="font-family:Arial" value="Arial">Arial</option>
												<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
												<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
												<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
												<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
												<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
												<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
												<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
												<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
												<option style="font-family:Chewy" value="Chewy">Chewy</option>
												<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>
											</select>
											<br/>
											<button id="btn-add-text" type="button" class="btn btn-default btn-lg">Add Text</button>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="wordart">
											<br>
											<div class="form-group">
												<textarea class="form-control" id="wordart-content"></textarea>
											</div>
											
											<select class="form-control input-lg" id="wordart-font-family">
												<option style="font-family:Arial" value="Arial">Arial</option>
												<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
												<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
												<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
												<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
												<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
												<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
												<option style="font-family:Chewy" value="Chewy">Chewy</option>
												<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>
											</select>
											<br/>
											<div class="row">
												<div class="col-xs-24">
													<!-- <div class="well"> -->
														<div class="carousel slide" id="myCarousel" data-interval="false">
															<!-- Carousel items -->
															<div class="carousel-inner wordarts">
																<div class="item active">
																	<div class="row-fluid">
																		<div class="col-xs-6 padding-sm"><img data-wordart="concave-top" class="img-thumbnail active" src="assets/img/wordarts/wordart-01.png" alt=""></div>
																		<div class="col-xs-6 padding-sm"><img data-wordart="concave-bottom" class="img-thumbnail" src="assets/img/wordarts/wordart-02.png" alt=""></div>
																		<div class="col-xs-6 padding-sm"><img data-wordart="concave-both" class="img-thumbnail" src="assets/img/wordarts/wordart-03.png" alt=""></div>
																		<div class="col-xs-6 padding-sm"><img data-wordart="triangle" class="img-thumbnail" src="assets/img/wordarts/wordart-04.png" alt=""></div>
																	</div>
																	<!--/row-fluid-->
																</div>
																<!--/item-->

																<div class="item">
																	<div class="row-fluid">
																		<div class="col-xs-6 padding-sm"><img data-wordart="curve-top" class="img-thumbnail" src="assets/img/wordarts/wordart-06.png" alt=""></div>
																	</div>
																	<!--/row-fluid-->
																</div>
																<!--/item-->
															</div>
															<!--/carousel-inner-->
															<a data-slide="prev" href="#myCarousel" class="left carousel-control">‹</a>
															<a data-slide="next" href="#myCarousel" class="right carousel-control">›</a>
														</div>
														<!--/myCarousel-->
													<!-- </div> -->
													<!--/well-->   
												</div>
											</div>											
											<div class="wordart-preview text-center hidden">
												<hr>
												<canvas id="demo" width=200 height=100></canvas>
												<br>
												<button class="btn btn-default btn-sm" id="add-wordart">Add Wordart</button>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade text-center" id="wordcloud">
											<br/>
											<textarea name="words-wordcloud" id="wordcloud-list" class="form-control"></textarea>
											<p class="help-block">Seperate words using comma <br>(eg. I,am,a,wordcloud)</p>
											<br/>
											<select class="form-control input-lg" id="wordcloud-font-family">
												<option style="font-family:Arial" value="Arial">Arial</option>
												<option style="font-family:Indie Flower" value="Indie Flower">Indie Flower</option>
												<option style="font-family:Poiret One" value="Poiret One">Poiret One</option>
												<option style="font-family:Trebuchet MS" value="Trebuchet MS">Trebuchet MS</option>
												<option style="font-family:Shadows Into Light" value="Shadows Into Light">Shadows Into Light</option>
												<option style="font-family:Pacifico" value="Pacifico">Pacifico</option>
												<option style="font-family:Quicksand" value="Quicksand">Quicksand</option>
												<option style="font-family:Architects Daughter" value="Architects Daughter">Architects Daughter</option>
												<option style="font-family:Dancing Script" value="Dancing Script">Dancing Script</option>
												<option style="font-family:Chewy" value="Chewy">Chewy</option>
												<option style="font-family:Gloria Hallelujah" value="Gloria Hallelujah">Gloria Hallelujah</option>
											</select>
											<br/>
											<select id="wordcloud-size" class="form-control input-lg">
												<option value="rectangle">Rectangle</option>
												<option value="square">Square</option>
											</select>
											<hr>
											<div class="form-group">
												<button class="btn btn-default btn-sm form-control" id="generate-wordcloud">Generate Wordcloud</button>
											</div>											
											
											<div class="wordcloud-preview text-center hidden">
												<br>
												<canvas id="wordcloud_canvas"></canvas>
												<br>
												<button class="btn btn-default btn-sm" id="add-wordcloud">Add Wordcloud</button>
											</div>
										</div>
									</div>
									<br/>
								</div>
								<!--/ Text Options Pane -->
								
								<!-- Image Options Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-image-options">
									<p class="custom-pane-header text-center h3">Image Options</p><hr>									
									<ul class="nav nav-tabs" role="tablist">
										<li class="active"><a role="presentation" href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
										<li><a role="presentation" href="#uploads" aria-controls="uploads" role="tab" data-toggle="tab">Uploads</a></li>
									</ul>

									<div class="tab-content">
										<div role="tabpanel" class="tab-pane fade in active" id="gallery">
											<br>
											<input type="text" id="search-gallery" placeholder="Search Gallery..." class="form-control">
											<div class="row display-gallery">
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Nike" alt="Nike" src="assets/img/sample/nike.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Adidas" alt="Adidas" src="assets/img/sample/adidas.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Being Human" alt="Being Human" src="assets/img/sample/being-human.png" alt=""></div>
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Puma" alt="Puma" src="assets/img/sample/puma.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Tattoo" alt="Tattoo" src="assets/img/sample/tattoos-01.png" alt=""></div>
												
												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Camera" alt="Camera" src="assets/img/sample/camera-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Dragon" alt="Dragon" src="assets/img/sample/dragon-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Hearts" alt="Hearts" src="assets/img/sample/hearts-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Happy" alt="Happy" src="assets/img/sample/happy-01.png" alt=""></div>

												<div class="col-xs-6 top5"><img class="img-thumbnail" title="Sad" alt="Sad" src="assets/img/sample/sad-01.png" alt=""></div>
											</div>
										</div>
										<div role="tabpanel" class="tab-pane fade" id="uploads">
											<div class="row display-gallery">
												<div class="col-sm-24 text-center">
													<br>
													<button id="btn-add-img" type="button" class="btn btn-default btn-lg"><i class="mdi mdi-upload"></i>Upload Image</button>
													<input type="file" id="add-img" style="display: none;" />
												</div>
											</div>
										</div>
									</div>
									<hr>
									<div class="form-group">
										<button class="btn btn-default btn-sm form-control" id="generate-svg">Generate SVG</button>
										<div id="dwn"></div>
									</div>
								</div>
								<!-- Image Options Pane -->

								<!-- Layering Options -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-layering-options">
									<p class="custom-pane-header text-center h3">Layers</p><hr>
									<!-- <table class="table table-hover">
										<tbody id="layers">
											<tr>
												<td><span>☰&nbsp;</span>Base Product Price</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Product - Colors</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Product - Textures</td>
											</tr>												
											<tr>
												<td><span>☰&nbsp;</span>Product - Patterns</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Texts</td>
											</tr>
											<tr>
												<td><span>☰&nbsp;</span>Images</td>
											</tr>
										</tbody>
									</table> -->
									<ul id="layers" class="list-group">
										<!-- <li class="list-group-item"><span>☰&nbsp;</span>This is Sortable</li>
										<li class="list-group-item"><span>☰&nbsp;</span>It works with Bootstrap...</li>
										<li class="list-group-item"><span>☰&nbsp;</span>...out of the box.</li>
										<li class="list-group-item"><span>☰&nbsp;</span>It has support for touch devices.</li>
										<li class="list-group-item"><span>☰&nbsp;</span>Just drag some elements around.</li> -->
									</ul>
								</div>
								<!--/ Layering Options -->

								<!-- Cart Calculations Pane -->
								<div role="tabpanel" class="tab-pane fade" id="tabpane-price-calculation-options">
									<p class="custom-pane-header text-center h3">Price Calculations</p><hr>
									<!-- table -->
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Size</th>
												<th class="text-center">#</th>
												<th class="text-center">Total</th>
											</tr>
										</thead>
											<tbody>
												<tr data-toggle="popover" data-popover-size="xs" title="Name & Number" data-html="true" data-content="<div data-nn-size='xs'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>XS</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="xs" class="sizes xs_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="xs_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="s" title="Name & Number" data-html="true" data-content="<div data-nn-size='s'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>S</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="s" class="sizes s_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="s_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="m" title="Name & Number" data-html="true" data-content="<div data-nn-size='m'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>M</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="m" class="sizes m_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="m_total">0</span></td>
												</tr>												
												<tr data-toggle="popover" data-popover-size="l" title="Name & Number" data-html="true" data-content="<div data-nn-size='l'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>L</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="l" class="sizes l_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="l_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="xl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>XL</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="xl" class="sizes xl_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="xl_total">0</span></td>
												</tr>
												<tr data-toggle="popover" data-popover-size="xxl" title="Name & Number" data-html="true" data-content="<div data-nn-size='xxl'><div class='row top3'><div class='col-xs-16'><input type='text' class='form-control input-sm' placeholder='Name'></div><div class='col-xs-8'><input type='text' class='form-control input-sm' placeholder='#'></div></div>">
													<td class="col-sm-6"><em>XXL</em></td>
													<td class="col-sm-2 text-center"><input type="text" data-input-size="xxl" class="sizes xxl_count form-control input-sm"></td>
													<td class="col-sm-2 text-center">$<span class="xxl_total">0</span></td>
												</tr>
												<tr>
													<td></td>													
													<td class="text-center">
														<h4><strong>Total</strong></h4>
													</td>
													<td class="text-center text-danger">
														<h4><strong>$<span class="total_price">0.00</span></strong></h4>	
													</td>
												</tr>
											</tbody>
									</table>
									<!--/ table -->
									
									<!-- <div class="form-group">
										<button class="btn btn-default btn-sm form-control" id="generate-image">Generate Image</button>
									</div> -->
									<h4>Details: </h4>
									<em class="used_objects">None</em>
									<br><br>
									<button class="btn btn-lg btn-block btn-default">Add To Cart</button>
								</div>
								<!--/ Cart Calculations Pane -->
							</div>
							<!--/ Tab Panes -->
							<br>
						</div>
						<div class="col-sm-18 col-md-12 popup-options">
							<span class="visible-md visible-lg"><br><br><br><br></span>
							<span class="visible-sm"><hr></span>
							<div class="popup-text-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-fill">
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Stroke</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-stroke">
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-stroke="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Pattern</div>
									<div class="pull-right">
										<div class="pattern-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="text-font-pattern">
									<div class="col-sm-8 col-md-6"><div class="well" data-src="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/all-pink.jpg" style="background: url('assets/img/textures/all-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-blend.jpg" style="background: url('assets/img/textures/black-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton.jpg" style="background: url('assets/img/textures/black-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-span.jpg" style="background: url('assets/img/textures/black-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-splasm.jpg" style="background: url('assets/img/textures/black-cotton-splasm.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-blend.jpg" style="background: url('assets/img/textures/blue-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-cotton-blend.jpg" style="background: url('assets/img/textures/blue-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/brown-blend.jpg" style="background: url('assets/img/textures/brown-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/collon-blend.jpg" style="background: url('assets/img/textures/collon-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink.jpg" style="background: url('assets/img/textures/coral-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink-cotton.jpg" style="background: url('assets/img/textures/coral-pink-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/dark-grey.jpg" style="background: url('assets/img/textures/dark-grey.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/emerald-cotton-blend.jpg" style="background: url('assets/img/textures/emerald-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/light-brown.jpg" style="background: url('assets/img/textures/light-brown.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/red-blend.jpg" style="background: url('assets/img/textures/red-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise.jpg" style="background: url('assets/img/textures/turquoise.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise-cotton-span.jpg" style="background: url('assets/img/textures/turquoise-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-black.jpg" style="background: url('assets/img/textures/white-black.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-blue.jpg" style="background: url('assets/img/textures/white-blue.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-cotton.jpg" style="background: url('assets/img/textures/white-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>

									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/dragon2.jpg" style="background: url('assets/img/patterns/dragon2.jpg') repeat scroll 0 0; background-size: cover"></div></div>						
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-1.jpg" style="background: url('assets/img/patterns/stars-1.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-4.jpg" style="background: url('assets/img/patterns/stars-4.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/balls.jpg" style="background: url('assets/img/patterns/balls.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-1.png" style="background: url('assets/img/patterns/pattern-1.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-2.png" style="background: url('assets/img/patterns/pattern-2.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-3.png" style="background: url('assets/img/patterns/pattern-3.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-4.png" style="background: url('assets/img/patterns/pattern-4.png') repeat scroll 0 0; background-size: cover"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-group btn-group-justified" role="group" id="text-properties">
									<div class="btn-group" role="group">
										<button type="button" id="text-bold" class="btn btn-default"><i class="fa fa-bold"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-italic" class="btn btn-default"><i class="fa fa-italic"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-underline" class="btn btn-default"><i class="fa fa-underline"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-line-through" class="btn btn-default"><i class="fa fa-strikethrough"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-overline" class="btn btn-default"><img style="width:14px;height:14px" src="assets/img/control-icons/text-overline.svg" alt="O"></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-left" class="btn btn-default"><i class="fa fa-align-left"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-center" class="btn btn-default"><i class="fa fa-align-center"></i></button>
									</div>
									<div class="btn-group" role="group">
										<button type="button" id="text-align-right"  class="btn btn-default"><i class="fa fa-align-right"></i></button>
									</div>
								</div>
								<br>
								<!-- <div class="row">
									<div class="col-xs-24">
										<div class="pull-left"><b>Text Curve:</b></div>
										<div class="pull-right"><input type="checkbox" id="text-curve-switch"></div>
									</div>
								</div>
								<br>
								<div class="row text-curve-prop">
									<div class="col-xs-12">
										<b>Curve Radius:</b><br><br><div id="text-curve-radius"></div>
									</div>
									<div class="col-xs-12">
										<b>Curve Space:</b><br><br><div id="text-curve-spacing"></div>
										<br>
									</div>
								</div> -->
								<div class="row">
									<div class="col-xs-12">
										<b>Height:</b><br><br><div id="text-line-height"></div>
									</div>
									<div class="col-xs-12">
										<b>Opacity:</b><br><br><div id="text-opacity"></div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-24">
										<b>Text Shadow:</b><br><br><div id="text-shadow"></div>
									</div>
								</div>
							</div>
							<div class="popup-image-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="image-fill">
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#000000" style="background-color: #000000"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="row">
									<div class="col-xs-24">
										<b>Opacity:</b><br><br><div id="image-opacity"></div>
									</div>
								</div>
							</div>
							<div class="popup-product-options hide">
								<div class="btn-lg color-selector">
									<div class="pull-left">Color</div>
									<div class="pull-right">
										<div class="color-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="base-path-fill">
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#A2C44C" style="background-color: #A2C44C"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#28272c" style="background-color: #28272c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e975a4" style="background-color: #e975a4"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#891c2f" style="background-color: #891c2f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#6e95ce" style="background-color: #6e95ce"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b3042b" style="background-color: #b3042b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#7d5b52" style="background-color: #7d5b52"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#fed348" style="background-color: #fed348"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#3c3230" style="background-color: #3c3230"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#333a32" style="background-color: #333a32"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#feac1c" style="background-color: #feac1c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#d1316f" style="background-color: #d1316f"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#edb963" style="background-color: #edb963"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#c2bebf" style="background-color: #c2bebf"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#4a5d7b" style="background-color: #4a5d7b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#497bba" style="background-color: #497bba"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#00995b" style="background-color: #00995b"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#01858a" style="background-color: #01858a"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#008767" style="background-color: #008767"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#85a35d" style="background-color: #85a35d"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#9cbad6" style="background-color: #9cbad6"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#f0c6dc" style="background-color: #f0c6dc"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#83cb67" style="background-color: #83cb67"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#612b39" style="background-color: #612b39"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FFFFFF" style="background-color: #FFFFFF"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304972" style="background-color: #304972"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#5a5947" style="background-color: #5a5947"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#e7dbcb" style="background-color: #e7dbcb"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#304050" style="background-color: #304050"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#282b3c" style="background-color: #282b3c"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#564d46" style="background-color: #564d46"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#FF6633" style="background-color: #FF6633"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#b8a8cd" style="background-color: #b8a8cd"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C8D8AB" style="background-color: #C8D8AB"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#C48710" style="background-color: #C48710"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-fill="#663D50" style="background-color: #663D50"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Texture</div>
									<div class="pull-right">
										<div class="texture-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="base-path-texture">
									<div class="col-sm-8 col-md-6"><div class="well" data-src="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/all-pink.jpg" style="background: url('assets/img/textures/all-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-blend.jpg" style="background: url('assets/img/textures/black-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton.jpg" style="background: url('assets/img/textures/black-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-span.jpg" style="background: url('assets/img/textures/black-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/black-cotton-splasm.jpg" style="background: url('assets/img/textures/black-cotton-splasm.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-blend.jpg" style="background: url('assets/img/textures/blue-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/blue-cotton-blend.jpg" style="background: url('assets/img/textures/blue-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/brown-blend.jpg" style="background: url('assets/img/textures/brown-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/collon-blend.jpg" style="background: url('assets/img/textures/collon-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink.jpg" style="background: url('assets/img/textures/coral-pink.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/coral-pink-cotton.jpg" style="background: url('assets/img/textures/coral-pink-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/dark-grey.jpg" style="background: url('assets/img/textures/dark-grey.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/emerald-cotton-blend.jpg" style="background: url('assets/img/textures/emerald-cotton-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/light-brown.jpg" style="background: url('assets/img/textures/light-brown.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/red-blend.jpg" style="background: url('assets/img/textures/red-blend.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise.jpg" style="background: url('assets/img/textures/turquoise.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/turquoise-cotton-span.jpg" style="background: url('assets/img/textures/turquoise-cotton-span.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-black.jpg" style="background: url('assets/img/textures/white-black.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-blue.jpg" style="background: url('assets/img/textures/white-blue.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/textures/white-cotton.jpg" style="background: url('assets/img/textures/white-cotton.jpg') repeat scroll 0 0; background-size: cover"></div></div>
								</div>
								<div class="clearfix"></div>
								<br>
								<div class="btn-lg color-selector">
									<div class="pull-left">Pattern</div>
									<div class="pull-right">
										<div class="pattern-preview"></div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="display-gallery hidden" id="base-path-pattern">
									<div class="col-sm-8 col-md-6"><div class="well" data-src="" style="background-color: "></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/dragon2.jpg" style="background: url('assets/img/patterns/dragon2.jpg') repeat scroll 0 0; background-size: cover"></div></div>										
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-1.jpg" style="background: url('assets/img/patterns/stars-1.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/stars-4.jpg" style="background: url('assets/img/patterns/stars-4.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/balls.jpg" style="background: url('assets/img/patterns/balls.jpg') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-1.png" style="background: url('assets/img/patterns/pattern-1.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-2.png" style="background: url('assets/img/patterns/pattern-2.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-3.png" style="background: url('assets/img/patterns/pattern-3.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-4.png" style="background: url('assets/img/patterns/pattern-4.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-5.png" style="background: url('assets/img/patterns/pattern-5.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-6.png" style="background: url('assets/img/patterns/pattern-6.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-7.png" style="background: url('assets/img/patterns/pattern-7.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-8.png" style="background: url('assets/img/patterns/pattern-8.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-9.png" style="background: url('assets/img/patterns/pattern-9.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-10.png" style="background: url('assets/img/patterns/pattern-10.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-11.png" style="background: url('assets/img/patterns/pattern-11.png') repeat scroll 0 0; background-size: cover"></div></div>
									<div class="col-sm-8 col-md-6"><div class="well" data-src="assets/img/patterns/pattern-12.png" style="background: url('assets/img/patterns/pattern-12.png') repeat scroll 0 0; background-size: cover"></div></div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<!--/ Left -->
					
					<div class="col-sm-14 col-md-14">
						<div class="row">
							<!-- Center -->
							<div class="col-sm-14 col-md-18">
								<!-- Design Tool Stage-->
								<div class="col-sm-24">
									<div class="text-center stage" id="design-studio">
										<div data-side="front" data-params='{"preview": "assets/img/products/mug-01/left-preview.png", "base": "assets/img/products/mug-01/left-base.png", "paths": [{"name": "base-front", "path": "M 227.37,22.00 C 231.09,36.83 234.98,60.87 235.00,76.00 235.00,76.00 235.00,105.00 235.00,105.00 235.00,105.00 233.17,127.00 233.17,127.00 229.87,159.99 225.51,186.55 211.29,217.00 204.61,231.32 192.02,253.25 179.83,263.07 172.12,269.30 159.66,272.62 150.00,274.25 138.01,276.26 126.16,277.02 114.00,277.00 114.00,277.00 93.00,276.00 93.00,276.00 80.32,275.94 62.84,271.67 53.09,263.33 46.97,258.10 37.13,242.46 33.22,235.00 23.03,215.55 18.47,206.38 12.73,185.00 3.51,150.60 -0.42,136.53 0.00,100.00 0.00,100.00 0.96,89.00 0.96,89.00 1.82,69.25 3.11,55.39 7.50,36.00 10.55,22.52 11.33,15.87 18.00,3.00 37.56,14.93 59.68,16.56 82.00,18.09 82.00,18.09 94.00,19.04 94.00,19.04 94.00,19.04 110.00,19.04 110.00,19.04 110.00,19.04 119.00,19.97 119.00,19.97 119.00,19.97 133.00,19.00 133.00,19.00 145.27,18.98 157.84,18.00 170.00,16.27 186.53,13.91 206.19,11.62 219.00,0.00 223.20,5.43 225.69,15.28 227.37,22.00 Z", "x": 127, "y": 84, "height": 260,"texture":"assets/img/textures/white-cotton.jpg"},{"name": "handle", "path": "M 13.44,124.00 C -5.00,95.04 -5.54,57.51 16.01,30.00 20.08,24.81 24.59,19.86 30.00,16.04 49.62,2.18 72.76,0.00 96.00,0.00 96.00,0.00 90.00,28.00 90.00,28.00 90.00,28.00 73.00,26.00 73.00,26.00 65.37,25.91 58.31,26.46 51.00,28.89 46.35,30.43 41.91,32.79 38.01,35.76 31.40,40.80 26.79,49.05 24.64,57.00 17.72,82.63 29.02,104.95 47.04,122.96 55.00,130.92 65.72,138.28 76.00,142.91 76.00,142.91 92.10,148.74 92.10,148.74 93.74,150.51 94.84,156.51 95.47,159.00 95.47,159.00 101.00,180.00 101.00,180.00 66.90,166.36 34.52,157.12 13.44,124.00 Z", "x": 46, "y": 102, "height": 169,"pattern":"assets/img/patterns/stars-1.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/browsewire.png","editorMode": true, "draggable": true, "resizeToW": 92, "x": 196, "y": 121}},{"type": "text", "params": {"src": "BROWSEWIRE", "editorMode": true, "draggable": true, "x": 167, "y":186,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#6E95CE"], "textSize": 20,"stroke":"#28272C"}},{"type": "text", "params": {"src": "Consulting Pvt. Ltd", "editorMode": true, "draggable": true, "x": 167, "y":209,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#B3042B"], "textSize": 15}},{"type": "text", "params": {"src": "Lets Grow Together", "editorMode": true, "draggable": true, "x": 184, "y":227,"fontWeight":"bold", "textAlign": "center", "font": "Trebuchet MS", "colors": ["#663D50"], "textSize": 12}}]}'></div>
										<div class="hide" data-side="back" data-params='{"preview": "assets/img/products/mug-01/right-preview.png", "base": "assets/img/products/mug-01/right-base.png", "paths": [{"name": "base-front", "path": "M 28.00,7.38 C 47.08,15.42 69.61,16.69 90.00,18.09 90.00,18.09 102.00,19.00 102.00,19.00 132.49,19.05 169.67,19.49 199.00,11.09 210.03,7.93 210.96,5.69 218.00,3.00 218.00,3.00 224.09,19.00 224.09,19.00 228.07,32.90 233.93,64.86 234.00,79.00 234.00,79.00 235.00,97.00 235.00,97.00 235.00,97.00 233.90,125.00 233.90,125.00 233.90,125.00 233.90,136.00 233.90,136.00 231.30,155.61 225.78,172.16 220.73,191.00 215.87,209.13 207.30,225.85 197.86,242.00 192.58,251.03 186.09,260.98 177.00,266.54 162.42,275.45 145.52,275.81 129.00,276.00 129.00,276.00 117.00,276.97 117.00,276.97 117.00,276.97 108.00,276.09 108.00,276.09 90.51,274.89 72.14,274.02 57.00,263.90 51.04,259.91 45.04,251.86 40.86,246.00 31.08,232.30 22.21,214.91 16.66,199.00 7.76,173.51 4.38,149.68 1.83,123.00 1.83,123.00 0.00,104.00 0.00,104.00 0.00,104.00 0.00,77.00 0.00,77.00 0.01,69.43 0.87,67.05 1.72,60.00 1.72,60.00 2.29,51.00 2.29,51.00 3.46,41.48 5.92,28.18 8.56,19.00 8.56,19.00 15.00,0.00 15.00,0.00 15.00,0.00 28.00,7.38 28.00,7.38 Z", "x": 61, "y": 79, "height": 260,"texture":"assets/img/textures/white-cotton.jpg"},{"name": "handle", "path": "M 54.00,6.69 C 81.57,16.70 98.12,41.49 101.16,70.00 101.16,70.00 101.92,77.00 101.92,77.00 101.92,91.20 96.79,108.85 89.55,121.00 79.07,138.57 61.25,153.40 43.00,162.40 43.00,162.40 25.00,169.80 25.00,169.80 18.41,172.44 6.40,177.84 0.00,179.00 0.00,179.00 9.00,147.00 9.00,147.00 18.95,146.19 27.60,142.15 36.00,136.94 55.35,124.93 76.01,101.47 78.82,78.00 79.50,72.32 78.84,67.51 77.66,62.00 76.26,55.42 75.09,49.72 71.16,44.00 58.35,25.34 32.33,22.99 12.00,28.00 12.00,28.00 6.00,0.00 6.00,0.00 24.04,0.00 36.63,0.39 54.00,6.69 Z", "x": 268, "y": 97, "height": 168,"pattern":"assets/img/patterns/stars-1.jpg"}],"template":[{"type": "image", "params": {"src": "assets/img/sample/browsewire.png","editorMode": true, "draggable": true, "resizeToW": 92, "x": 126, "y": 113}},{"type": "text", "params": {"src": "BROWSEWIRE", "editorMode": true, "draggable": true, "x": 98, "y":181,"fontWeight":"bold", "textAlign": "center", "font": "Quicksand", "colors": ["#6E95CE"], "textSize": 20,"stroke":"#28272C"}}]}'></div>
									</div>								
								</div>
								<!--/ Design Tool Stage-->
							</div>
							<!--/ Center -->

							<!-- Right -->
							<div class="col-sm-10 col-md-6">
								<div class="row">
									<span class="visible-xs"><br></span>
									<div class="col-sm-12 text-center preview-images">
										<div class="row">
											<!-- Preview Images -->
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="front-preview" alt="Front">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="left-preview" alt="Left">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="back-preview" alt="Back">
											</div>
											<div class="col-xs-6 col-sm-24">
												<img src="" class="img-thumbnail" id="right-preview" alt="Right">
											</div>
											<!--/ Preview Images -->
											
											<div class="col-xs-24 visible-lg text-center operation-icons">
												<br><br><br>
												<i class="ion-chevron-left ion-2x previous-operation" title="Pervious Operation" data-toggle="tooltip"></i>
												&nbsp;&nbsp;&nbsp;&nbsp;
												<i class="ion-chevron-right ion-2x next-operation" title="Next Operation" data-toggle="tooltip"></i>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--/ Right -->
						</div>

						<div class="row">
							<div class="col-sm-14 col-md-18">
								<div class="col-sm-20 col-sm-offset-2">
									<br>
									<div class="col-xs-18 col-md-21">
										<br>
										<div id="zoom-stage"></div>
									</div>
									<div class="col-xs-3">
										<div class="circle">
											<div class="circle_outer">
												<div class="left_arrow"><a href="javascript:void(0)" class="move" data-side="left"><i class="ion-1-half ion-ios-arrow-left"></i></a></div>
												<div class="top_arrow"><a href="javascript:void(0)" class="move" data-side="up"><i class="ion-1-half ion-ios-arrow-up"></i></a></div>
												<div class="right_arrow"><a href="javascript:void(0)" class="move" data-side="right"><i class="ion-1-half ion-ios-arrow-right"></i></a></div>
												<div class="down_arrow"><a href="javascript:void(0)" class="move" data-side="down"><i class="ion-1-half ion-ios-arrow-down"></i></a></div>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Price -->
							<div class="col-sm-10 col-md-6">
								<span class="visible-xs"><br><br><br><br><br><br></span>
								<div class="col-sm-12 text-center total_price_col">
									<br>
									<div class="well well-sm"><h4>$<span class="total_price">0.00</span></h4></div>
									<i class="face-share ion-2x ion-social-facebook-outline"></i>
									&nbsp;&nbsp;
									<i class="tweet-share ion-2x ion-social-twitter-outline"></i>
								</div>
							</div>
							<!--/ Price -->
						</div>
					</div>
				</div>
				<!--/ Design Tool -->
			</div>
		</div>
		<!--/  Main Container -->
		
		<!-- Pre-load Fonts -->
		<section class="preload-fonts"></section>
		<!--/ Pre-load Fonts -->
		<!-- JS -->
		<!-- plugins -->
		<script type="text/javascript" src="assets/lib/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/lib/confirmation/bootstrap-confirmation.min.js"></script>
		<script type="text/javascript" src="assets/lib/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
		<script type="text/javascript" src="assets/lib/wordcloud2/wordcloud2.js"></script>
		<script type="text/javascript" src="assets/lib/fabric/fabric.min.js"></script>
		<script type="text/javascript" src="assets/lib/design-studio/design-studio.js"></script>
		<script type="text/javascript" src="assets/lib/fabric-curved-text/fabric.curvedText.js"></script>
		<script type="text/javascript" src="assets/lib/nouislider/nouislider.min.js"></script>

		<script src="assets/lib/bootstrap-switch/bootstrap-switch.min.js"></script>
		<!--/ plugins -->
		<script type="text/javascript" src="assets/js/index.js"></script>
		<!--script type="text/javascript" src="assets/js/wordart.js"></script-->
		<!--/ JS -->
	</body>
</html>